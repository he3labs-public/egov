defmodule Core.Application do
  @moduledoc """
  The Core Application Service.

  The core system business domain lives in this application.

  Exposes API to clients such as the `CoreWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link(
      [
        supervisor(Core.Repo, []),
        supervisor(Phoenix.PubSub.PG2, [Core.PubSub, []]),
        Core.Activity,
        demo_banking_provisioner(),
        Core.Voting.Election.Scheduler,
        Core.Voting.Election.Opener,
        Core.Voting.Referenda.Scheduler,
        Core.Voting.Referenda.Opener
      ],
      strategy: :one_for_one,
      name: Core.Supervisor
    )
  end

  defp demo_banking_provisioner do
    Application.fetch_env!(:core, :demo_banking_provisioner)
  end
end
