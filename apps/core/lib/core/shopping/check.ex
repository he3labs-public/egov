defmodule Core.Shopping.Check do
  use Ecto.Schema
  import Ecto.Changeset
  alias Core.Shopping.CheckItem

  @derive {Poison.Encoder, only: [:customer_id, :customer_name, :status, :total, :items]}

  @statuses ["open", "closed"]

  schema("shopping_checks") do
    field(:customer_id, Ecto.UUID)
    field(:customer_name, :string)
    field(:status, :string)
    field(:total, :integer, default: 0)
    embeds_many(:items, CheckItem, on_replace: :delete)

    timestamps()
  end

  @doc false
  def changeset(check, attrs) do
    check
    |> cast(attrs, [:customer_id, :customer_name, :status, :total])
    |> cast_embed(:items)
    |> validate_required([:status])
    |> validate_inclusion(:status, @statuses)
  end

  def add_item_changeset(%__MODULE__{} = check, item) do
    check
    |> changeset(%{total: check.total + item.price})
    |> put_embed(:items, check.items ++ [item])
  end
end
