defmodule Core.Shopping do
  @moduledoc """
  The Shopping context.
  """

  import Ecto.Query, warn: false

  alias Core.Repo
  alias Core.Shopping.{Check, CheckItem, Product}
  alias Core.Banking

  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_products do
    Repo.all(Product)
  end

  @doc """
  Gets a single product.

  Raises `Ecto.NoResultsError` if the Product does not exist.

  ## Examples

      iex> get_product!(123)
      %Product{}

      iex> get_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_product!(id), do: Repo.get!(Product, id)

  def get_product(id), do: Repo.get(Product, id)

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Product.

  ## Examples

      iex> delete_product(product)
      {:ok, %Product{}}

      iex> delete_product(product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking product changes.

  ## Examples

      iex> change_product(product)
      %Ecto.Changeset{source: %Product{}}

  """
  def change_product(%Product{} = product) do
    Product.changeset(product, %{})
  end

  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_checks do
    Repo.all(Check)
  end

  @doc """
  Creates a check.

  ## Examples

      iex> create_check(%{field: value})
      {:ok, %Check{}}

      iex> create_check(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_check(attrs \\ %{}) do
    %Check{}
    |> Check.changeset(attrs)
    |> Repo.insert()
  end

  def current_check() do
    query = from(check in Check, order_by: [desc: :inserted_at], limit: 1)

    with nil <- Repo.one(query) do
      {:ok, check} =
        empty_check()
        |> Check.changeset(%{})
        |> Repo.insert()

      check
    end
  end

  def reset_check(check) do
    update_check(check, empty_check_attrs())
  end

  def empty_check() do
    %Check{
      items: [],
      total: 0,
      status: "open",
      customer_id: nil,
      customer_name: nil
    }
  end

  def empty_check_attrs() do
    %{
      items: [],
      total: 0,
      status: "open",
      customer_id: nil,
      customer_name: nil
    }
  end

  @doc """
  Updates a check.

  ## Examples

      iex> update_check(check, %{field: new_value})
      {:ok, %Check{}}

      iex> update_check(check, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_check(%Check{} = check, attrs) do
    check
    |> Check.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking check changes.

  ## Examples

      iex> change_check(check)
      %Ecto.Changeset{source: %Check{}}

  """
  def change_check(%Check{} = check) do
    Check.changeset(check, %{})
  end

  def add_product_to_check(%Check{} = check, product) do
    case CheckItem.from_product(product) do
      {:ok, item} ->
        changeset = Check.add_item_changeset(check, item)

        with {:error, changeset} <- Repo.update(changeset) do
          reason = EctoHelper.pretty_error_sentence(changeset.errors)
          {:error, reason}
        end

      {:error, changeset} ->
        reason = EctoHelper.pretty_error_sentence(changeset.errors)
        {:error, reason}
    end
  end

  def add_customer_to_check(%Check{} = check, customer_id, customer_name) do
    with {:error, changeset} <-
           update_check(check, %{customer_id: customer_id, customer_name: customer_name}) do
      reason = EctoHelper.pretty_error_sentence(changeset.errors)
      {:error, reason}
    end
  end

  @doc """
  Change check status to "closed"
  """
  def close_check(%Check{} = check) do
    with {:error, changeset} <- update_check(check, %{status: "closed"}) do
      reason = EctoHelper.pretty_error_sentence(changeset.errors)
      {:error, reason}
    end
  end

  @doc """
  Lookup a customer by id_number.
  """
  def lookup_customer(id_number) do
    case Core.Account.get_identity(id_number) do
      nil ->
        {:error, "Customer not found"}

      identity ->
        {:ok, identity |> Repo.preload(:nem_account)}
    end
  end

  def charge_customer(customer_id_number, amount) do
    # Create a bank transaction from the customer to the "store"
    #   + Demo Lord account will act as the "store"
    #   + Send Banking transfer
    #   + Stash transaction id in socket assign
    #   + Subscribe to Core "banking:transaction_completed" and "banking:transaction_failed"
    #   + Broadcast respective success/fail messages to clerk and customer

    attrs = %{
      from_id: customer_id_number,
      to_id: store_id_number(),
      description: "Shopping",
      amount: amount
    }

    with {:error, changeset} <- Banking.create_transaction(attrs) do
      reason = EctoHelper.pretty_error_sentence(changeset.errors)
      {:error, reason}
    end
  end

  def store_id_number() do
    store_account().id_number
  end

  def store_account() do
    from(
      a in Banking.Account,
      where: [account_type: "store"],
      order_by: [desc: :inserted_at],
      limit: 1
    )
    |> Repo.all()
    |> List.first()
    |> case do
      nil ->
        case Banking.create_account(new_store_account_attrs(), broadcast: false) do
          {:ok, account} ->
            account

          {:error, changeset} ->
            reason = EctoHelper.pretty_error_sentence(changeset.errors)
            raise "Problem creating the Demo Store account: #{reason}"
        end

      account ->
        account
    end
  end

  def new_store_account_attrs() do
    %{
      first_name: "Demo",
      last_name: "Store",
      id_number: Ecto.UUID.generate(),
      account_type: "store"
    }
  end

  def count_store_accounts() do
    Banking.query_accounts_by_type("store")
    |> Repo.aggregate(:count, :id)
  end

  def count_products() do
    Repo.aggregate(Product, :count, :id)
  end
end
