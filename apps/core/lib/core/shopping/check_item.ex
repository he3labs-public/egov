defmodule Core.Shopping.CheckItem do
  use Ecto.Schema
  import Ecto.Changeset

  alias Core.Shopping.Product

  @derive {Poison.Encoder, only: [:id, :name, :price, :timestamp]}
  @primary_key false

  embedded_schema do
    field(:id, :binary_id)
    field(:name, :string)
    field(:price, :integer)
    field(:timestamp, :naive_datetime)
  end

  @doc false
  def from_product(%Product{} = product) do
    attrs =
      product
      |> Map.from_struct()

    %__MODULE__{}
    |> cast(attrs, [:id, :name, :price])
    |> validate_required([:id, :name, :price])
    |> put_change(:timestamp, Core.naive_now())
    |> apply_action(:insert)
  end

  def matchable(%__MODULE__{} = item) do
    item
    |> Map.from_struct()
    |> Map.delete(:timestamp)
  end
end
