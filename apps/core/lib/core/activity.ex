defmodule Core.Activity do
  use GenServer

  defmodule Entry do
    defstruct [:message, :datetime]
  end

  def start_link([]) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init([]) do
    :timer.send_interval(1000, :truncate)

    {:ok, %{history: [], last_updated: DateTime.utc_now()}}
  end

  def log(message) when is_binary(message) do
    GenServer.cast(__MODULE__, {:log, message, DateTime.utc_now()})
  end

  def history do
    GenServer.call(__MODULE__, :history)
  end

  def last_updated do
    GenServer.call(__MODULE__, :last_updated)
  end

  ##
  ## Server
  ##

  def handle_cast({:log, message, datetime}, state) do
    entry = %Entry{message: message, datetime: datetime}
    updated_state = add_entry(state, entry)

    Core.broadcast("activity:log", entry)

    {:noreply, updated_state}
  end

  def handle_call(:history, _from, state) do
    {:reply, state.history, state}
  end

  def handle_call(:last_updated, _from, state) do
    {:reply, state.last_updated, state}
  end

  def handle_info(:truncate, state) do
    truncated_history = Enum.take(state.history, 200)
    updated_state = %{state | history: truncated_history}

    {:noreply, updated_state}
  end

  defp add_entry(state, entry) do
    state
    |> Map.put(:history, [entry | state.history])
    |> Map.put(:last_updated, DateTime.utc_now())
  end
end
