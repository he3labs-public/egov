defmodule Core.Account.Identity do
  use Ecto.Schema
  import Ecto.Changeset

  alias Core.Account.{ApiToken, MailingAddress}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  @default_phone "(406) 555-1212"
  @default_blood_degree "1/4"
  @default_mailing_address %MailingAddress{
    address1: "654 Main St.",
    address2: "Suite #1",
    city: "Billings",
    state: "MT",
    postcode: "59105"
  }

  schema "identities" do
    field(:birthdate, :date)
    field(:blood_degree, :string, default: @default_blood_degree)
    field(:email, :string)
    field(:first_name, :string)
    field(:last_name, :string)
    field(:phone, :string, default: @default_phone)
    field(:pin, :string)

    embeds_one(:mailing_address, MailingAddress, on_replace: :delete)
    has_one(:api_token, ApiToken)
    belongs_to(:nem_account, Core.NemAccount, references: :address, type: :string)

    timestamps()
  end

  @doc false
  def changeset(identity, attrs) do
    identity
    |> cast(attrs, [:birthdate, :email, :first_name, :last_name, :nem_account_id, :pin])
    |> put_embed(:mailing_address, @default_mailing_address)
    |> cast_embed(:mailing_address, required: true)
    |> assoc_constraint(:nem_account)
    |> put_change(:blood_degree, @default_blood_degree)
    |> put_change(:phone, @default_phone)
    |> put_change(:pin, Core.Demo.Pin.unique_pin("identities"))
    |> validate_required([:birthdate, :email, :first_name, :last_name, :pin])
  end

  def default_mailing_address() do
    @default_mailing_address
  end

  def full_name(%__MODULE__{} = identity) do
    "#{identity.first_name} #{identity.last_name}"
  end
end
