defmodule Core.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.Query, warn: false

  alias Ecto.Multi
  alias Core.{NemAccount, Repo}
  alias Core.Account.{ApiToken, Identity}

  def authorize(nil), do: nil
  def authorize(pin), do: Repo.get_by(Identity, pin: pin) |> Repo.preload(:api_token)

  @doc """
  Returns the list of identities.

  ## Examples

      iex> list_identities()
      [%Identity{}, ...]

  """
  def list_identities do
    Repo.all(Identity)
    |> Repo.preload(:api_token)
  end

  @doc """
  Counts the number of identities.
  """
  def count_identities do
    Repo.aggregate(Identity, :count, :id)
  end

  @doc """
  Gets a single identity.

  Raises `Ecto.NoResultsError` if the Identity does not exist.

  ## Examples

      iex> get_identity!(123)
      %Identity{}

      iex> get_identity!(456)
      ** (Ecto.NoResultsError)

  """
  def get_identity!(id), do: Repo.get!(Identity, id)
  def get_identity(id), do: Repo.get(Identity, id)

  @doc """
  Creates a identity.

  ## Examples

      iex> create_identity(%{field: value})
      {:ok, %Identity{}}

      iex> create_identity(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_identity(attrs \\ %{}) do
    multi =
      Multi.new()
      |> Multi.insert(
        :nem_account,
        NemAccount.generate_key_pair() |> NemAccount.insert_changeset()
      )
      |> Multi.run(:identity, &insert_identity(&1, attrs))
      |> Multi.run(
        :api_token,
        &create_api_token(%{token: ApiToken.generate_token(), identity_id: &1.identity.id})
      )

    case Repo.transaction(multi) do
      {:ok, %{identity: identity}} ->
        Core.broadcast("account:identity_created", identity)
        {:ok, identity}

      {:error, :nem_account, changeset, _} ->
        Core.broadcast("account:identity_creation_failed", changeset)
        {:error, changeset}

      {:error, :identity, changeset, _} ->
        Core.broadcast("account:identity_creation_failed", changeset)
        {:error, changeset}
    end
  end

  @doc """
  Updates a identity.

  ## Examples

      iex> update_identity(identity, %{field: new_value})
      {:ok, %Identity{}}

      iex> update_identity(identity, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_identity(%Identity{} = identity, attrs) do
    identity
    |> Identity.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Identity.

  ## Examples

      iex> delete_identity(identity)
      {:ok, %Identity{}}

      iex> delete_identity(identity)
      {:error, %Ecto.Changeset{}}

  """
  def delete_identity(%Identity{} = identity) do
    Repo.delete(identity)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking identity changes.

  ## Examples

      iex> change_identity(identity)
      %Ecto.Changeset{source: %Identity{}}

  """
  def change_identity(%Identity{} = identity) do
    Identity.changeset(identity, %{})
  end

  defp insert_identity(%{nem_account: nem_account}, params) do
    %Identity{nem_account_id: nem_account.address}
    |> Identity.changeset(params)
    |> Repo.insert()
  end

  @doc """
  Returns the list of api_tokens.

  ## Examples

      iex> list_api_tokens()
      [%ApiToken{}, ...]

  """
  def list_api_tokens do
    Repo.all(ApiToken)
  end

  def api_token_valid?(token) do
    query =
      from(
        entry in "api_tokens",
        where: entry.token == ^token,
        select: entry.id
      )

    if Repo.one(query) do
      true
    else
      false
    end
  end

  @doc """
  Gets a single api_token.

  Raises `Ecto.NoResultsError` if the Api token does not exist.

  ## Examples

      iex> get_api_token!(123)
      %ApiToken{}

      iex> get_api_token!(456)
      ** (Ecto.NoResultsError)

  """
  def get_api_token!(id), do: Repo.get!(ApiToken, id)
  def get_api_token_by!(opts), do: Repo.get_by!(ApiToken, opts)

  @doc """
  Creates a api_token.

  ## Examples

      iex> create_api_token(%{field: value})
      {:ok, %ApiToken{}}

      iex> create_api_token(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_api_token(attrs \\ %{}) do
    %ApiToken{}
    |> ApiToken.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a api_token.

  ## Examples

      iex> update_api_token(api_token, %{field: new_value})
      {:ok, %ApiToken{}}

      iex> update_api_token(api_token, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_api_token(%ApiToken{} = api_token, attrs) do
    api_token
    |> ApiToken.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ApiToken.

  ## Examples

      iex> delete_api_token(api_token)
      {:ok, %ApiToken{}}

      iex> delete_api_token(api_token)
      {:error, %Ecto.Changeset{}}

  """
  def delete_api_token(%ApiToken{} = api_token) do
    Repo.delete(api_token)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking api_token changes.

  ## Examples

      iex> change_api_token(api_token)
      %Ecto.Changeset{source: %ApiToken{}}

  """
  def change_api_token(%ApiToken{} = api_token) do
    ApiToken.changeset(api_token, %{})
  end
end
