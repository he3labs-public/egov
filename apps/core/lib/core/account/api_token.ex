defmodule Core.Account.ApiToken do
  use Ecto.Schema
  import Ecto.Changeset

  schema "api_tokens" do
    field :token, :string

    belongs_to(:identity, Core.Account.Identity, type: :binary_id)

    timestamps()
  end

  def generate() do
    %__MODULE__{
      token: generate_token()
    }
  end

  def generate_token() do
    Ecto.UUID.generate |> String.replace("-", "")
  end

  @doc false
  def changeset(api_token, attrs) do
    api_token
    |> cast(attrs, [:token, :identity_id])
    |> assoc_constraint(:identity)
    |> validate_required([:token])
  end
end
