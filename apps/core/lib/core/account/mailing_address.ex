defmodule Core.Account.MailingAddress do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field :address1, :string
    field :address2, :string
    field :city, :string
    field :state, :string
    field :postcode, :string
  end

  @doc false
  def changeset(address, attrs) do
    address
    |> cast(attrs, [:address1, :address2, :city, :state, :postcode])
    |> validate_required([:address1, :city, :state, :postcode])
  end
end
