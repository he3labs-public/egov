defmodule Core.Demo.BankAccount.Provisioner do
  use GenServer
  require Logger

  alias Core.Account.{Identity}
  alias Core.Activity
  alias Core.Banking

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Core.subscribe("account:identity_created")
    Core.subscribe("banking:account_created")

    Activity.log("Bank Account provisioner ready")

    {:ok, %{}}
  end

  def provision(%Banking.Account{} = account, amount) when is_integer(amount) do
    GenServer.call(__MODULE__, {:provision, account, amount})
  end

  def handle_call({:provision, account, amount}, _from, state) do
    result = provision_bank_account(account, amount)
    {:reply, result, state}
  end

  def handle_info({"account:identity_created", %Identity{} = identity}, state) do
    Banking.create_account(%{
      first_name: identity.first_name,
      last_name: identity.last_name,
      id_number: identity.id,
      account_type: "citizen"
    })

    {:noreply, state}
  end

  def handle_info({"banking:account_created", %Banking.Account{} = account}, state) do
    provision_bank_account(account, generate_seed_amount())

    {:noreply, state}
  end

  def generate_seed_amount do
    x = :rand.uniform(10000)
    100_000 + x
  end

  def provision_bank_account(%Banking.Account{} = account, amount) when is_integer(amount) do
    attrs = %{
      from_id: provisioner_id_number(),
      to_id: account.id_number,
      description: "Seeding Account",
      amount: amount
    }

    with {:error, changeset} <- Banking.create_transaction(attrs) do
      reason = EctoHelper.pretty_error_sentence(changeset.errors)
      Logger.warn("[Banking Provisioner] Failed to provision: #{reason}")
      {:error, reason}
    end
  end

  def provisioner_id_number() do
    provisioner_id = "781891de-6797-4c33-96f1-b49e693bd6c8"

    case Banking.lookup(provisioner_id) do
      nil ->
        attrs = %{
          first_name: "Demo",
          last_name: "Provisioner",
          id_number: provisioner_id,
          account_type: "provision",
          nem_account: Application.fetch_env!(:core, :demo_lord_key_pair)
        }

        case Banking.create_account(attrs) do
          {:ok, _account} ->
            provisioner_id

          {:error, changeset} ->
            reason = EctoHelper.pretty_error_sentence(changeset.errors)
            raise "Problem creating the Demo Provisioner account: #{reason}"
        end

      _account ->
        provisioner_id
    end
  end
end
