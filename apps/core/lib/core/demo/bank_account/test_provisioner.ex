defmodule Core.Demo.BankAccount.TestProvisioner do
  use GenServer
  require Logger

  alias Core.Account.{Identity}
  alias Core.{Banking}

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Core.subscribe("account:identity_created")
    Core.subscribe("banking:account_created")

    {:ok, %{}}
  end

  def handle_info({"account:identity_created", %Identity{} = identity}, state) do
    attrs = %{
      first_name: identity.first_name,
      last_name: identity.last_name,
      id_number: identity.id
    }

    Logger.debug "[Demo.BankAccount.TestProvisioner] Banking.create_account(#{inspect attrs})"

    {:noreply, state}
  end

  def handle_info({"banking:account_created", %Banking.Account{} = account}, state) do
    Logger.debug "[Demo.BankAccount.TestProvisioner] Demo.seed_bank_account(#{inspect account})"

    {:noreply, state}
  end
end
