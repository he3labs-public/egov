defmodule Core.Demo.BankAccount.Seeder do
  use GenServer
  require Logger

  alias Exnem.Transaction
  alias Exnem.Crypto.KeyPair
  alias Core.Demo.BankAccount
  alias Core.{Activity, Repo, NemTransaction}

  defmodule State do
    defstruct [:seed, :observer]
  end

  def start(%BankAccount.Seed{} = seed) do
    GenServer.start(__MODULE__, seed)
  end

  def init(%BankAccount.Seed{} = seed) do
    {:ok, pid} = start_observer(seed.nem_address)

    seed = seed |> Repo.preload(:seed_transaction)
    {:ok, %State{seed: seed, observer: pid}}
  end

  def handle_info("observerReady", state) do
    updated_seed = announce_transfer(state.seed)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"error", error}, state) do
    error_meaning = Exnem.StatusErrors.meaning(error.status)
    info("[BankAccount.Seeder] Seeding failed #{error.status}: #{error_meaning}")

    updated_seed = handle_error(state.seed, error)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def handle_info({"unconfirmedAdded", dto}, state) do
    updated_seed = handle_unconfirmed(state.seed, dto)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"confirmedAdded", dto}, state) do
    info("[BankAccount.Seeder] Seeding complete.")
    updated_seed = handle_confirmed(state.seed, dto)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def terminate(_reason, state) do
    stop_observer(state)
    :ok
  end

  defp handle_error(seed, error) do
    tx = seed.seed_transaction

    updated_tx =
      tx
      |> NemTransaction.failed_changeset(error.status)
      |> Repo.update!()

    updated_seed = %{seed | seed_transaction: updated_tx}
    Core.broadcast("demo:bank_account_seeding_failed", updated_seed)
    updated_seed
  end

  defp handle_unconfirmed(seed, _dto) do
    seed
  end

  defp handle_confirmed(seed, _dto) do
    tx = seed.seed_transaction

    updated_tx =
      tx
      |> NemTransaction.confirmed_changeset()
      |> Repo.update!()

    updated_seed = %{seed | seed_transaction: updated_tx}
    Core.broadcast("demo:bank_account_seeded", updated_seed)
    updated_seed
  end

  defp announce_transfer(
         %{
           amount: amount,
           nem_address: nem_address
         } = seed
       ) do
    {:ok, mosaic} = Transaction.mosaic(BankAccount.Seed.bank_mosaic(), amount)

    {:ok, transfer_transaction} =
      Transaction.transfer(nem_address, [mosaic], message: "Demo Seeding")

    demo_lord = Application.fetch_env!(:core, :demo_lord_key_pair)

    verf_tx =
      transfer_transaction
      |> Transaction.pack()
      |> KeyPair.sign(demo_lord)

    :ok = Exnem.Announce.announce(verf_tx)

    original_tx =
      NemTransaction.announced_changeset(verf_tx.hash)
      |> Repo.insert!()

    updated_seed = %{seed | seed_transaction: original_tx}

    Core.broadcast("demo:bank_account_seed_announced", updated_seed)

    updated_seed
  end

  defp start_observer(nem_address) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.start_link(nem_address, self())
  end

  defp stop_observer(%State{observer: pid}) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.stop(pid)
  end

  defp info(message) do
    Logger.debug(message)
    Activity.log(message)
  end
end
