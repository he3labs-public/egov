defmodule Core.Demo.BankAccount.Seed do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  
  schema "bank_account_seeds" do
    field(:amount, :integer)
    field(:banking_account_id, :binary_id)
    field(:nem_address, :string)

    belongs_to(:seed_transaction, Core.NemTransaction, references: :tx_hash, type: :string)

    timestamps()
  end

  def bank_mosaic() do
    Application.fetch_env!(:core, :demo_lord_mosaic_ids).usd
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:amount, :nem_address, :banking_account_id])
    |> cast_assoc(:seed_transaction)
    |> validate_required([:amount, :nem_address, :banking_account_id])
  end

  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end
