defmodule Core.Demo do
  @moduledoc """
  The Demo context.
  """
  alias Core.{Account, Banking, NemAccount, Repo, Voting}
  alias Core.Demo.BankAccount
  require Logger

  def demo_lord_key_pair() do
    Application.fetch_env!(:core, :demo_lord_key_pair)
  end

  def namespace do
    Application.fetch_env!(:core, :demo_namespace)
  end

  def mosaic_name do
    Application.fetch_env!(:core, :demo_mosaic_name)
  end

  def mosaic_supply do
    Application.fetch_env!(:core, :demo_mosaic_supply)
  end

  def usd_mosaic_id do
    Application.fetch_env!(:core, :demo_lord_mosaic_ids).usd
  end

  def create_identity(%{} = attrs) do
    Account.create_identity(attrs)
  end

  def bank_mosaic() do
    Application.fetch_env!(:core, :demo_lord_mosaic_ids).usd
  end

  def seed_bank_account(%Banking.Account{
        id: account_id,
        nem_account_id: address
      }) do
    attrs = %{
      banking_account_id: account_id,
      nem_address: address,
      amount: generate_seed_amount()
    }

    changeset = BankAccount.Seed.insert_changeset(attrs)

    case Repo.insert(changeset) do
      {:ok, seed} ->
        {:ok, _pid} = BankAccount.Seeder.start(seed)
        {:ok, seed}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def generate_seed_amount do
    x = :rand.uniform(150)
    50 + x
  end

  def request_referenda_ballot(referenda_id, voter_id) do
    NemAccount.generate_key_pair()
    |> NemAccount.insert_changeset()
    |> Repo.insert()
    |> case do
      {:ok, nem_account} ->
        Voting.request_referenda_ballot(referenda_id, voter_id, nem_account.address)

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def request_election_ballot(election_id, voter_id) do
    NemAccount.generate_key_pair()
    |> NemAccount.insert_changeset()
    |> Repo.insert()
    |> case do
      {:ok, nem_account} ->
        Voting.request_election_ballot(election_id, voter_id, nem_account.address)

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @max_message_size 1024
  def submit_election_ballot(election_id, voter_id, ballot) do
    zipped_ballot = Voting.serialize_ballot(ballot)

    participation = Voting.get_election_participation(election_id, voter_id)

    parse_submission =
      ballot
      |> Voting.Election.SubmissionPayload.insert_changeset()
      |> Ecto.Changeset.apply_action(:insert)

    cond do
      participation == nil ->
        {:error, "Ballot does not exist, request one first."}

      not Core.NemTransaction.confirmed?(
        Repo.preload(participation, :request_transaction).request_transaction
      ) ->
        {:error, "Ballot request not yet confirmed."}

      byte_size(zipped_ballot) > @max_message_size ->
        {:error, "Ballot cannot be submitted due to length!  Fatal error."}

      :error == elem(parse_submission, 0) ->
        {:error, "Ballot could not be parsed as an election ballot."}

      elem(parse_submission, 1).election_id != election_id ->
        {:error, "Ballot references the wrong election id."}

      true ->
        Logger.debug("[Voting] Ballot submitted -- #{inspect(ballot)}")

        Voting.submit_election_ballot(
          election_id,
          voter_id,
          participation.ballot_address,
          zipped_ballot
        )
    end
  end

  def submit_referenda_ballot(referenda_id, voter_id, ballot) do
    zipped_ballot = Voting.serialize_ballot(ballot)

    participation = Voting.get_referenda_participation(referenda_id, voter_id)

    parse_submission =
      ballot
      |> Voting.Referenda.SubmissionPayload.insert_changeset()
      |> Ecto.Changeset.apply_action(:insert)

    cond do
      participation == nil ->
        {:error, "Ballot does not exist, request one first."}

      not Core.NemTransaction.confirmed?(
        Repo.preload(participation, :request_transaction).request_transaction
      ) ->
        {:error, "Ballot request not yet confirmed."}

      byte_size(zipped_ballot) > @max_message_size ->
        {:error, "Ballot cannot be submitted due to length!  Fatal error."}

      :error == elem(parse_submission, 0) ->
        {:error, "Ballot could not be parsed as an referenda ballot."}

      elem(parse_submission, 1).referenda_id != referenda_id ->
        {:error, "Ballot references the wrong referenda id."}

      true ->
        Logger.debug("[Voting] Ballot submitted -- #{inspect(ballot)}")

        Voting.submit_referenda_ballot(
          referenda_id,
          voter_id,
          participation.ballot_address,
          zipped_ballot
        )
    end
  end
end
