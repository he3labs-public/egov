defmodule Core.Demo.Voting.Confirmation do
  @behaviour Core.Demo.Voting.Confirmation.Impl

  alias Core.Voting.Confirmation

  @spec get_confirmation(%Confirmation{}) :: String.t()
  def get_confirmation(%Confirmation{} = confirmation) do
    impl().get_confirmation(confirmation)
  end

  defp impl do
    Application.get_env(
      :demo,
      :voting_confirmation_impl,
      Core.Demo.Voting.Confirmation.DefaultImpl
    )
  end
end
