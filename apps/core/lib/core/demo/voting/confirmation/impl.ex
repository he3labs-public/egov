defmodule Core.Demo.Voting.Confirmation.Impl do
  @moduledoc false

  alias Core.Voting.Confirmation

  @callback get_confirmation(%Confirmation{}) :: String.t()
end
