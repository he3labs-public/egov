defmodule Core.Demo.Voting.Confirmation.DefaultImpl do
  @moduledoc false
  @behaviour Core.Demo.Voting.Confirmation.Impl

  alias Core.Voting.Confirmation

  @spec get_confirmation(%Confirmation{}) :: String.t()
  def get_confirmation(%Confirmation{} = confirmation) do
    {:ok, tranasction} = Exnem.Node.get("transaction/#{confirmation.tx_hash}")

    {:ok, transfer} =
      tranasction
      |> Exnem.DTO.parse_transaction()

    transfer.transaction.message.payload
    |> :zlib.gunzip()
  end
end
