defmodule Core.Demo.Pin do
  @doc """
  Generates 4-character pin codes from the Apsáalooke alphabet, excluding double vowels
  """
  @code_length 4

  def generate() do
    1..@code_length
    |> Enum.map(&generator(alphabet(), &1))
    |> Enum.join("")
  end

  def total_codes(alphabet) do
    alphabet_size = length(alphabet)
    word_size = @code_length

    :math.pow(alphabet_size, word_size)
    |> round
  end

  defp letters() do
    ["A", "E", "I", "O", "U", "B", "D", "H", "K", "L", "M", "N", "P", "S", "T", "W", "X"]
  end

  # defp missing_letters() do
  #   ["C", "F", "G", "J", "Q", "R", "V", "Y", "Z"]
  # end

  def alphabet() do
    letters()
  end

  defp generator(alphabet, _) do
    Enum.random(alphabet)
  end

  # ----------------------------------------------------------------------

  import Ecto.Query, warn: false
  alias Core.Repo

  def unique_pin(where) do
    generate()
    |> unique_pin(where)
  end

  defp unique_pin(<<_, _, _, _>> = pin, where) do
    pin
    |> check_pin(where)
    |> case do
      true ->
        pin

      false ->
        generate()
        |> unique_pin(where)
    end
  end

  defp check_pin(p, where) do
    from(i in where, where: i.pin == ^p, select: "1")
    |> Repo.all()
    |> case do
      [] ->
        true

      _ ->
        false
    end
  end
end
