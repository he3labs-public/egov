defmodule Core.Demo.Seed do
  alias Core.Demo.Seed
  alias Core.Account
  alias Core.Banking
  alias Core.Factory
  alias Core.Voting

  def identities(count) when is_integer(count) do
    Enum.map(1..count, fn _ ->
      {:ok, identity} = Account.create_identity(Factory.params_for(:identity))
      identity
    end)
  end

  def shopping() do
    Seed.Shopping.create_store_account()
    Seed.Shopping.create_products()
  end

  def election(hours: duration) do
    additional_minutes = duration * 60
    end_date = Core.naive_now(additional_minutes)

    Factory.params_for(:voting_election, end_date: end_date)
    |> Voting.create_election!()
    |> Voting.finalize_election()
  end

  def referenda(hours: duration) do
    additional_minutes = duration * 60
    end_date = Core.naive_now(additional_minutes)

    Factory.params_for(:voting_referenda, end_date: end_date)
    |> Voting.create_referenda!()
    |> Voting.finalize_referenda()
  end
end

defmodule Core.Demo.Seed.Shopping do
  alias Core.Banking
  alias Core.Shopping

  def create_store_account do
    Shopping.new_store_account_attrs()
    |> Banking.create_account(broadcast: false)
    |> case do
      {:ok, account} ->
        account

      {:error, changeset} ->
        reason = EctoHelper.pretty_error_sentence(changeset.errors)
        raise "Problem creating the Store account: #{reason}"
    end
  end

  def create_products() do
    Enum.map(list_products(), fn attrs ->
      attrs
      |> Shopping.create_product()
      |> case do
        {:ok, product} ->
          product

        {:error, changeset} ->
          reason = EctoHelper.pretty_error_sentence(changeset.errors)
          raise "Problem creating a Shopping product: #{reason}"
      end
    end)
  end

  def list_products() do
    [
      %{id: "00000000-0000-0000-0000-000000000001", name: "Item 1", price: 100},
      %{id: "00000000-0000-0000-0000-000000000002", name: "Item 2", price: 200},
      %{id: "00000000-0000-0000-0000-000000000003", name: "Item 3", price: 300},
      %{id: "00000000-0000-0000-0000-000000000004", name: "Item 4", price: 400},
      %{id: "00000000-0000-0000-0000-000000000005", name: "Item 5", price: 500},
      %{id: "00000000-0000-0000-0000-000000000006", name: "Item 6", price: 600},
      %{id: "00000000-0000-0000-0000-000000000007", name: "Item 7", price: 700},
      %{id: "00000000-0000-0000-0000-000000000008", name: "Item 8", price: 800},
      %{id: "00000000-0000-0000-0000-000000000009", name: "Item 9", price: 900},
      %{id: "00000000-0000-0000-0000-00000000000a", name: "Item 10", price: 1000},
      %{id: "00000000-0000-0000-0000-00000000000b", name: "Item 11", price: 1100},
      %{id: "00000000-0000-0000-0000-00000000000c", name: "Item 12", price: 1200},
      %{id: "00000000-0000-0000-0000-00000000000d", name: "Item 13", price: 1300},
      %{id: "00000000-0000-0000-0000-00000000000e", name: "Chicken Noodle Soup", price: 75},
      %{id: "00000000-0000-0000-0000-00000000000f", name: "Toy Truck", price: 550},
      %{id: "00000000-0000-0000-0000-000000000010", name: "Item 16", price: 1600},
      %{id: "00000000-0000-0000-0000-000000000011", name: "Coca-Cola", price: 100},
      %{id: "00000000-0000-0000-0000-000000000012", name: "Dawn Dish Detergent", price: 250},
      %{id: "00000000-0000-0000-0000-000000000013", name: "Speedstick Deodorant", price: 350},
      %{id: "00000000-0000-0000-0000-000000000014", name: "Facial Tissues", price: 85},
      %{id: "00000000-0000-0000-0000-000000000015", name: "Altoids", price: 95},
      %{id: "00000000-0000-0000-0000-000000000016", name: "Ground Coffee", price: 325},
      %{id: "00000000-0000-0000-0000-000000000017", name: "Pringles", price: 225},
      %{id: "00000000-0000-0000-0000-000000000018", name: "Crest Toothpaste", price: 175},
      %{id: "00000000-0000-0000-0000-000000000019", name: "Item 25", price: 2500},
      %{id: "00000000-0000-0000-0000-00000000001a", name: "40 Sheet Notebook", price: 125},
      %{id: "00000000-0000-0000-0000-00000000001b", name: "Item 27", price: 2700},
      %{id: "00000000-0000-0000-0000-00000000001c", name: "Item 28", price: 2800},
      %{id: "00000000-0000-0000-0000-00000000001d", name: "Item 29", price: 2900},
      %{id: "00000000-0000-0000-0000-00000000001e", name: "Item 30", price: 3000},
      %{id: "00000000-0000-0000-0000-00000000001f", name: "Item 31", price: 3100},
      %{id: "00000000-0000-0000-0000-000000000020", name: "Item 32", price: 3200},
      %{id: "00000000-0000-0000-0000-000000000021", name: "Item 33", price: 3300},
      %{id: "00000000-0000-0000-0000-000000000022", name: "Item 34", price: 3400},
      %{id: "00000000-0000-0000-0000-000000000023", name: "Item 35", price: 3500},
      %{id: "00000000-0000-0000-0000-000000000024", name: "Item 36", price: 3600},
      %{id: "00000000-0000-0000-0000-000000000025", name: "Item 37", price: 3700},
      %{id: "00000000-0000-0000-0000-000000000026", name: "Item 38", price: 3800},
      %{id: "00000000-0000-0000-0000-000000000027", name: "Item 39", price: 3900},
      %{id: "00000000-0000-0000-0000-000000000028", name: "Item 40", price: 4000}
    ]
  end

  def generate_blank_products() do
    Enum.map(33..40, fn x ->
      {:ok, id} = <<x::unsigned-128>> |> Ecto.UUID.load()

      %{
        id: id,
        name: "Item #{x}",
        price: x * 100
      }
    end)
  end
end
