defmodule Core.Repo do
  use Ecto.Repo, otp_app: :core

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    {:ok, Keyword.put(opts, :url, System.get_env("DATABASE_URL"))}
  end

  if Mix.env() in [:dev, :test] do
    @spec truncate(Ecto.Schema.t) :: {:ok, map} | {:error, Exception.t}
    def truncate(schema) do
      table_name = schema.__schema__(:source)
      query("TRUNCATE #{table_name}", [])
    end
  end
end
