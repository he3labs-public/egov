defmodule Core.Demo.Reset do
  alias Core.Activity
  alias Core.Demo.Reset.Worker

  def run() do
    {:ok, pid} = Worker.start([self()])
    Worker.perform(pid)

    {:ok, pid}
  end
end

defmodule Core.Demo.Reset.Worker do
  @moduledoc """
  Worker that performs the reset actions.
  """
  use GenServer

  alias Core.Repo
  alias Core.Activity

  @schemas [
    Core.Demo.BankAccount.Seed,
    Core.Banking.Transaction,
    Core.Banking.Account,
    Core.Voting.Confirmation,
    Core.Voting.Election.Participation,
    Core.Voting.Election.Seed,
    Core.Voting.Election,
    Core.Voting.Referenda.Participation,
    Core.Voting.Referenda.Seed,
    Core.Voting.Referenda,
    Core.Voting.Registration,
    Core.Account.ApiToken,
    Core.Account.Identity,
    Core.Shopping.Product,
    Core.NemAccount,
    Core.NemTransaction,
  ]

  ##
  ## Client
  ##
  def start([parent]) do
    GenServer.start(__MODULE__, %{parent: parent}, name: __MODULE__)
  end

  def start_link([parent]) do
    GenServer.start_link(__MODULE__, %{parent: parent}, name: __MODULE__)
  end

  def init(opts) when is_map(opts) do
    {:ok, opts}
  end

  def perform(worker) do
    GenServer.call(worker, :perform)
  end

  @spec empty_all(list(Ecto.Schema.t)) :: list(tuple())
  def empty_all(schemas) when is_list(schemas) do
    log("Emptying all tables")
    Enum.map(schemas, &empty_schema(&1))
  end

  defp empty_schema(schema) do
    with {num_rows, nil} <- Repo.delete_all(schema) do
      {:ok, num_rows}
    else
      error ->
        log("Error emptying '#{source(schema)}'")
        error
    end
  end

  @spec source(Ecto.Schema.t) :: any
  defp source(schema) do
    schema.__schema__(:source)
  end

  def schemas() do
    @schemas
  end

  ##
  ## Server
  ##
  def handle_call(:perform, _from, state) do
    log("Reset initiated")

    empty_all(schemas())

    number_identities = 4
    log("Creating #{number_identities} identities")
    _identities = Core.Demo.Seed.identities(number_identities)

    log("Creating Election (duration 4 hours)")
    Core.Demo.Seed.election(hours: 4)

    log("Creating Referenda (duration 4 hours)")
    Core.Demo.Seed.referenda(hours: 4)

    log("Setting up Shopping products")
    Core.Demo.Seed.shopping()

    Process.send_after(self(), :stop, 30_000)

    {:reply, :ok, state}
  end

  def handle_info(:stop, state) do
    log("All done")
    {:stop, :normal, state}
  end

  defp log(message) do
    Activity.log("[Reset] #{message}")
  end
end
