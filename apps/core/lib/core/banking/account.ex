defmodule Core.Banking.Account do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  @account_types ["citizen", "reserve", "store", "provision", "approver"]

  schema "bank_accounts" do
    field :first_name, :string
    field :last_name, :string
    field :id_number, :binary_id
    field :account_type, :string, default: "citizen"

    belongs_to(:nem_account, Core.NemAccount, references: :address, type: :string)

    timestamps()
  end

  @doc false
  def changeset(account, attrs \\ %{}) do
    account
    |> cast(attrs, [:first_name, :last_name, :id_number, :account_type, :nem_account_id])
    |> assoc_constraint(:nem_account)
    |> validate_required([:first_name, :last_name, :id_number, :nem_account_id])
    |> validate_inclusion(:account_type, @account_types)
  end

  def insert_changeset(attrs \\ %{}) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end
