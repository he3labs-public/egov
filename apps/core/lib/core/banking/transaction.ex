defmodule Core.Banking.Transaction do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  @statuses ["pending", "failed", "completed"]

  schema "bank_transactions" do
    belongs_to(:from, Core.Banking.Account, references: :id_number)
    belongs_to(:to, Core.Banking.Account, references: :id_number)
    belongs_to(:nem_transaction, Core.NemTransaction, references: :tx_hash, type: :string)

    field(:description, :string)
    field(:amount, :integer)
    field(:status, :string, default: "pending")

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:description, :amount, :from_id, :to_id, :nem_transaction_id, :status])
    |> validate_required([:description, :amount, :from_id, :to_id, :status])
    |> validate_number(:amount, greater_than: 0)
    |> validate_inclusion(:status, @statuses)
    |> assoc_constraint(:from)
    |> assoc_constraint(:to)
    |> assoc_constraint(:nem_transaction)
  end

  @doc """
  Determine whether the given transaction is a positive or negative value relative to the given person `id_number`.

  If the person is not a member of the transaction, an error is raised.
  """
  def relative_amount(%__MODULE__{} = transaction, id_number) do
    cond do
      sender?(transaction, id_number) ->
        -transaction.amount

      recipient?(transaction, id_number) ->
        transaction.amount

      true ->
        raise "Cannot determine plus or minus value of transaction for user #{id_number}: #{
                inspect(transaction)
              }"
    end
  end

  @doc """
  Describe a transaction to one of the participants.

  Given a transaction from Jeremy Ping to Tom Feld

  If the viewer is the sender:
      Transaction to Tom Feld

  If the viewer is the receiver:
      Transaction from Jeremy Ping

  If the viewer is neither sender nor receiver:
      Transaction from Tom Feld to Jeremy Ping
  """
  def describe_to(transaction, viewer_id) do
    {direction, name} =
      cond do
        transaction.from_id == viewer_id ->
          {"to", display_name(transaction.to)}

        transaction.to_id == viewer_id ->
          {"from", display_name(transaction.from)}

        true ->
          {"from", "#{display_name(transaction.from)} to #{display_name(transaction.to)}"}
      end

    "Transfer #{direction} #{name}"
  end

  defp display_name(account) do
    "#{account.first_name} #{account.last_name}"
  end

  @doc """
  Checks if transaction is complete.
  """
  def complete?(%__MODULE__{} = transaction) do
    "completed" === transaction.status
  end

  @doc """
  Checks if `id_number` is the sender or receiver of a transaction.
  """
  def member?(%__MODULE__{from_id: from, to_id: to}, id_number) do
    [from, to]
    |> Enum.member?(id_number)
  end

  @doc """
  Checks if `id_number` is the sender of a transaction.
  """
  def sender?(%__MODULE__{from_id: from}, id_number) do
    id_number === from
  end

  @doc """
  Checks if `id_number` is the recipient of a transaction.
  """
  def recipient?(%__MODULE__{to_id: to}, id_number) do
    id_number === to
  end
end
