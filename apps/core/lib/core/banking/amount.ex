defmodule Core.Banking.Amount do
  def string_to_integer(value) when is_binary(value) do

    with {float, _} <- Float.parse(value) do
      Kernel.round(float * 100)
    end
  end

  def integer_to_string(value) when is_integer(value) do
    {whole_int, "." <> remainder} =
      (value / 100)
      |> to_string
      |> Integer.parse()

    zero_padded_remainder =
      remainder
      |> String.pad_trailing(2, "0")

    "#{whole_int}.#{zero_padded_remainder}"
  end
end
