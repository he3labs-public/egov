defmodule Core.Banking.Transaction.Worker do
  use GenServer
  require Logger

  @log_tag "[Banking Transaction Worker]"

  alias Core.Activity
  alias Core.Banking
  alias Core.Banking.Transaction

  ##
  ## Client
  ##

  def start(%Transaction{id: id}) do
    GenServer.start(__MODULE__, id)
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  @doc """
  Announce and monitor a single banking transaction.

  If the transaction has not been announced, announce it, and monitor wait for confirmation.
  If the transaction has been announced, check the status and wait for confirmation if it is unconfirmed.
  """
  def init(transaction_id) do
    transaction = Banking.get_transaction!(transaction_id)
    from_address = transaction.from.nem_account_id
    recipient = transaction.to.nem_account_id

    info_log("Starting for Transaction #{transaction.id}...")

    {:ok, pid} = start_observer(from_address)

    state = %{
      observer: pid,
      transaction_id: transaction_id,
      from_address: from_address,
      recipient: recipient
    }

    setup_timeout()

    {:ok, state}
  end

  def terminate(_reason, state) do
    stop_observer(state)
    :ok
  end

  def completed(pid, data) do
    GenServer.call(pid, {:completed, data})
  end

  def fail(pid, status) do
    GenServer.call(pid, {:fail, status})
  end

  ##
  ## Server
  ##

  def handle_info("observerReady", state) do
    info_log("Observer ready, now work can begin...")

    send(self(), :initial_action)

    {:noreply, state}
  end

  def handle_info(:initial_action, %{transaction_id: transaction_id} = state) do
    transaction = Banking.get_transaction!(transaction_id)

    case transaction.nem_transaction_id do
      nil ->
        Banking.announce_transaction(state.transaction_id)
        {:noreply, state}

      _hash ->
        updateStatusFromApi(state)
        {:noreply, state}
    end
  end

  # "unconfirmedAdded"  "unconfirmedRemoved"

  def handle_info({"confirmedAdded", _dto}, state) do
    Banking.complete_transaction(state.transaction_id)
    {:noreply, state}
  end

  def handle_info({"error", %{status: status}}, state) do
    Banking.fail_transaction(state.transaction_id, status)

    {:stop, :normal, state}
  end

  def handle_info({_, _dto}, state), do: {:noreply, state}

  ##
  ## Helpers
  ##

  defp start_observer(nem_address) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.start_link(nem_address, self())
  end

  defp stop_observer(%{observer: pid}) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.stop(pid)
  end

  defp setup_timeout do
    # TODO: Setup a final timeout @ 60 seconds
    #       When timeout is reached, double-check status via API.
    nil
  end

  defp updateStatusFromApi(_state) do
    # TODO: Read NEM tx status via api
    #     unconfirmed: wait
    #     confirmed: success / update db and Core.broadcast
    #     not found: failure / update db and Core.broadcast
    nil
  end

  def handle_call({:completed, _dto}, _from, state) do
    {:reply, Banking.complete_transaction(state.transaction_id), state}
  end

  def handle_call({:fail, status}, _from, state) do
    {:reply, Banking.fail_transaction(state.transaction_id, status), state}
  end

  def handle_call(:stop, _from, state) do
    {:stop, :normal, state}
  end

  defp info_log(message) do
    Logger.info(@log_tag <> " #{message}")
    Activity.log(@log_tag <> " #{message}")
  end
end
