defmodule Core.Banking do
  @moduledoc """
  The Banking context.
  """

  require Logger

  import Ecto.Query, warn: false
  alias Ecto.Multi

  alias Core.Repo
  alias Core.Banking.{Account, Amount}
  alias Core.{NemAccount, NemTransaction}

  @doc """
  Returns the list of bank_accounts.

  ## Examples

      iex> list_accounts()
      [%Account{}, ...]

  """
  def list_accounts do
    Repo.all(Account) |> Repo.preload(:nem_account)
  end

  def list_citizen_accounts do
    from(a in Account, where: [account_type: "citizen"])
    |> Repo.all()
    |> Repo.preload(:nem_account)
  end

  def lookup(id_number) do
    Repo.get_by(Account, id_number: id_number)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id) |> Repo.preload(:nem_account)

  def get_account_by(opts) do
    Account
    |> Repo.get_by(opts)
    |> case do
      nil ->
        {:error, "Account not found: #{inspect(opts)}"}

      account ->
        {:ok, account |> Repo.preload(:nem_account)}
    end
  end

  @doc """
  Creates a account.

  ## Examples

      iex> create_account(%{first_name: "John", last_name: "Doe", id_number: "1234-abc-893"})
      {:ok, %Account{}}

      iex> create_account(%{first_name: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_account(params \\ %{}, opts \\ []) do
    broadcast = Keyword.get(opts, :broadcast, true)

    multi =
      Multi.new()
      |> Multi.insert(
        :nem_account,
        case Map.get(params, :nem_account) do
          nil ->
            NemAccount.generate_key_pair()

          nem_acct ->
            nem_acct
        end
        |> NemAccount.insert_changeset()
      )
      |> Multi.run(
        :account,
        &insert_account(&1, params)
      )

    case Repo.transaction(multi) do
      {:ok, %{account: account}} ->
        account = account |> Repo.preload(:nem_account)

        if broadcast do
          Core.broadcast("banking:account_created", account)
        end

        {:ok, account}

      {:error, :nem_account, changeset, _} ->
        if broadcast do
          Core.broadcast("banking:account_creation_failed", changeset)
        end

        {:error, changeset}

      {:error, :account, changeset, _} ->
        if broadcast do
          Core.broadcast("banking:account_creation_failed", changeset)
        end

        {:error, changeset}
    end
  end

  @doc """
  Updates a account.

  ## Examples

      iex> update_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_account(%Account{} = account, attrs) do
    account
    |> Account.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Account.

  ## Examples

      iex> delete_account(account)
      {:ok, %Account{}}

      iex> delete_account(account)
      {:error, %Ecto.Changeset{}}

  """
  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{source: %Account{}}

  """
  def change_account(%Account{} = account) do
    Account.changeset(account, %{})
  end

  defp insert_account(%{nem_account: %NemAccount{address: nem_account_id}}, attrs) do
    %Account{nem_account_id: nem_account_id}
    |> Account.changeset(attrs)
    |> Repo.insert()
  end

  alias Core.Banking.Transaction

  @doc """
  Returns the list of transactions.

  ## Examples

      iex> list_transactions()
      [%Transaction{}, ...]

  """
  def list_transactions do
    Repo.all(Transaction)
    |> Repo.preload(from: [:nem_account], to: [:nem_account])
  end

  @doc """
  Gets a single transaction.

  Raises `Ecto.NoResultsError` if the Transaction does not exist.

  ## Examples

      iex> get_transaction!(123)
      %Transaction{}

      iex> get_transaction!(456)
      ** (Ecto.NoResultsError)

  """
  def get_transaction!(id) do
    Repo.get!(Transaction, id)
    |> Repo.preload(from: [:nem_account], to: [:nem_account])
  end

  def get_transactions_for(id) do
    from(
      t in Transaction,
      where: t.from_id == ^id,
      or_where: t.to_id == ^id,
      order_by: [desc: :inserted_at]
    )
    |> Repo.all()
    |> Repo.preload([:from, :to])
  end

  @doc """
  Creates a transaction.

  ## Examples

      iex> create_transaction(%{field: value})
      {:ok, %Transaction{}}

      iex> create_transaction(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_transaction(attrs \\ %{}) do
    changeset =
      %Transaction{}
      |> Transaction.changeset(attrs)

    with {:ok, transaction} <- Repo.insert(changeset) do
      transaction = Repo.preload(transaction, [:from, :to])

      if transaction.from.nem_account_id == nil do
        raise "'From' is trying to transact with a 'nil' nem account id"
      end

      if transaction.to.nem_account_id == nil do
        raise "'To' is trying to transact with a 'nil' nem account id"
      end

      Core.broadcast("banking:transaction_created", transaction)

      if worker_enabled?() do
        {:ok, _pid} = Transaction.Worker.start(transaction)
      end

      #     + params - banking transaction
      #     + starts nem account observer
      #     + looks at status of bank transaction
      #         new: generates nem transaction and announces
      #         pending: reads nem tx status via api
      #             unconfirmed: wait
      #             confirmed: success / update db and Core.broadcast
      #             not found: failure / update db and Core.broadcast

      {:ok, transaction}
    end
  end

  @doc """
  Updates a transaction.

  ## Examples

      iex> update_transaction(transaction, %{field: new_value})
      {:ok, %Transaction{}}

      iex> update_transaction(transaction, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_transaction(%Transaction{} = transaction, attrs) do
    transaction
    |> Transaction.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Transaction.

  ## Examples

      iex> delete_transaction(transaction)
      {:ok, %Transaction{}}

      iex> delete_transaction(transaction)
      {:error, %Ecto.Changeset{}}

  """
  def delete_transaction(%Transaction{} = transaction) do
    Repo.delete(transaction)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking transaction changes.

  ## Examples

      iex> change_transaction(transaction)
      %Ecto.Changeset{source: %Transaction{}}

  """
  def change_transaction(%Transaction{} = transaction) do
    Transaction.changeset(transaction, %{})
  end

  def complete_transaction(transaction_id) do
    Logger.info("Transaction confirmation received, updating records...")

    bank_tx =
      get_transaction!(transaction_id)
      |> Repo.preload(:nem_transaction)

    nem_tx =
      bank_tx.nem_transaction
      |> NemTransaction.confirmed_changeset()
      |> Repo.update!()

    bank_tx = %{bank_tx | nem_transaction: nem_tx}

    {:ok, updated_bank_tx} = update_transaction(bank_tx, %{status: "completed"})

    Logger.info("Records updated, transaction complete")
    Core.broadcast("banking:transaction_completed", updated_bank_tx)

    updated_bank_tx
  end

  def fail_transaction(transaction_id, status) do
    bank_tx =
      get_transaction!(transaction_id)
      |> Repo.preload(:nem_transaction)

    nem_tx =
      bank_tx.nem_transaction
      |> NemTransaction.failed_changeset(status)
      |> Repo.update!()

    bank_tx = %{bank_tx | nem_transaction: nem_tx}

    with {:ok, updated_bank_tx} <- update_transaction(bank_tx, %{status: "failed"}) do
      Logger.info("Transaction Error: #{status}")
      Core.broadcast("banking:transaction_failed", updated_bank_tx)

      {:ok, updated_bank_tx}
    end
  end

  def announce_transaction(transaction_id) do
    transaction =
      get_transaction!(transaction_id)
      |> Repo.preload([:to, :from])

    {:ok, mosaic} = Exnem.Transaction.mosaic(Core.Demo.bank_mosaic(), transaction.amount)

    to_address = transaction.to.nem_account.address

    {:ok, transfer_transaction} =
      Exnem.Transaction.transfer(to_address, [mosaic], message: transaction.description)

    from_account = transaction.from.nem_account

    verf_tx =
      transfer_transaction
      |> Exnem.Transaction.pack()
      |> Exnem.Crypto.KeyPair.sign(from_account)

    case Exnem.Announce.announce(verf_tx) do
      :ok ->
        original_nem_tx =
          NemTransaction.announced_changeset(verf_tx.hash)
          |> Repo.insert!()

        {:ok, updated_transaction} =
          update_transaction(transaction, %{
            status: "pending",
            nem_transaction_id: original_nem_tx.tx_hash
          })

        updated_transaction = updated_transaction |> Repo.preload(:nem_transaction)

        Core.broadcast(
          "banking:transaction_announced",
          updated_transaction |> Repo.preload(:nem_transaction)
        )

        {:ok, updated_transaction}

      {:error, _} ->
        update_transaction(transaction, %{status: "failed"})
    end
  end

  @doc """
  Transfer funds from one account to another. From and To params are the id
  numbers of the respective parties. Both parties must have valid bank accounts
  or the transfer will result in an error.

  Params:

  from   (binary) - Id number of the person sending the money
  to     (binary) - Id number of the person receiving the money
  amount (binary) - Amount to send, e.g. "1.00"

  ## Examples

    > transfer(from, to, amount)
    {:ok, %Transaction{}}

    > transfer(from, to, "-1")
    {:error, "Amount is not valid"}
  """
  def transfer(from, to, amount) when is_binary(from) and is_binary(to) and is_binary(amount) do
    with {:ok, _sender} <- get_account_by(id_number: from),
         {:ok, _recipient} <- get_account_by(id_number: to) do
      %{
        from_id: from,
        to_id: to,
        description: "Transfer ",
        amount: Amount.string_to_integer(amount)
      }
      |> create_transaction()
      |> case do
        {:ok, transaction} ->
          {:ok, transaction}

        {:error, changeset} ->
          reason = EctoHelper.pretty_error_sentence(changeset.errors)

          {:error, reason}
      end
    end
  end

  @doc """
  Calculate an account balance given a list of transactions.
  """
  def calculate_balance(transactions, id_number)
      when is_list(transactions) and is_binary(id_number) do
    transactions
    |> Enum.filter(&Transaction.complete?(&1))
    |> Enum.map(&Transaction.relative_amount(&1, id_number))
    |> Enum.sum()
  end

  @doc false
  defp worker_enabled?() do
    Application.fetch_env!(:core, :banking_worker_enabled)
  end

  def query_accounts_by_type(type) when is_binary(type) do
    from(
      a in Account,
      where: [account_type: ^type],
      order_by: [desc: :inserted_at]
    )
  end

  def query_accounts_by_type(types) when is_list(types) do
    from(
      a in Account,
      where: a.account_type in ^types,
      order_by: [desc: :inserted_at]
    )
  end
end
