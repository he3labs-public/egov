defmodule Core.NemAccount do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:address, :string, autogenerate: false}
  @foreign_key_type :string

  schema "nem_accounts" do
    field(:private_key, :string)
    field(:public_key, :string)

    timestamps()
  end

  @doc false
  def changeset(nem_account, attrs \\ %{}) do
    nem_account
    |> cast(attrs, [:public_key, :private_key, :address])
    |> validate_required([:public_key, :private_key, :address])
  end

  @doc false
  def insert_changeset(attrs \\ %{}) do
    %__MODULE__{}
    |> changeset(attrs)
  end

  def generate_key_pair() do
    Exnem.Crypto.KeyPair.generate(Exnem.Config.network_type())
  end
end
