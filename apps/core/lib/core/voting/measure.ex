defmodule Core.Voting.Measure do
  use Ecto.Schema
  import Ecto.Changeset
  alias Core.Voting.Measure

  embedded_schema do
    field :name, :string
    field :overview, :string
    field :title, :string
  end

  @doc false
  def changeset(%Measure{} = measure, attrs) do
    measure
    |> cast(attrs, [:name, :title, :overview])
    |> validate_required([:name, :title, :overview])
  end
end
