defmodule Core.Voting.Validation do
  alias Ecto.Changeset

  def validate_proper_date_range(changeset, start_field, end_field) do
    if changeset.valid? do
      if :lt ==
           NaiveDateTime.compare(
             Changeset.get_field(changeset, start_field),
             Changeset.get_field(changeset, end_field)
           ) do
        changeset
      else
        Changeset.add_error(changeset, :start_date, "Start date cannot be after the end date.")
      end
    else
      changeset
    end
  end
end
