defmodule Core.Voting.Election.Seed do
  use Ecto.Schema
  import Ecto.Changeset

  @max_ballots 1_000_000_000

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "election_seeds" do
    field(:mosaic_id, :string)
    field(:mosaic_amount, :integer)
    field(:duration, :integer)
    field(:namespace_id, :string)
    field(:nem_address, :string)
    field(:election_id, :binary_id)

    belongs_to(:seed_transaction, Core.NemTransaction, references: :tx_hash, type: :string)

    timestamps()
  end

  def max_ballots() do
    @max_ballots
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [
      :mosaic_id,
      :namespace_id,
      :nem_address,
      :election_id,
      :duration,
      :seed_transaction_id
    ])
    |> assoc_constraint(:seed_transaction)
    |> validate_required([:mosaic_id, :namespace_id, :nem_address, :election_id, :duration])
    |> put_change(:mosaic_amount, @max_ballots)
  end

  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end
