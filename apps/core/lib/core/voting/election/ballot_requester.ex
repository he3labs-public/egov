defmodule Core.Voting.Election.BallotRequester do
  use GenServer
  require Logger

  alias Exnem.Transaction
  alias Exnem.Crypto.KeyPair
  alias Core.Voting.Election
  alias Core.{Repo, NemAccount, NemTransaction}

  defmodule State do
    defstruct [:seed, :observer]
  end

  def start(%Election.Participation{} = seed) do
    GenServer.start(__MODULE__, seed)
  end

  def init(%Election.Participation{} = seed) do
    {:ok, pid} = start_observer(seed.ballot_address)
    seed = seed |> Repo.preload(election: :nem_account)

    {:ok, %State{seed: seed, observer: pid}}
  end

  def handle_info("observerReady", state) do
    updated_seed = announce_chit_receive(state.seed)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"error", error}, state) do
    error_meaning = Exnem.StatusErrors.meaning(error.status)
    Logger.info("[Election.BallotRequester] Request failed #{error.status}: #{error_meaning}")

    updated_seed = handle_error(state.seed, error)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def handle_info({"unconfirmedAdded", dto}, state) do
    updated_seed = handle_unconfirmed(state.seed, dto)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"confirmedAdded", dto}, state) do
    Logger.info("[Election.BallotRequester] Request complete.")
    updated_seed = handle_confirmed(state.seed, dto)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def terminate(_reason, state) do
    stop_observer(state)
    :ok
  end

  defp handle_error(seed, error) do
    updated_tx =
      seed.request_transaction
      |> NemTransaction.failed_changeset(error.status)
      |> Repo.update!()

    updated_seed = %{seed | request_transaction: updated_tx}

    Core.broadcast("voting:ballot_request_failed", updated_seed)

    updated_seed
  end

  defp handle_unconfirmed(seed, _dto) do
    seed
  end

  defp handle_confirmed(seed, _dto) do
    seed =
      seed
      |> Repo.preload(:request_transaction)

    seed.request_transaction
    |> NemTransaction.confirmed_changeset()
    |> Repo.update()
    |> case do
      {:ok, updated_tx} ->
        updated_seed = %{seed | request_transaction: updated_tx}

        Core.broadcast("ballot_ready", updated_seed)

        updated_seed

      _ ->
        Core.broadcast("voting:ballot_request_failed", seed)

        seed
    end
  end

  defp announce_chit_receive(
         %Election.Participation{
           voter_id: _voter_id,
           election: %Election{id: namespace, nem_account: %NemAccount{} = election_nem_account},
           ballot_address: voter_nem_address
         } = seed
       ) do
    mosaic_id = Exnem.mosaic_id(namespace <> ":election_chit")
    {:ok, mosaic} = Transaction.mosaic(mosaic_id, 1)

    {:ok, transfer_transaction} =
      Transaction.transfer(voter_nem_address, [mosaic], message: "Demo Ballot Request")

    verf_tx =
      transfer_transaction
      |> Transaction.pack()
      |> KeyPair.sign(election_nem_account)

    Exnem.Announce.announce(verf_tx)

    nem_tx =
      verf_tx.hash
      |> NemTransaction.announced_changeset()
      |> Repo.insert!()

    seed
    |> Election.Participation.changeset(%{request_transaction_id: nem_tx.tx_hash})
    |> Repo.update()
    |> case do
      {:ok, updated_seed} ->
        Core.broadcast("voting:ballot_request_announced", updated_seed)
        updated_seed

      _ ->
        Core.broadcast("voting:ballot_request_failed", seed)
        seed
    end
  end

  defp start_observer(nem_address) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.start_link(nem_address, self())
  end

  defp stop_observer(%State{observer: pid}) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.stop(pid)
  end
end
