defmodule Core.Voting.Election.Scheduler do
  use Task, restart: :transient

  alias Core.Activity
  alias Core.Voting.Election
  alias Core.Voting
  require Logger

  def start_link([]) do
    Task.start_link(__MODULE__, :run, [])
  end

  # When app starts up, this should be run
  def run() do
    info("Scheduler starting up ...")

    elections = Voting.list_elections_to_schedule()
    count = length(elections)

    info("Found #{count} to schedule")
    Enum.map(elections, &schedule/1)

    count
  end

  def schedule(election = %Election{}) do
    # actually, ~30 sec before start_date
    start = DateTime.from_naive!(election.start_date, "Etc/UTC")
    info("Scheduling Election #{election.id} at #{inspect start} (current time #{inspect DateTime.utc_now()})")

    {:ok, pid} = SchedEx.run_at(Voting, :open_election, [election.id], start)

    stats = SchedEx.stats(pid)
    Logger.debug("[Election.Scheduler] Stats for Election #{election.id} (#{inspect pid}) are #{inspect stats}")

    {:ok, pid}
  end

  defp info(message) do
    message = "[Election.Scheduler] #{message}"
    Logger.debug(message)
    Activity.log(message)
  end
end
