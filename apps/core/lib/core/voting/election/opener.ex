defmodule Core.Voting.Election.Opener do
  use GenServer

  alias Core.Activity
  alias Core.Repo
  alias Core.Voting.Election

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(_) do
    Core.subscribe("voting:election_seeded")

    Activity.log("Election Opener ready")

    {:ok, %{}}
  end

  def handle_info({"voting:election_seeded", %Election.Seed{} = seed}, state) do
    Repo.get!(Election, seed.election_id)
    |> Election.opening_changeset()
    |> Repo.update()

    {:noreply, state}
  end
end
