defmodule Core.Voting.Election.Tally do
  alias Core.Repo
  alias Core.Voting.Election

  @zero_uuid "00000000-0000-0000-0000-000000000000"

  def run(%Election{} = election) do
    election = election |> Repo.preload(:nem_account)
    tally = initialize_election_tally_slate(election.candidates)

    get_possible_election_votes(election.id, election.nem_account)
    |> Enum.map(fn ballot ->
      ballot
      |> Core.Voting.unserialize_ballot()
      |> Map.fetch!("candidates")
      |> prepare_election_ballot(initialize_election_ballot(), tally)
    end)
    |> do_election_tally(tally)
  end

  def initialize_election_tally_slate(candidates) do
    Core.Voting.Candidate.positions()
    |> Enum.reduce(%{}, fn pos, acc ->
      possible_candidates =
        candidates
        |> Enum.filter(fn c -> c.position == pos end)
        |> Enum.map(fn c -> {c.id, %{name: c.name, count: 0}} end)
        |> Enum.concat([{@zero_uuid, %{name: "Abstained", count: 0}}])

      Map.put(acc, pos, Map.new(possible_candidates))
    end)
  end

  # Initializes one ballot with all abstains,
  # in the case where the data coming in does not contain an entry
  defp initialize_election_ballot do
    Core.Voting.Candidate.positions()
    |> Enum.map(fn pos ->
      {pos, @zero_uuid}
    end)
    |> Map.new()
  end

  defp get_possible_election_votes(namespace, nem_account) do
    path = "account/#{nem_account.public_key}/transactions/incoming?pageSize=200"
    mosaic_id = Exnem.mosaic_id(namespace <> ":election_chit")

    case Exnem.Node.get(path) do
      {:ok, transactions} ->
        transactions
        |> Enum.map(&Exnem.DTO.parse_transaction/1)
        |> Enum.filter(&Core.Voting.is_valid_vote?(nem_account, mosaic_id, &1))
        |> Enum.map(&Core.Voting.extract_transaction_payload/1)

      _ ->
        []
    end
  end

  defp prepare_election_ballot(ballot_candidates, empty_ballot, tally) do
    ballot_candidates
    |> Enum.reduce({:ok, empty_ballot}, fn candidate, acc ->
      %{"position" => position, "id" => id} = candidate

      with {:ok, curr_ballot} <- acc do
        case Map.get(tally, position) do
          nil ->
            {:error, "[Tally] Position '#{position}' not found"}

          map ->
            case Map.get(map, id) do
              nil ->
                {:error, "[Tally] Candidate Id '#{id}' not found"}

              _record ->
                updated_ballot = Map.update!(curr_ballot, position, fn _ -> id end)
                {:ok, updated_ballot}
            end
        end
      end
    end)
  end

  defp do_election_tally(ballots, tally) do
    ballots
    |> Enum.reduce({tally, 0, 0}, fn entry, {acc, errors, total} ->
      case entry do
        {:ok, ballot} ->
          updated_acc =
            ballot
            |> Enum.reduce(acc, fn {pos, id}, acc ->
              put_in(acc, [pos, id, :count], acc[pos][id][:count] + 1)
            end)

          {updated_acc, errors, total + 1}

        {:error, _string} ->
          {acc, errors + 1, total}
      end
    end)
  end
end
