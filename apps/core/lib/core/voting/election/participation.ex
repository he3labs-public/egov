defmodule Core.Voting.Election.Participation do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  alias Core.Account.Identity
  alias Core.Voting.Election
  alias Core.NemAccount

  schema "voting_election_participations" do
    belongs_to(:voter, Identity, type: :binary_id)
    belongs_to(:election, Election)

    belongs_to(
      :nem_account,
      NemAccount,
      foreign_key: :ballot_address,
      references: :address,
      type: :string
    )

    belongs_to(:request_transaction, Core.NemTransaction, references: :tx_hash, type: :string)
    belongs_to(:submit_transaction, Core.NemTransaction, references: :tx_hash, type: :string)

    field(:voted, :boolean, default: false)
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [
      :voter_id,
      :election_id,
      :ballot_address,
      :request_transaction_id,
      :submit_transaction_id
    ])
    |> validate_required([
      :voter_id,
      :election_id,
      :ballot_address
    ])
    |> assoc_constraint(:request_transaction)
    |> assoc_constraint(:submit_transaction)
    |> assoc_constraint(:voter)
    |> assoc_constraint(:election)
    |> assoc_constraint(:nem_account)
    |> unique_constraint(
      :voter_id,
      name: :voting_election_participations_voter_id_election_id_index
    )
  end

  def submit_changeset(changeset) do
    changeset
    |> cast(%{}, [])
    |> put_change(:voted, true)
  end
end
