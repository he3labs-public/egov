defmodule Core.Voting.Election.BallotRequest do
  use Ecto.Schema
  import Ecto.Changeset

  alias Core.Voting.Election
  alias Core.Repo

  embedded_schema do
    field(:election_id, :binary_id)
    field(:voter_id, :binary_id)
    field(:ballot_address, :string)
  end

  def new(attrs \\ %{}) do
    %__MODULE__{}
    |> changeset(attrs)
    |> apply_action(:insert)
  end

  def changeset(struct, attrs \\ %{}) do
    struct
    |> cast(attrs, [:election_id, :voter_id, :ballot_address])
    |> validate_required([:election_id, :voter_id, :ballot_address])
  end

  def validate(%__MODULE__{election_id: election_id, voter_id: voter_id} = request) do
    with {:ok, _request} <- check_valid(request),
         {:ok, election} <- check_if_exists(election_id),
         :ok <- check_if_open(election),
         :ok <- check_voter_eligible(election, voter_id),
         :ok <- check_participation(election, voter_id) do
      :ok
    end
  end

  defp check_valid(request) do
    changeset(request)
    |> apply_action(:insert)
  end

  defp check_if_exists(election_id) do
    case Repo.get(Election, election_id) do
      nil ->
        {:error, "Election not found"}

      election ->
        {:ok, election}
    end
  end

  defp check_if_open(election) do
    case Core.Voting.election_open?(election) do
      false ->
        {:error, "Election not running"}

      true ->
        :ok
    end
  end

  defp check_voter_eligible(election, voter_id) do
    if Core.Voting.voter_eligible?(election, voter_id) do
      :ok
    else
      {:error, "Voter not eligible"}
    end
  end

  defp check_participation(election, voter_id) do
    case Core.Voting.get_election_participation(election.id, voter_id) do
      _no_participation = nil ->
        :ok

      _participated ->
        {:error, "Ballot has already been sent"}
    end
  end
end
