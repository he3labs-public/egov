defmodule Core.Voting.Election.SubmissionPayload do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:election_id, :binary_id, autogenerate: false}
  
  embedded_schema do
    embeds_many(:candidates, Core.Voting.Election.SubmissionPayload.Candidate)
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:election_id])
    |> cast_embed(:candidates, required: true)
  end

  @doc false
  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end

defmodule Core.Voting.Election.SubmissionPayload.Candidate do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:candidate_id, :binary_id, autogenerate: true}
  embedded_schema do
    field(:id, :string)
    field(:name, :string)
    field(:position, :string)
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:id, :name, :position])
    |> validate_required([:id, :name, :position])
  end

  @doc false
  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end
