defmodule Core.Voting.Election.BallotSubmitter do
  use GenServer
  require Logger

  alias Exnem.Transaction
  alias Exnem.Crypto.KeyPair
  alias Core.Voting.{Election, Confirmation}
  alias Core.{Demo, Repo, NemAccount, NemTransaction}
  alias Ecto.Multi

  defmodule State do
    defstruct [:seed, :observer, :submission]
  end

  def start(%Election.Participation{} = seed, submission) do
    GenServer.start(__MODULE__, [seed, submission])
  end

  def init([%Election.Participation{} = seed, submission]) do
    {:ok, pid} = start_observer(seed.ballot_address)

    seed = seed |> Repo.preload(election: :nem_account)

    {:ok, %State{seed: seed, observer: pid, submission: submission}}
  end

  def handle_info("observerReady", state) do
    updated_seed = announce_chit_submit(state.seed, state.submission)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"error", error}, state) do
    error_meaning = Exnem.StatusErrors.meaning(error.status)
    Logger.info("[Election.BallotSubmitter] Seeding failed #{error.status}: #{error_meaning}")

    updated_seed = handle_error(state.seed, error)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def handle_info({"unconfirmedAdded", dto}, state) do
    updated_seed = handle_unconfirmed(state.seed, dto)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"confirmedAdded", dto}, state) do
    Logger.info("[Election.BallotSubmitter] Submission complete.")
    updated_seed = handle_confirmed(state.seed, dto)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def terminate(_reason, state) do
    stop_observer(state)
    :ok
  end

  defp handle_error(seed, error) do
    updated_tx =
      seed.submit_transaction
      |> NemTransaction.failed_changeset(error.status)
      |> Repo.update!()

    updated_seed = %{seed | submit_transaction: updated_tx}

    Core.broadcast("voting:ballot_submit_failed", updated_seed)

    updated_seed
  end

  defp handle_unconfirmed(seed, _dto) do
    seed
  end

  defp handle_confirmed(seed, _dto) do
    seed = seed |> Repo.preload(:submit_transaction)

    confirmation_params = %{
      election_id: seed.election_id,
      voter_id: seed.voter_id,
      pin: Demo.Pin.unique_pin("confirmations"),
      tx_hash: seed.submit_transaction_id
    }

    Multi.new()
    |> Multi.update(
      :submit_transaction,
      NemTransaction.confirmed_changeset(seed.submit_transaction)
    )
    |> Multi.insert(:confirmation, Confirmation.insert_changeset(confirmation_params))
    |> Repo.transaction()
    |> case do
      {:ok, %{submit_transaction: updated_tx, confirmation: confirmation}} ->
        Logger.debug(
          "[Election.BallotSubmitter] Submission succeeded for voter #{seed.voter_id}: #{
            confirmation.pin
          }."
        )

        direct_confirmation = confirmation |> Map.delete(:referenda_id)
        Core.broadcast("ballot_confirmed", direct_confirmation)

        %{seed | submit_transaction: updated_tx}

      {:error, :submit_transaction, changeset, _} ->
        reason = EctoHelper.pretty_error_sentence(changeset.errors)
        Logger.debug("[Election.BallotSubmitter] Failed to updated submit transaction: #{reason}")

        Core.broadcast("voting:ballot_submit_failed", seed)

        seed

      {:error, :confirmation, changeset, _} ->
        reason = EctoHelper.pretty_error_sentence(changeset.errors)
        Logger.debug("[Election.BallotSubmitter] Failed to insert confirmation record: #{reason}")

        Core.broadcast("voting:ballot_submit_failed", seed)

        seed
    end
  end

  defp announce_chit_submit(
         %{
           voter_id: _voter_id,
           election: %Election{id: namespace, nem_account: %NemAccount{} = election_nem_account},
           ballot_address: voter_nem_address
         } = seed,
         submission
       ) do
    voter_nem_account = Repo.get!(NemAccount, voter_nem_address)

    mosaic_id = Exnem.mosaic_id(namespace <> ":election_chit")
    {:ok, mosaic} = Transaction.mosaic(mosaic_id, 1)

    {:ok, transfer_transaction} =
      Transaction.transfer(election_nem_account.address, [mosaic], message: submission.ballot)

    verf_tx =
      transfer_transaction
      |> Transaction.pack()
      |> KeyPair.sign(voter_nem_account)

    Exnem.Announce.announce(verf_tx)

    nem_tx =
      verf_tx.hash
      |> NemTransaction.announced_changeset()
      |> Repo.insert!()

    seed
    |> Election.Participation.changeset(%{submit_transaction_id: nem_tx.tx_hash})
    |> Repo.update()
    |> case do
      {:ok, updated_seed} ->
        Core.broadcast("voting:ballot_submit_announced", updated_seed)

        updated_seed

      _ ->
        Core.broadcast("voting:ballot_submit_failed", seed)
        seed
    end
  end

  defp start_observer(nem_address) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.start_link(nem_address, self())
  end

  defp stop_observer(%State{observer: pid}) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.stop(pid)
  end
end
