defmodule Core.Voting.Candidate do
  use Ecto.Schema
  import Ecto.Changeset

  @positions ~w(Chairman Vice-Chairman Secretary Vice-Secretary)

  embedded_schema do
    field(:name, :string)
    field(:position, :string)
  end

  def positions() do
    @positions
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:name, :position])
    |> validate_required([:name, :position])
    |> validate_inclusion(:position, @positions)
  end

  @doc false
  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end
