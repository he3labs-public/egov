defmodule Core.Voting do
  @moduledoc """
  The Voting context.
  """

  import Ecto.Query, warn: false
  import Exnem.Transaction, only: [confirmed?: 1, recipient_equals?: 2, transfers_one_mosaic?: 2]
  alias Ecto.Multi
  alias Core.{NemAccount, Repo}
  alias Core.Voting.{Registration, Referenda, Election}
  require Logger

  @doc """
  Returns the list of voting_registrations.

  ## Examples

      iex> list_voting_registrations()
      [%Registration{}, ...]

  """
  def list_voting_registrations do
    Repo.all(Registration)
  end

  @doc """
  Gets a single registration.

  Raises `Ecto.NoResultsError` if the Registration does not exist.

  ## Examples

      iex> get_registration!(123)
      %Registration{}

      iex> get_registration!(456)
      ** (Ecto.NoResultsError)

  """
  def get_registration!(id), do: Repo.get!(Registration, id)

  @doc """
  Creates a registration.

  ## Examples

      iex> create_registration(%{field: value})
      {:ok, %Registration{}}

      iex> create_registration(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_registration(attrs \\ %{}) do
    result =
      %Registration{}
      |> Registration.changeset(attrs)
      |> Repo.insert()

    case result do
      {:ok, registration} ->
        Core.broadcast("voting:voter_registered", registration)

      {:error, changeset} ->
        Core.broadcast("voting:voter_registration_failed", changeset)
    end

    result
  end

  @doc """
  Updates a registration.

  ## Examples

      iex> update_registration(registration, %{field: new_value})
      {:ok, %Registration{}}

      iex> update_registration(registration, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_registration(%Registration{} = registration, attrs) do
    registration
    |> Registration.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Registration.

  ## Examples

      iex> delete_registration(registration)
      {:ok, %Registration{}}

      iex> delete_registration(registration)
      {:error, %Ecto.Changeset{}}

  """
  def delete_registration(%Registration{} = registration) do
    Repo.delete(registration)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking registration changes.

  ## Examples

      iex> change_registration(registration)
      %Ecto.Changeset{source: %Registration{}}

  """
  def change_registration(%Registration{} = registration) do
    Registration.changeset(registration, %{})
  end

  @doc """
  Returns the list of voting_elections.

  ## Examples

      iex> list_voting_elections()
      [%Election{}, ...]

  """
  def list_voting_elections do
    Repo.all(Election)
  end

  @doc """
  Gets a single election.

  Raises `Ecto.NoResultsError` if the Election does not exist.

  ## Examples

      iex> get_election!(123)
      %Election{}

      iex> get_election!(456)
      ** (Ecto.NoResultsError)

  """
  def get_election!(id), do: Repo.get!(Election, id)

  @doc """
  Creates a election.

  ## Examples

      iex> create_election(%{field: value})
      {:ok, %Election{}}

      iex> create_election(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_election(attrs \\ %{}) do
    %Election{}
    |> Election.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a election.

  ## Examples

      iex> update_election(election, %{field: new_value})
      {:ok, %Election{}}

      iex> update_election(election, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_election(%Election{} = election, attrs) do
    if election.finalized_at do
      {:error,
       election
       |> Election.changeset()
       |> Ecto.Changeset.add_error(
         :finalized_at,
         "This election has been finalized and may not be updated."
       )}
    else
      election
      |> Election.changeset(attrs)
      |> Repo.update()
    end
  end

  @doc """
  Deletes a Election.

  ## Examples

      iex> delete_election(election)
      {:ok, %Election{}}

      iex> delete_election(election)
      {:error, %Ecto.Changeset{}}

  """
  def delete_election(%Election{} = election) do
    Repo.delete(election)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking election changes.

  ## Examples

      iex> change_election(election)
      %Ecto.Changeset{source: %Election{}}

  """
  def change_election(%Election{} = election) do
    Election.changeset(election, %{})
  end

  # ----------------------------------------------------------------------

  @doc """
  Get the number of elections.
  """
  def count_elections() do
    Repo.aggregate(Election, :count, :id)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking election changes.
  """
  def list_elections_to_schedule do
    date = Core.naive_now()

    from(
      e in Election,
      where: not is_nil(e.finalized_at) and is_nil(e.opened_at) and e.start_date > ^date
    )
    |> Repo.all()
  end

  defp get_finalized_election(id) do
    election = Repo.get(Election, id)

    if election.finalized_at != nil do
      {:ok, election}
    else
      error_changeset =
        election
        |> Election.changeset()
        |> Ecto.Changeset.add_error(
          :finalized_at,
          "Couldn't load a finalized Election with id %{id}.",
          id: id
        )

      {:error, error_changeset}
    end
  end

  @doc """
  Load a election from an id, assure the data is finalized, create a new nem account for it, and update the election
  """
  def open_election(election_id) do
    Logger.debug("[Voting] Election opening: " <> election_id)

    multi =
      Multi.new()
      |> Multi.run(:get_election, fn _ -> get_finalized_election(election_id) end)
      |> Multi.insert(
        :nem_account,
        NemAccount.generate_key_pair()
        |> NemAccount.insert_changeset()
      )
      |> Multi.run(
        :election,
        fn %{get_election: election, nem_account: %NemAccount{address: nem_account_id}} ->
          Election.account_changeset(election, nem_account_id)
          |> Repo.update()
        end
      )

    case Repo.transaction(multi) do
      {:ok, %{election: election}} ->
        seed_election_account(election)

      {:error, _, changeset, _} ->
        Core.broadcast("voting:election_open_failed", changeset)
        {:error, changeset}
    end
  end

  @doc """
  Receive an election, and load its NEM account.
  Calculate the duration of the namespace.
  Insert a new seed into the database and spin up a seeder (observer).
  """
  def seed_election_account(election = %Election{}) do
    election =
      election
      |> Repo.preload(:nem_account)

    duration_sec = NaiveDateTime.diff(election.end_date, election.start_date, :second)

    result =
      %{
        mosaic_id: "election_chit",
        namespace_id: election.id,
        duration: Exnem.Duration.to_blocks(duration_sec, :second),
        nem_address: election.nem_account.address,
        election_id: election.id
      }
      |> Election.Seed.insert_changeset()
      |> Repo.insert()

    case result do
      {:ok, seed} ->
        if voting_workers_enabled?() do
          {:ok, _pid} = Election.Seeder.start(seed)
        end

      {:error, changeset} ->
        Logger.warn("Election id #{election.id} open failed.")
        Core.broadcast("voting:election_open_failed", changeset)
    end

    result
  end

  def create_election!(attrs \\ %{}) do
    {:ok, election} = create_election(attrs)
    election
  end

  def finalize_election(election = %Election{}) do
    result =
      election
      |> Election.finalizing_changeset()
      |> Repo.update()

    case result do
      {:ok, election} ->
        Logger.debug("[Voting] Scheduling Election #{election.id}")
        Election.Scheduler.schedule(election)

      {:error, changeset} ->
        Core.broadcast("voting:election_finalize_failed", changeset)
    end

    result
  end

  def tally_election(%Election{} = election) do
    Core.Voting.Election.Tally.run(election)
  end

  @doc """
  Creates an election particpation record.

  ## Examples

      iex> create_election_participation(%{voter_id: value, election_id: value, ballot_address: nem_account_address})
      {:ok, %Election.Participation{}}

      iex> create_election_participation(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_election_participation(attrs \\ %{}) do
    %Election.Participation{}
    |> Election.Participation.changeset(attrs)
    |> Repo.insert()
  end

  def election_open?(%Election{} = election) do
    is_open?(election)
  end

  defp election_request_to_participation(request) do
    request
    |> Map.from_struct()
    |> create_election_participation()
  end

  def request_election_ballot(election_id, voter_id, ballot_address) do
    attrs = %{
      election_id: election_id,
      voter_id: voter_id,
      ballot_address: ballot_address
    }

    with {:ok, request} <- Election.BallotRequest.new(attrs),
         :ok <- Election.BallotRequest.validate(request),
         participation_result = election_request_to_participation(request),
         {:ok, participation_seed} <- participation_result do
      if voting_workers_enabled?() do
        {:ok, _pid} = Election.BallotRequester.start(participation_seed)
      end

      participation_result
    end
  end

  def submit_election_participation(participation) do
    participation
    |> Election.Participation.submit_changeset()
    |> Repo.update()
    |> case do
      {:ok, participation} -> {:ok, participation}
      {:error, _changeset} -> {:error, "Could not update ballot database record"}
    end
  end

  def submit_election_ballot(election_id, voter_id, ballot_address, ballot_submission) do
    attrs = %{
      election_id: election_id,
      voter_id: voter_id,
      ballot_address: ballot_address,
      ballot: ballot_submission
    }

    with {:ok, submission} <- Election.BallotSubmit.new(attrs),
         :ok <- Election.BallotSubmit.validate(submission),
         participation = get_election_participation(election_id, voter_id),
         {:ok, participation_seed} <- submit_election_participation(participation) do
      if voting_workers_enabled?() do
        {:ok, _pid} = Election.BallotSubmitter.start(participation_seed, submission)
      end

      :ok
    end
  end

  def get_election_participation(election_id, voter_id) do
    Repo.get_by(
      Election.Participation,
      election_id: election_id,
      voter_id: voter_id
    )
  end

  def get_election_voted(election_id, voter_id) do
    get_election_participation(election_id, voter_id).voted
  end

  # ----------------------------------------------------------------------

  @doc """
  Returns the list of referendas.

  ## Examples

      iex> list_referendas()
      [%Referenda{}, ...]

  """
  def list_referendas do
    Repo.all(Referenda)
  end

  @doc """
  Gets a single referenda.

  Raises `Ecto.NoResultsError` if the Referenda does not exist.

  ## Examples

      iex> get_referenda!(123)
      %Referenda{}

      iex> get_referenda!(456)
      ** (Ecto.NoResultsError)

  """
  def get_referenda!(id), do: Repo.get!(Referenda, id)

  @doc """
  Creates a referenda.

  ## Examples

      iex> create_referenda(%{field: value})
      {:ok, %Referenda{}}

      iex> create_referenda(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_referenda(attrs \\ %{}) do
    %Referenda{}
    |> Referenda.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a referenda.

  ## Examples

      iex> update_referenda(referenda, %{field: new_value})
      {:ok, %Referenda{}}

      iex> update_referenda(referenda, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_referenda(%Referenda{} = referenda, attrs) do
    if referenda.finalized_at do
      {:error,
       referenda
       |> Referenda.changeset()
       |> Ecto.Changeset.add_error(
         :finalized_at,
         "This election has been finalized and may not be updated."
       )}
    else
      referenda
      |> Referenda.changeset(attrs)
      |> Repo.update()
    end
  end

  @doc """
  Deletes a Referenda.

  ## Examples

      iex> delete_referenda(referenda)
      {:ok, %Referenda{}}

      iex> delete_referenda(referenda)
      {:error, %Ecto.Changeset{}}

  """
  def delete_referenda(%Referenda{} = referenda) do
    Repo.delete(referenda)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking referenda changes.

  ## Examples

      iex> change_referenda(referenda)
      %Ecto.Changeset{source: %Referenda{}}

  """
  def change_referenda(%Referenda{} = referenda) do
    Referenda.changeset(referenda, %{})
  end

  @doc """
  Get the number of referenda.
  """
  def count_referenda() do
    Repo.aggregate(Referenda, :count, :id)
  end

  def list_referendas_to_schedule do
    date = Core.naive_now()

    from(
      e in Referenda,
      where: not is_nil(e.finalized_at) and is_nil(e.opened_at) and e.start_date > ^date
    )
    |> Repo.all()
  end

  defp get_finalized_referenda(id) do
    referenda = Repo.get(Referenda, id)

    if referenda.finalized_at != nil do
      {:ok, referenda}
    else
      error_changeset =
        referenda
        |> Referenda.changeset()
        |> Ecto.Changeset.add_error(
          :finalized_at,
          "Couldn't load a finalized Referenda with id %{id}.",
          id: id
        )

      {:error, error_changeset}
    end
  end

  def tally_referenda(%Referenda{} = referenda) do
    Core.Voting.Referenda.Tally.run(referenda)
  end

  @doc """
  Load a referenda from an id, assure the data is finalized, create a new nem account for it, and update the referenda
  """
  def open_referenda(referenda_id) do
    Logger.debug("[Voting] Referenda opening: " <> referenda_id)

    multi =
      Multi.new()
      |> Multi.run(:get_referenda, fn _ -> get_finalized_referenda(referenda_id) end)
      |> Multi.insert(
        :nem_account,
        NemAccount.generate_key_pair()
        |> NemAccount.insert_changeset()
      )
      |> Multi.run(
        :referenda,
        fn %{get_referenda: referenda, nem_account: %NemAccount{address: nem_account_id}} ->
          Referenda.account_changeset(referenda, nem_account_id)
          |> Repo.update()
        end
      )

    case Repo.transaction(multi) do
      {:ok, %{referenda: referenda}} ->
        seed_referenda_account(referenda)

      {:error, _, changeset, _} ->
        Core.broadcast("voting:referenda_open_failed", changeset)
        {:error, changeset}
    end
  end

  @doc """
  Receive an referenda, and load its NEM account.
  Calculate the duration of the namespace.
  Insert a new seed into the database and spin up a seeder (observer).
  """
  def seed_referenda_account(referenda = %Referenda{}) do
    referenda =
      referenda
      |> Repo.preload(:nem_account)

    duration_sec = NaiveDateTime.diff(referenda.end_date, referenda.start_date, :second)

    result =
      %{
        mosaic_id: "referenda_chit",
        namespace_id: referenda.id,
        duration: Exnem.Duration.to_blocks(duration_sec, :second),
        nem_address: referenda.nem_account.address,
        referenda_id: referenda.id
      }
      |> Referenda.Seed.insert_changeset()
      |> Repo.insert()

    case result do
      {:ok, seed} ->
        if voting_workers_enabled?() do
          {:ok, _pid} = Referenda.Seeder.start(seed)
        end

      {:error, changeset} ->
        Logger.warn("Referenda id #{referenda.id} open failed.")
        Core.broadcast("voting:referenda_open_failed", changeset)
    end

    result
  end

  def create_referenda!(attrs \\ %{}) do
    {:ok, referenda} = create_referenda(attrs)
    referenda
  end

  def finalize_referenda(%Referenda{} = referenda) do
    result =
      referenda
      |> Referenda.finalizing_changeset()
      |> Repo.update()

    case result do
      {:ok, referenda} ->
        Logger.debug("[Voting] Scheduling Referenda #{referenda.id}")
        Referenda.Scheduler.schedule(referenda)

      {:error, changeset} ->
        Core.broadcast("voting:referenda_finalize_failed", changeset)
    end

    result
  end

  @doc """
  Creates a referenda particpation record.

  ## Examples

      iex> create_referenda_participation(%{voter_id: value, referenda_id: value, ballot_address: nem_account_address})
      {:ok, %Election.Participation{}}

      iex> create_referenda_participation(bad_value1, bad_value2)
      {:error, %Ecto.Changeset{}}

  """
  def create_referenda_participation(attrs \\ {}) do
    %Referenda.Participation{}
    |> Referenda.Participation.changeset(attrs)
    |> Repo.insert()
  end

  def referenda_open?(%Referenda{} = referenda) do
    is_open?(referenda)
  end

  defp referenda_request_to_participation(request) do
    request
    |> Map.from_struct()
    |> create_referenda_participation()
  end

  def request_referenda_ballot(referenda_id, voter_id, ballot_address) do
    attrs = %{
      referenda_id: referenda_id,
      voter_id: voter_id,
      ballot_address: ballot_address
    }

    with {:ok, request} <- Referenda.BallotRequest.new(attrs),
         :ok <- Referenda.BallotRequest.validate(request),
         participation_result = referenda_request_to_participation(request),
         {:ok, participation_seed} <- participation_result do
      if voting_workers_enabled?() do
        {:ok, _pid} = Referenda.BallotRequester.start(participation_seed)
      end

      participation_result
    end
  end

  def submit_referenda_participation(changeset) do
    changeset
    |> Referenda.Participation.changeset(%{})
    |> Referenda.Participation.submit_changeset()
    |> Repo.update()
    |> case do
      {:ok, participation} -> {:ok, participation}
      {:error, _changeset} -> {:error, "Could not update ballot database record"}
    end
  end

  def submit_referenda_ballot(referenda_id, voter_id, ballot_address, ballot_submission) do
    attrs = %{
      referenda_id: referenda_id,
      voter_id: voter_id,
      ballot_address: ballot_address,
      ballot: ballot_submission
    }

    with {:ok, submission} <- Referenda.BallotSubmit.new(attrs),
         :ok <- Referenda.BallotSubmit.validate(submission),
         participation = get_referenda_participation(referenda_id, voter_id),
         {:ok, participation_seed} <- submit_referenda_participation(participation) do
      if voting_workers_enabled?() do
        {:ok, _pid} = Referenda.BallotSubmitter.start(participation_seed, submission)
      end

      :ok
    end
  end

  def get_referenda_participation(referenda_id, voter_id) do
    Repo.get_by(
      Referenda.Participation,
      referenda_id: referenda_id,
      voter_id: voter_id
    )
  end

  def get_referenda_voted(referenda_id, voter_id) do
    get_referenda_participation(referenda_id, voter_id).voted
  end

  alias Core.Voting.Confirmation

  @doc """
  Returns the list of confirmations.

  ## Examples

      iex> list_confirmations()
      [%Confirmation{}, ...]

  """
  def list_confirmations do
    Repo.all(Confirmation)
  end

  @doc """
  Gets a single confirmation.

  Raises `Ecto.NoResultsError` if the Confirmation does not exist.

  ## Examples

      iex> get_confirmation!(123)
      %Confirmation{}

      iex> get_confirmation!(456)
      ** (Ecto.NoResultsError)

  """
  def get_confirmation!(id), do: Repo.get!(Confirmation, id)

  @doc """
  Creates a confirmation.

  ## Examples

      iex> create_confirmation(%{field: value})
      {:ok, %Confirmation{}}

      iex> create_confirmation(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_confirmation(attrs \\ %{}) do
    %Confirmation{}
    |> Confirmation.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a confirmation.

  ## Examples

      iex> update_confirmation(confirmation, %{field: new_value})
      {:ok, %Confirmation{}}

      iex> update_confirmation(confirmation, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_confirmation(%Confirmation{} = confirmation, attrs) do
    confirmation
    |> Confirmation.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Confirmation.

  ## Examples

      iex> delete_confirmation(confirmation)
      {:ok, %Confirmation{}}

      iex> delete_confirmation(confirmation)
      {:error, %Ecto.Changeset{}}

  """
  def delete_confirmation(%Confirmation{} = confirmation) do
    Repo.delete(confirmation)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking confirmation changes.

  ## Examples

      iex> change_confirmation(confirmation)
      %Ecto.Changeset{source: %Confirmation{}}

  """
  def change_confirmation(%Confirmation{} = confirmation) do
    Confirmation.changeset(confirmation, %{})
  end

  @doc """
  Receives attributes with a pin, validates it, and checks it against the db.

  Returns {:ok, %Voting.Confirmation} from the database or {:error, %Ecto.Changeset{}}
  """
  def check_confirmation(attrs \\ %{}) do
    Confirmation.check_changeset(attrs)
    |> Ecto.Changeset.apply_action(:insert)
    |> case do
      {:ok, %{pin: pin}} ->
        case Repo.get(Confirmation, String.upcase(pin)) do
          nil ->
            Confirmation.error_changeset("Confirmation Pin not found.")
            |> Ecto.Changeset.apply_action(:insert)

          result ->
            {:ok, result}
        end

      error ->
        error
    end
  end

  def serialize_ballot(ballot) do
    ballot
    |> Poison.encode!()
    |> :zlib.gzip()
  end

  def unserialize_ballot(ballot) do
    ballot
    |> :zlib.gunzip()
    |> Poison.decode!()
  end

  def is_valid_vote?(nem_account, mosaic_id, {:ok, %Exnem.DTO.TransferTransaction{} = transfer}) do
    confirmed?(transfer) and recipient_equals?(transfer, nem_account) and
      transfers_one_mosaic?(transfer, mosaic_id)
  end

  def is_valid_vote?(_nem_account, _mosaic_id, _decode_result), do: false

  defp voting_workers_enabled?() do
    Application.fetch_env!(:core, :voting_workers_enabled)
  end

  def extract_transaction_payload({:ok, %{transaction: %{message: %{payload: payload}}}}) do
    payload
  end

  defp is_open?(%{end_date: end_date, opened_at: opened_at}) do
    now = Core.naive_now()

    cond do
      is_nil(opened_at) ->
        false

      NaiveDateTime.compare(end_date, now) == :gt ->
        true

      true ->
        false
    end
  end

  def voter_eligible?(%Referenda{}, _voter_id) do
    true
  end

  def voter_eligible?(%Election{}, _voter_id) do
    true
  end
end
