defmodule Core.Voting.Referenda do
  use Ecto.Schema
  import Ecto.Changeset

  alias Core.Voting.Measure
  alias Core.Voting.Validation

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "voting_referendas" do
    field(:start_date, :naive_datetime)
    field(:end_date, :naive_datetime)

    field(:finalized_at, :naive_datetime)
    field(:opened_at, :naive_datetime)

    embeds_many(:measures, Measure, on_replace: :delete)
    belongs_to(:nem_account, Core.NemAccount, references: :address, type: :string)

    timestamps()
  end

  @doc false
  def changeset(referenda, attrs \\ %{}) do
    referenda
    |> cast(attrs, [:start_date, :end_date, :finalized_at, :opened_at])
    |> cast_embed(:measures, required: true)
    |> validate_required([:start_date, :end_date])
    |> Validation.validate_proper_date_range(:start_date, :end_date)
  end

  def opening_changeset(referenda) do
    referenda
    |> cast(%{}, [])
    |> put_change(:opened_at, Core.naive_now())
  end

  def account_changeset(referenda, address) do
    referenda
    |> cast(%{nem_account_id: address}, [:nem_account_id])
    |> validate_required([:nem_account_id])
  end

  def finalizing_changeset(referenda) do
    referenda
    |> cast(%{}, [])
    |> put_change(:finalized_at, Core.naive_now())
  end
end
