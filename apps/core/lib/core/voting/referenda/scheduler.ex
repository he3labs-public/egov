defmodule Core.Voting.Referenda.Scheduler do
  use Task, restart: :transient

  alias Core.Activity
  alias Core.Voting.Referenda
  alias Core.Voting
  require Logger

  def start_link([]) do
    Task.start_link(__MODULE__, :run, [])
  end

  # When app starts up, this should be run
  def run() do
    info("Scheduler starting up ...")

    referendas = Voting.list_referendas_to_schedule()
    count = length(referendas)

    info("Found #{count} to schedule")
    Enum.map(referendas, &schedule/1)

    count
  end

  def schedule(referenda = %Referenda{}) do
    # actually, ~30 sec before start_date
    start = DateTime.from_naive!(referenda.start_date, "Etc/UTC")
    info("Scheduling Referenda #{referenda.id} at #{inspect(start)} (current time #{inspect DateTime.utc_now()})")

    {:ok, pid} = SchedEx.run_at(Voting, :open_referenda, [referenda.id], start)

    stats = SchedEx.stats(pid)
    Logger.debug("[Referenda.Scheduler] Stats for Referenda #{referenda.id} (#{inspect pid}) are #{inspect stats}")

    {:ok, pid}
  end

  defp info(message) do
    message = "[Referenda.Scheduler] #{message}"
    Logger.debug(message)
    Activity.log(message)
  end
end
