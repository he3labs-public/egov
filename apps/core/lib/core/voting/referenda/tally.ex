defmodule Core.Voting.Referenda.Tally do
  alias Core.Repo
  alias Core.Voting.Referenda

  def run(%Referenda{} = referenda) do
    referenda = referenda |> Repo.preload(:nem_account)

    tally = initialize_referenda_tally_slate(referenda.measures)
    empty_ballot = initialize_referenda_ballot(referenda.measures)

    get_possible_referenda_votes(referenda.id, referenda.nem_account)
    |> Enum.map(fn ballot ->
      ballot
      |> Core.Voting.unserialize_ballot()
      |> Map.fetch!("referenda")
      |> prepare_referenda_ballot(empty_ballot, tally)
    end)
    |> do_referenda_tally(tally)
  end

  def initialize_referenda_tally_slate(measures) do
    measures
    |> Enum.reduce(%{}, fn measure, acc ->
      Map.put(acc, measure.id, %{yes: 0, no: 0, abstain: 0})
    end)
  end

  def initialize_referenda_ballot(measures) do
    measures
    |> Enum.map(fn measure ->
      {measure.id, 0}
    end)
    |> Map.new()
  end

  defp get_possible_referenda_votes(namespace, nem_account) do
    # add paging support to
    path = "account/#{nem_account.public_key}/transactions/incoming?pageSize=200"
    mosaic_id = Exnem.mosaic_id(namespace <> ":referenda_chit")

    case Exnem.Node.get(path) do
      {:ok, transactions} ->
        transactions
        |> Enum.map(&Exnem.DTO.parse_transaction/1)
        |> Enum.filter(&Core.Voting.is_valid_vote?(nem_account, mosaic_id, &1))
        |> Enum.map(&Core.Voting.extract_transaction_payload/1)

      _ ->
        []
    end
  end

  defp prepare_referenda_ballot(ballot, empty_ballot, tally) do
    ballot
    |> Enum.reduce({:ok, empty_ballot}, fn measure, acc ->
      %{"id" => id, "vote" => vote} = measure

      with {:ok, curr_ballot} <- acc do
        case Map.get(tally, id) do
          nil ->
            {:error, "[Tally] Measure Id '#{id}' not found"}

          _record ->
            updated_ballot = Map.update!(curr_ballot, id, fn _ -> vote end)
            {:ok, updated_ballot}
        end
      end
    end)
  end

  defp do_referenda_tally(ballots, tally) do
    ballots
    |> Enum.reduce({tally, 0, 0}, fn entry, {acc, errors, total} ->
      case entry do
        {:ok, ballot} ->
          updated_acc =
            ballot
            |> Enum.reduce(acc, fn {id, vote}, acc ->
              case vote do
                1 -> put_in(acc, [id, :yes], acc[id][:yes] + 1)
                0 -> put_in(acc, [id, :abstain], acc[id][:abstain] + 1)
                -1 -> put_in(acc, [id, :no], acc[id][:no] + 1)
                _ -> acc
              end
            end)

          {updated_acc, errors, total + 1}

        {:error, _string} ->
          {acc, errors + 1, total}
      end
    end)
  end
end
