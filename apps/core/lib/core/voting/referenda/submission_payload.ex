defmodule Core.Voting.Referenda.SubmissionPayload do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:referenda_id, :binary_id, autogenerate: false}
  embedded_schema do
    embeds_many(:referenda, Core.Voting.Referenda.SubmissionPayload.Measure)
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:referenda_id])
    |> cast_embed(:referenda, required: true)
  end

  @doc false
  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end

defmodule Core.Voting.Referenda.SubmissionPayload.Measure do
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field(:vote, :integer)
  end

  @doc false
  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:vote])
    |> validate_required([:vote])
  end

  @doc false
  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end
end
