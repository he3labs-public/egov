defmodule Core.Voting.Referenda.Opener do
  use GenServer

  alias Core.Activity
  alias Core.Repo
  alias Core.Voting.Referenda

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(_) do
    Core.subscribe("voting:referenda_seeded")

    Activity.log("Referenda Opener ready")
    {:ok, %{}}
  end

  def handle_info({"voting:referenda_seeded", %Referenda.Seed{} = seed}, state) do
    Repo.get!(Referenda, seed.referenda_id)
    |> Referenda.opening_changeset()
    |> Repo.update()

    {:noreply, state}
  end
end
