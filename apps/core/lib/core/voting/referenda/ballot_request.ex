defmodule Core.Voting.Referenda.BallotRequest do
  use Ecto.Schema
  import Ecto.Changeset

  alias Core.Voting.Referenda
  alias Core.Repo

  embedded_schema do
    field(:referenda_id, :binary_id)
    field(:voter_id, :binary_id)
    field(:ballot_address, :string)
  end

  def new(attrs \\ %{}) do
    %__MODULE__{}
    |> changeset(attrs)
    |> apply_action(:insert)
  end

  def changeset(struct, attrs \\ %{}) do
    struct
    |> cast(attrs, [:referenda_id, :voter_id, :ballot_address])
    |> validate_required([:referenda_id, :voter_id, :ballot_address])
  end

  def validate(%__MODULE__{referenda_id: referenda_id, voter_id: voter_id} = request) do
    with {:ok, _request} <- check_valid(request),
         {:ok, referenda} <- check_if_exists(referenda_id),
         :ok <- check_if_open(referenda),
         :ok <- check_voter_eligible(referenda, voter_id),
         :ok <- check_participation(referenda, voter_id) do
      :ok
    end
  end

  defp check_valid(request) do
    changeset(request)
    |> apply_action(:insert)
  end

  defp check_if_exists(referenda_id) do
    case Repo.get(Referenda, referenda_id) do
      nil ->
        {:error, "Referenda not found"}

      referenda ->
        {:ok, referenda}
    end
  end

  defp check_if_open(referenda) do
    case Core.Voting.referenda_open?(referenda) do
      false ->
        {:error, "Referenda not running"}

      true ->
        :ok
    end
  end

  defp check_voter_eligible(referenda, voter_id) do
    if Core.Voting.voter_eligible?(referenda, voter_id) do
      :ok
    else
      {:error, "Voter not eligible"}
    end
  end

  defp check_participation(referenda, voter_id) do
    case Core.Voting.get_referenda_participation(referenda.id, voter_id) do
      _no_participation = nil ->
        :ok

      _participation ->
        {:error, "Ballot has already been sent"}
    end
  end
end
