defmodule Core.Voting.Referenda.Participation do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  alias Core.Account.Identity
  alias Core.Voting.Referenda
  alias Core.NemAccount

  schema "voting_referenda_participations" do
    belongs_to(:voter, Identity, type: :binary_id)
    belongs_to(:referenda, Referenda)

    belongs_to(
      :nem_account,
      NemAccount,
      foreign_key: :ballot_address,
      references: :address,
      type: :string
    )

    belongs_to(:request_transaction, Core.NemTransaction, references: :tx_hash, type: :string)
    belongs_to(:submit_transaction, Core.NemTransaction, references: :tx_hash, type: :string)

    field(:voted, :boolean, default: false)
  end

  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [
      :voter_id,
      :referenda_id,
      :ballot_address,
      :request_transaction_id,
      :submit_transaction_id
    ])
    |> validate_required([
      :voter_id,
      :referenda_id,
      :ballot_address
    ])
    |> assoc_constraint(:request_transaction)
    |> assoc_constraint(:submit_transaction)
    |> assoc_constraint(:voter)
    |> assoc_constraint(:referenda)
    |> assoc_constraint(:nem_account)
    |> unique_constraint(
      :voter_id,
      name: :voting_referenda_participations_voter_id_referenda_id_index
    )
  end

  def submit_changeset(changeset) do
    changeset
    |> cast(%{}, [])
    |> put_change(:voted, true)
  end
end
