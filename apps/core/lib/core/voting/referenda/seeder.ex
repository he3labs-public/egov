defmodule Core.Voting.Referenda.Seeder do
  use GenServer
  require Logger

  alias Exnem.Transaction
  alias Exnem.Crypto.KeyPair
  alias Core.Voting.Referenda
  alias Core.{Activity, Repo, NemAccount, NemTransaction}

  defmodule State do
    defstruct [:seed, :observer]
  end

  def start(%Referenda.Seed{} = seed) do
    GenServer.start(__MODULE__, seed)
  end

  def init(%Referenda.Seed{} = seed) do
    {:ok, pid} = start_observer(seed.nem_address)

    {:ok, %State{seed: seed, observer: pid}}
  end

  def handle_info("observerReady", state) do
    updated_seed = announce_namespace_and_mosaic_creation(state.seed)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"error", error}, state) do
    error_meaning = Exnem.StatusErrors.meaning(error.status)
    info("[Referenda.Seeder] Seeding failed #{error.status}: #{error_meaning}")

    updated_seed = handle_error(state.seed, error)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def handle_info({"unconfirmedAdded", dto}, state) do
    updated_seed = handle_unconfirmed(state.seed, dto)
    {:noreply, %{state | seed: updated_seed}}
  end

  def handle_info({"confirmedAdded", dto}, state) do
    info("[Referenda.Seeder] Seeding complete.")
    updated_seed = handle_confirmed(state.seed, dto)
    {:stop, :normal, %{state | seed: updated_seed}}
  end

  def terminate(_reason, state) do
    stop_observer(state)
    :ok
  end

  defp handle_error(seed, error) do
    updated_tx =
      seed.seed_transaction
      |> NemTransaction.failed_changeset(error.status)
      |> Repo.update!()

    Core.broadcast("voting:referenda_seeding_failed", %{seed | seed_transaction: updated_tx})
  end

  defp handle_unconfirmed(seed, _dto) do
    seed
  end

  defp handle_confirmed(seed, _dto) do
    seed =
      seed
      |> Repo.preload(:seed_transaction)

    seed.seed_transaction
    |> NemTransaction.confirmed_changeset()
    |> Repo.update()
    |> case do
      {:ok, updated_tx} ->
        info("Referenda #{seed.namespace_id} open succeeded.")
        Core.broadcast("voting:referenda_seeded", %{seed | seed_transaction: updated_tx})

      _ ->
        Core.broadcast("voting:referenda_seeding_failed", seed)
    end
  end

  defp announce_namespace_and_mosaic_creation(
         %{
           namespace_id: namespace,
           mosaic_id: mosaic,
           duration: duration,
           nem_address: nem_address
         } = seed
       ) do
    nem_account = Repo.get!(NemAccount, nem_address)

    {:ok, namespace_transaction} = Transaction.register_root_namespace(namespace, duration)

    {:ok, mosaic_transaction} =
      Transaction.mosaic_definition(namespace, mosaic, duration: duration)

    {:ok, supply_transaction} =
      Transaction.supply_change(:increase, namespace, mosaic, Referenda.Seed.max_ballots())

    {:ok, aggregate} =
      [
        supply_transaction,
        mosaic_transaction,
        namespace_transaction
      ]
      |> Enum.map(&Transaction.convert_to_inner(&1, nem_account.public_key))
      |> Transaction.aggregate()

    verf_tx =
      aggregate
      |> Transaction.pack()
      |> KeyPair.sign(nem_account)

    Exnem.Announce.announce(verf_tx)

    nem_tx =
      NemTransaction.announced_changeset(verf_tx.hash)
      |> Repo.insert!()

    seed
    |> Referenda.Seed.changeset(%{seed_transaction_id: nem_tx.tx_hash})
    |> Repo.update()
    |> case do
      {:ok, updated_seed} ->
        Core.broadcast("voting:referenda_seed_announced", updated_seed)

        updated_seed

      _ ->
        Core.broadcast("voting:referenda_seed_failed", seed)
        seed
    end
  end

  defp start_observer(nem_address) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.start_link(nem_address, self())
  end

  defp stop_observer(%State{observer: pid}) do
    module = Application.fetch_env!(:core, :exnem_observer)
    module.stop(pid)
  end

  defp info(message) do
    Logger.info(message)
    Activity.log(message)
  end
end
