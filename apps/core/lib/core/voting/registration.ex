defmodule Core.Voting.Registration do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "voting_registrations" do
    field(:first_name, :string)
    field(:id_number, :string)
    field(:last_name, :string)

    timestamps()
  end

  @doc false
  def changeset(registration, attrs) do
    registration
    |> cast(attrs, [:first_name, :last_name, :id_number])
    |> validate_required([:first_name, :last_name, :id_number])
  end

  @doc false
  def insert_changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:first_name, :last_name, :id_number])
    |> validate_required([:first_name, :last_name, :id_number])
  end
end
