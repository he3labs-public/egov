defmodule Core.Voting.Confirmation do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Phoenix.Param, key: :pin}
  @primary_key {:pin, :string, autogenerate: false}
  @foreign_key_type :string

  schema "confirmations" do
    field(:election_id, :binary)
    field(:referenda_id, :binary)
    field(:tx_hash, :string)
    field(:voter_id, :binary)

    timestamps()
  end

  @doc false
  def changeset(confirmation, attrs) do
    confirmation
    |> cast(attrs, [:pin, :election_id, :referenda_id, :voter_id, :tx_hash])
    |> validate_required([:pin, :voter_id, :tx_hash])
  end

  def insert_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
  end

  def check_changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:pin])
    |> validate_required([:pin])
    |> update_change(:pin, &String.trim/1)
    |> validate_length(:pin, is: 4)
  end

  def error_changeset(string) do
    %__MODULE__{}
    |> cast(%{}, [])
    |> add_error(:pin, string)
  end
end
