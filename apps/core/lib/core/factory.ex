defmodule Core.Factory do
  use ExMachina.Ecto, repo: Core.Repo

  alias Core.Account.{ApiToken, Identity, MailingAddress}
  alias Core.Demo.{BankAccount, Pin}
  alias Core.{NemAccount, NemTransaction, Banking, Voting, Shopping}
  alias Faker.{Name, Date, Address}
  alias Ecto.UUID

  defp personalized_email(first_name, last_name) do
    user_name =
      [first_name, last_name]
      |> Enum.map_join(Faker.Util.pick(~w(. _)), &String.replace(&1, ~s(  ), ~s()))
      |> String.downcase()

    "#{user_name}@example.#{Faker.Util.pick(~w(org com net))}"
  end

  def identity_factory do
    first_name = Name.first_name()
    last_name = Name.last_name()

    %Identity{
      first_name: first_name,
      last_name: last_name,
      birthdate: Date.date_of_birth(),
      email: personalized_email(first_name, last_name),
      mailing_address: Identity.default_mailing_address(),
      pin: Pin.generate(),
      nem_account: build(:nem_account)
    }
  end

  def mailing_address_factory do
    %MailingAddress{
      address1: Address.street_address(),
      address2: Address.secondary_address(),
      city: Address.city(),
      state: Address.state(),
      postcode: Address.postcode()
    }
  end

  def nem_account_factory do
    key_info = Exnem.Crypto.KeyPair.generate(:mijin_test)
    struct(NemAccount, key_info)
  end

  def nem_transaction_factory do
    %NemTransaction{
      tx_hash: generate_transaction_hash()
    }
  end

  def bank_account_factory do
    %Banking.Account{
      first_name: Name.first_name(),
      last_name: Name.last_name(),
      id_number: UUID.generate(),
      nem_account: build(:nem_account)
    }
  end

  def bank_account_seed_factory() do
    %BankAccount.Seed{
      amount: Core.Demo.generate_seed_amount(),
      seed_transaction: build(:nem_transaction)
    }
  end

  def bank_transaction_factory do
    amount = Faker.Util.pick(100..2100)

    %Banking.Transaction{
      to: build(:bank_account),
      from: build(:bank_account),
      description: "Transfer",
      amount: amount
    }
  end

  def voting_registration_factory do
    %Voting.Registration{
      first_name: Name.first_name(),
      last_name: Name.last_name(),
      id_number: UUID.generate()
    }
  end

  def voting_election_factory do
    candidates =
      Enum.flat_map(Voting.Candidate.positions(), fn pos ->
        how_many = Faker.Util.pick(2..3)
        Enum.map(1..how_many, fn _ -> build(:candidate, %{position: pos}) end)
      end)

    %Voting.Election{
      start_date: Core.naive_now(1, :sec),
      end_date: Core.naive_now(240),
      candidates: candidates,
      nem_account: build(:nem_account)
    }
  end

  def election_ballot_submission(%Voting.Election{} = election) do
    choices =
      Voting.Candidate.positions()
      |> Enum.map(fn position ->
        election.candidates
        |> Enum.filter(fn candidate -> candidate.position == position end)
        |> Faker.Util.pick()
        |> Map.from_struct()

        # missing abstains
      end)

    %Voting.Election.SubmissionPayload{
      election_id: election.id,
      candidates: choices
    }
  end

  def voting_referenda_factory do
    measures = build_list(4, :voting_measure)

    %Voting.Referenda{
      start_date: Core.naive_now(1, :sec),
      end_date: Core.naive_now(240),
      measures: measures,
      nem_account: build(:nem_account)
    }
  end

  def referenda_ballot_submission(%Voting.Referenda{} = referenda) do
    choices =
      referenda.measures
      |> Enum.map(fn measure ->
        %{
          id: measure.id,
          vote: Faker.Util.pick([-1, -1, 0, 1, 1])
        }
      end)

    %Voting.Referenda.SubmissionPayload{
      referenda_id: referenda.id,
      referenda: choices
    }
  end

  def voting_measure_factory do
    prop_number = Faker.Util.pick(1..199)
    overview = Faker.Lorem.paragraphs(1..2) |> Enum.join("\n\n")

    %Voting.Measure{
      name: "Proposition #{prop_number}",
      title: voting_measure_title(),
      overview: overview
    }
  end

  defp voting_measure_title do
    buzzword = String.capitalize(Faker.Company.buzzword())
    industry_sector = Faker.Industry.sub_sector()

    buzzword <> " " <> industry_sector
  end

  def voting_confirmation_factory do
    voter = insert(:identity)
    election = insert(:voting_election)
    nem_transaction = insert(:nem_transaction)

    %Voting.Confirmation{
      pin: Core.Demo.Pin.unique_pin("confirmations"),
      voter_id: voter.id,
      election_id: election.id,
      tx_hash: nem_transaction.tx_hash
    }
  end

  def candidate_factory do
    full_name = Name.first_name() <> " " <> Name.last_name()

    %Voting.Candidate{
      name: full_name,
      position: Faker.Util.pick(Voting.Candidate.positions())
    }
  end

  defp generate_transaction_hash() do
    :crypto.strong_rand_bytes(32)
    |> Base.encode16()
  end

  def api_token_factory do
    %ApiToken{
      identity: build(:identity),
      token: ApiToken.generate_token()
    }
  end

  def product_factory do
    %Shopping.Product{
      id: UUID.generate(),
      name: Faker.Beer.name(),
      price: Faker.Util.pick(200..500)
    }
  end
end
