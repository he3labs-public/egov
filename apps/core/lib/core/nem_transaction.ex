defmodule Core.NemTransaction do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:tx_hash, :string, autogenerate: false}
  @foreign_key_type :string

  schema "nem_transactions" do
    field(:announced_at, :naive_datetime)
    field(:confirmed_at, :naive_datetime)

    field(:error, :string)
    field(:error_at, :naive_datetime)
  end

  def changeset(changeset, attrs) do
    changeset
    |> cast(attrs, [:tx_hash])
    |> validate_required([:tx_hash])
  end

  def confirmed?(%__MODULE__{} = transaction) do
    transaction.confirmed_at != nil
  end

  def announced_changeset(tx_hash) do
    attrs = %{
      tx_hash: tx_hash,
      announced_at: Core.naive_now()
    }

    %__MODULE__{}
    |> cast(attrs, [:tx_hash, :announced_at])
    |> validate_required([:tx_hash, :announced_at])
  end

  def confirmed_changeset(changeset) do
    attrs = %{confirmed_at: Core.naive_now()}

    changeset
    |> cast(attrs, [:confirmed_at])
    |> validate_required([:confirmed_at])
  end

  def failed_changeset(changeset, message) do
    attrs = %{error: message, error_at: Core.naive_now()}

    changeset
    |> cast(attrs, [:error, :error_at])
    |> validate_required([:error, :error_at])
  end
end
