defmodule Demo.Generate do
  require Logger
  alias Core.{Factory, Voting, Account, Banking, Activity, Demo}

  def election() do
    Factory.params_for(:voting_election)
    |> Voting.create_election!()
    |> Voting.finalize_election()
  end

  def referenda() do
    Factory.params_for(:voting_referenda)
    |> Voting.create_referenda!()
    |> Voting.finalize_referenda()
  end

  def identity() do
    Factory.params_for(:identity)
    |> Account.create_identity()
  end

  def banking() do
    identity()
    |> banking()
  end

  def banking({:ok, identity}) do
    account =
      Factory.insert(
        :bank_account,
        id_number: identity.id,
        first_name: identity.first_name,
        last_name: identity.last_name
      )

    some = Faker.Util.pick(1..5)

    initial =
      Factory.insert(
        :bank_transaction,
        to: account,
        inserted_at: Core.naive_now(-100),
        status: "completed",
        description: "Demo Seeding",
        amount: 100_000
      )

    transactions =
      Enum.map(1..some, fn index ->
        if Faker.Util.pick(0..1) == 1 do
          Factory.insert(
            :bank_transaction,
            to: account,
            inserted_at: Core.naive_now(-index),
            status: "completed"
          )
        else
          Factory.insert(
            :bank_transaction,
            from: account,
            inserted_at: Core.naive_now(-index),
            status: "completed"
          )
        end
      end)

    balance =
      [initial | transactions]
      |> Banking.calculate_balance(account.id_number)
      |> Core.Banking.Amount.integer_to_string()

    %{account: account, balance: balance, transactions: [initial | transactions]}
  end

  def product() do
    Factory.insert(:product)
  end

  alias Exnem.Transaction
  alias Exnem.Crypto.KeyPair

  def main_token() do
    namespace = Demo.namespace()
    mosaic_name = Demo.mosaic_name()
    supply = Demo.mosaic_supply()

    []
    |> register_namespace_transaction(namespace)
    |> mosaic_definition(namespace, mosaic_name)
    |> supply_transaction(namespace, mosaic_name, supply)
    |> execute()
  end

  defp register_namespace_transaction(transactions, namespace) do
    if Exnem.Namespace.name_exists?(namespace) do
      transactions
    else
      info("Demo namespace #{namespace} does not exist, registering...")

      {:ok, namespace_transaction} =
        Transaction.register_root_namespace(namespace, blocks_in_a_year())

      transactions ++ [{:namespace, namespace_transaction}]
    end
  end

  defp mosaic_definition(transactions, namespace, mosaic_name) do
    if Exnem.Mosaic.exists?(namespace, mosaic_name) do
      transactions
    else
      info("Demo mosaic #{namespace}:#{mosaic_name} does not exist, registering...")

      {:ok, mosaic_transaction} =
        Transaction.mosaic_definition(
          namespace,
          mosaic_name,
          duration: blocks_in_a_year(),
          transferable: true
        )

      transactions ++ [{:mosaic, mosaic_transaction}]
    end
  end

  defp supply_transaction(transactions, namespace, mosaic_name, total_supply) do
    if Exnem.Mosaic.exists?(namespace, mosaic_name) do
      case Exnem.Mosaic.get_supply(namespace, mosaic_name) do
        {:ok, supply} ->
          if supply < total_supply * 0.2 do
            increase_supply(transactions, namespace, mosaic_name, total_supply - supply)
          else
            transactions
          end

        {:error, reason} ->
          error("Error reading supply of Demo mosaic: #{inspect(reason)}")
      end
    else
      initial_supply(transactions, namespace, mosaic_name, total_supply)
    end
  end

  defp increase_supply(transactions, namespace, mosaic_name, increase) do
    info("Demo mosaic #{namespace}:#{mosaic_name} supply being increased by #{increase}...")

    {:ok, supply_transaction} =
      Transaction.supply_change(:increase, namespace, mosaic_name, increase)

    transactions ++ [{:supply, supply_transaction}]
  end

  defp initial_supply(transactions, namespace, mosaic_name, initial_supply) do
    info(
      "Demo mosaic #{namespace}:#{mosaic_name} receiving an initial supply of #{initial_supply}..."
    )

    {:ok, supply_transaction} =
      Transaction.supply_change(:increase, namespace, mosaic_name, initial_supply)

    transactions ++ [{:supply, supply_transaction}]
  end

  defp execute(transactions) when is_list(transactions) do
    case length(transactions) do
      0 ->
        info("Demo namespace, mosaic, and supply are good to go.")
        :ok

      _ ->
        info("Demo #{names(transactions)} are being set up...")
        announce(transactions)
    end
  end

  defp announce(transactions) do
    demo_lord = Application.fetch_env!(:core, :demo_lord_key_pair)

    {:ok, aggregate} =
      transactions
      |> Enum.reverse()
      |> Keyword.values()
      |> Enum.map(&Transaction.convert_to_inner(&1, demo_lord.public_key))
      |> Transaction.aggregate()

    aggregate
    |> Transaction.pack()
    |> KeyPair.sign(demo_lord)
    |> Exnem.Announce.sync_announce()
    |> case do
      {:ok, _confirmation} ->
        info("Demo #{names(transactions)} set up complete.")
        :ok

      {:error, reason} ->
        error("Error completing Demo set up: #{inspect(reason)}")
        {:error, reason}
    end
  end

  defp blocks_in_a_year do
    Exnem.Duration.to_blocks(365, :day)
  end

  defp names(transactions) do
    transactions |> Keyword.keys() |> Enum.map(&to_string/1) |> Core.to_sentence()
  end

  defp info(message) do
    output = "[Demo.Generate] " <> message

    IO.puts(output)
    Activity.log(output)
  end

  defp error(message) do
    output = "[Demo.Generate] " <> message
    Logger.error(output)
    Activity.log(output)
  end
end
