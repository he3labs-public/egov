defmodule Core do
  alias Phoenix.PubSub

  @moduledoc """
  Core keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def broadcast(topic, payload) do
    PubSub.broadcast(Core.PubSub, topic, {topic, payload})
  end

  def broadcast_from(topic, payload) do
    PubSub.broadcast_from(Core.PubSub, self(), topic, {topic, payload})
  end

  def subscribe(topic) do
    PubSub.subscribe(Core.PubSub, topic)
  end

  def naive_now() do
    NaiveDateTime.utc_now()
  end

  def naive_now(additional_minutes) do
    NaiveDateTime.utc_now()
    |> NaiveDateTime.add(additional_minutes * 60, :second)
  end

  def naive_now(additional_seconds, :sec) do
    NaiveDateTime.utc_now()
    |> NaiveDateTime.add(additional_seconds, :second)
  end

  def naive_now(additional_hours, :hour) do
    NaiveDateTime.utc_now()
    |> NaiveDateTime.add(additional_hours * 3600, :second)
  end

  def uuid_to_number(uuid, max) do
    case Ecto.UUID.dump(uuid) do
      :error ->
        nil

      {:ok, value} ->
        value
        |> :binary.decode_unsigned()
        |> rem(max)
    end
  end

  def uuids_to_number(uuid1, uuid2, max) do
    with {:ok, value1} <- Ecto.UUID.dump(uuid1),
         {:ok, value2} <- Ecto.UUID.dump(uuid2) do
      (:binary.decode_unsigned(value1) + :binary.decode_unsigned(value2)) |> rem(max)
    end
  end

  @spec to_sentence([binary]) :: binary
  def to_sentence([]), do: ""
  def to_sentence([single]), do: single
  def to_sentence([first, second]), do: "#{first} and #{second}"
  def to_sentence([first | rest]), do: Enum.join(rest, ", ") <> ", and #{first}"
end
