defmodule Mix.Tasks.CreateReferenda do
  use Mix.Task

  alias Core.{Factory, Voting}

  @shortdoc "Simply runs the /0 function"
  def run(_) do
    {:ok, _} = Application.ensure_all_started(:core)

    Factory.params_for(:voting_referenda)
    |> Voting.create_referenda()
  end
end
