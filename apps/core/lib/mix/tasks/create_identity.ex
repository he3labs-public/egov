defmodule Mix.Tasks.CreateIdentity do
  use Mix.Task

  alias Core.{Factory, Account}

  @shortdoc "Simply runs the /0 function"
  def run(_) do
    {:ok, _} = Application.ensure_all_started(:core)

    Factory.params_for(:identity)
    |> Account.create_identity()
  end
end
