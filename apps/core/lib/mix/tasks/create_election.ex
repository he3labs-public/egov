defmodule Mix.Tasks.CreateElection do
  use Mix.Task

  alias Core.{Voting, Factory}

  @shortdoc "Creates a random election"
  def run(_) do
    {:ok, _} = Application.ensure_all_started(:core)

    Factory.params_for(:voting_election)
    |> Voting.create_election()
  end
end
