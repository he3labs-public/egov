use Mix.Config

# Configure your database
config :logger, level: :info

config :core, Core.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "onc_demo_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  ownership_timeout: 120_000

config :core,
  exnem_observer: Exnem.TestObserver,
  demo_banking_provisioner: Core.Demo.BankAccount.TestProvisioner,
  banking_worker_enabled: false,
  voting_workers_enabled: false

config :exnem, node_url: "localhost:3000"
