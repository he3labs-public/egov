use Mix.Config

config :core, ecto_repos: [Core.Repo]

import_config "#{Mix.env()}.exs"

config :core,
  demo_namespace: "oncdemo",
  demo_mosaic_name: "token",
  demo_mosaic_supply: 8_900_000_000_000,
  demo_lord_mosaic_ids: %{
    usd: 8_050_144_702_280_590_652
  },
  demo_lord_key_pair: %{
    public_key: "",
    private_key: "",
    address: ""
  }
