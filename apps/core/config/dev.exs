use Mix.Config

# Configure your database
config :core, Core.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "onc_demo_dev",
  hostname: "localhost",
  pool_size: 10

config :core,
  exnem_observer: Exnem.Observer,
  demo_banking_provisioner: Core.Demo.BankAccount.Provisioner,
  banking_worker_enabled: true,
  voting_workers_enabled: true
