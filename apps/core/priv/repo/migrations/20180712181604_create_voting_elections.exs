defmodule Core.Repo.Migrations.CreateVotingElections do
  use Ecto.Migration

  def change do
    create table(:voting_elections, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :start_date, :naive_datetime
      add :end_date, :naive_datetime
      add :chairmen, {:array, :map}
      add :vice_chairmen, {:array, :map}
      add :secretaries, {:array, :map}
      add :vice_secretaries, {:array, :map}

      timestamps()
    end

  end
end
