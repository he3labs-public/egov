defmodule Core.Repo.Migrations.CreateBankAccountSeeds do
  use Ecto.Migration

  def change do
    create table(:bank_account_seeds, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :amount, :integer, null: false
      add :banking_account_id, :binary_id, null: false
      add :nem_address, :string, null: false
      add :tx_hash, :string
      add :announced_at, :naive_datetime
      add :confirmed_at, :naive_datetime
      add :error, :string
      add :error_at, :naive_datetime

      timestamps()
    end

    create index(:bank_account_seeds, [:banking_account_id])
    create index(:bank_account_seeds, [:nem_address])
  end
end
