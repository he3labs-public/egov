defmodule Core.Repo.Migrations.ChangeBankAccountsIdNumberField do
  use Ecto.Migration
  import Ecto.Query, only: [from: 2]
  alias Core.Repo

  def change do
    id_number_map = build_id_number_map()

    alter table(:bank_accounts) do
      add(:binary_id_number, :binary_id)
    end

    flush()

    fill_binary_id_number(id_number_map)

    alter table(:bank_accounts) do
      remove :id_number
    end

    rename table(:bank_accounts), :binary_id_number, to: :id_number

    create(unique_index(:bank_accounts, [:id_number]))
  end

  defp build_id_number_map do
    from("bank_accounts", select: [:id, :id_number])
    |> Repo.all()
    |> Enum.reduce(%{}, fn %{id: id, id_number: id_number}, acc ->
      Map.put(acc, id, id_number)
    end)
  end

  defp fill_binary_id_number(map) do
    Enum.each(map, fn {id, id_number} ->
      {:ok, binary_id_number} = Ecto.UUID.dump(id_number)

      from(a in "bank_accounts", where: a.id == ^id)
      |> Repo.update_all(set: [binary_id_number: binary_id_number])
    end)
  end
end
