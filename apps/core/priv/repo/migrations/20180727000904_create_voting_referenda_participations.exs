defmodule Core.Repo.Migrations.CreateVotingReferendaParticipations do
  use Ecto.Migration

  def change do
    create table(:voting_referenda_participations, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :voter_id, references(:identities, on_delete: :delete_all, type: :binary_id)
      add :referenda_id, references(:voting_referendas, on_delete: :delete_all, type: :binary_id)
      add :ballot_address, references(:nem_accounts, column: :address, type: :string, on_delete: :nilify_all)
    end

    create unique_index(:voting_referenda_participations, [:voter_id, :referenda_id])
  end
end
