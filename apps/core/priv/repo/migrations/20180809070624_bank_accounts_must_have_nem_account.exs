defmodule Core.Repo.Migrations.BankAccountsMustHaveNemAccount do
  use Ecto.Migration
  import Ecto.Query, only: [from: 2]

  alias Core.{Repo, NemAccount}

  def change do
    drop(constraint("bank_accounts", "bank_accounts_nem_account_id_fkey"))
    flush()

    fetch_bank_accounts()
    |> Enum.map(&add_nem_account_if_blank/1)

    alter table(:bank_accounts) do
      modify(
        :nem_account_id,
        references(
          :nem_accounts,
          column: :address,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        ),
        null: false
      )
    end
  end

  defp fetch_bank_accounts do
    Repo.all(from(a in "bank_accounts", select: [:id, :nem_account_id]))
  end

  defp add_nem_account_if_blank(bank_account) do
    if bank_account.nem_account_id == nil do
      {:ok, nem_account} = create_nem_account()

      {1, _} =
        from(a in "bank_accounts", where: a.id == ^bank_account.id)
        |> Repo.update_all(set: [nem_account_id: nem_account.address])
    end
  end

  defp create_nem_account() do
    NemAccount.generate_key_pair()
    |> NemAccount.insert_changeset()
    |> Repo.insert()
  end
end
