defmodule Core.Repo.Migrations.ChangeAuthorizeToFinalize do
  use Ecto.Migration

  def change do
    alter table("voting_elections") do
      remove(:authorized_at)
      add(:finalized_at, :naive_datetime)
    end

    alter table("voting_referendas") do
      remove(:authorized_at)
      add(:finalized_at, :naive_datetime)
    end
  end
end
