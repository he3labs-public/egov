defmodule Core.Repo.Migrations.AddAccountTypeToBankAccounts do
  use Ecto.Migration

  def change do
    alter table("bank_accounts") do
      add :account_type, :string, null: false, default: "citizen"
    end
  end
end
