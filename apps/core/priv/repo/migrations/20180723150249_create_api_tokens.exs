defmodule Core.Repo.Migrations.CreateApiTokens do
  use Ecto.Migration

  def change do
    create table(:api_tokens) do
      add :token, :string
      add :identity_id, references(:identities, type: :binary_id, on_delete: :delete_all)

      timestamps()
    end

    create index(:api_tokens, [:identity_id])
  end
end
