defmodule Core.Repo.Migrations.AddPinCodeToIdentities do
  use Ecto.Migration
  import Ecto.Query

  alias Core.{Demo, Repo}

  def up do
    alter table("identities") do
      add(:pin, :string)
    end

    flush()
    generate_pins()

    alter table("identities") do
      modify(:pin, :string, null: false)
    end

    create(unique_index("identities", [:pin]))
  end

  def down do
    alter table("identities") do
      remove(:pin)
    end
  end

  defp generate_pins() do
    query = from(i in "identities", where: is_nil(i.pin), select: i.id)
    identities = Repo.all(query)

    Enum.each(identities, fn id ->
      new_pin = Demo.Pin.unique_pin("identities")

      from(i in "identities", where: i.id == ^id)
      |> Repo.update_all(set: [pin: new_pin])
    end)
  end
end
