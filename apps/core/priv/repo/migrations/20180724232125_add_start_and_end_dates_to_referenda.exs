defmodule Core.Repo.Migrations.AddStartAndEndDatesToReferenda do
  use Ecto.Migration

  def change do
    alter table("referendas") do
      add(
        :nem_account_id,
        references(
          :nem_accounts,
          column: :address,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )

      add(:start_date, :naive_datetime)
      add(:end_date, :naive_datetime)
      add(:authorized_at, :naive_datetime)
      add(:opened_at, :naive_datetime)
    end
  end
end
