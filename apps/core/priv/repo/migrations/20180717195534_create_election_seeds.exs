defmodule Core.Repo.Migrations.CreateElectionSeeds do
  use Ecto.Migration

  def change do
    alter table(:voting_elections) do
      add(
        :nem_account_id,
        references(
          :nem_accounts,
          column: :address,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end

    create(index(:voting_elections, [:nem_account_id]))

    create table(:election_seeds, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:nem_address, :string)

      timestamps()
    end
  end
end
