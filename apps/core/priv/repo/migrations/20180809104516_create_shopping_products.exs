defmodule Core.Repo.Migrations.CreateShoppingProducts do
  use Ecto.Migration

  def change do
    create table(:shopping_products, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :price, :integer

      timestamps()
    end

  end
end
