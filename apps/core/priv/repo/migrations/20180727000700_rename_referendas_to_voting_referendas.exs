defmodule Core.Repo.Migrations.RenameReferendasToVotingReferendas do
  use Ecto.Migration

  def change do
    rename table(:referendas), to: table(:voting_referendas)
  end
end
