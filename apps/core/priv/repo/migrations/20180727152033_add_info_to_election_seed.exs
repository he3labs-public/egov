defmodule Core.Repo.Migrations.AddInfoToElectionSeed do
  use Ecto.Migration

  def change do
    alter table("election_seeds") do
      add(:mosaic_id, :string)
      add(:mosaic_amount, :integer)
      add(:duration, :integer)
      add(:namespace_id, :string)
      add(:election_id, :binary_id)
    end
  end
end
