defmodule Core.Repo.Migrations.CreateBankAccounts do
  use Ecto.Migration

  def change do
    create table(:bank_accounts, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:first_name, :string, null: false)
      add(:last_name, :string, null: false)
      add(:id_number, :string, null: false)

      add(
        :nem_account_id,
        references(
          :nem_accounts,
          column: :address,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )

      timestamps()
    end

    create(index(:bank_accounts, [:nem_account_id]))
  end
end
