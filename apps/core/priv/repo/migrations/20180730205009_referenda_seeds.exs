defmodule Core.Repo.Migrations.ReferendaSeeds do
  use Ecto.Migration

  def change do
    create table(:referenda_seeds, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:mosaic_id, :string)
      add(:mosaic_amount, :integer)
      add(:duration, :integer)
      add(:namespace_id, :string)
      add(:referenda_id, :binary_id)
      add(:nem_address, :string)

      timestamps()
    end
  end
end
