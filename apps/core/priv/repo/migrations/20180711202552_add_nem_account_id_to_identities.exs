defmodule Core.Repo.Migrations.AddNemAccountIdToIdentities do
  use Ecto.Migration

  def change do
    alter table("identities") do
      add(
        :nem_account_id,
        references(
          :nem_accounts,
          column: :address,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end
  end
end
