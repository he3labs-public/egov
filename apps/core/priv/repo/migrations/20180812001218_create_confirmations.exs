defmodule Core.Repo.Migrations.CreateConfirmations do
  use Ecto.Migration

  def change do
    create table(:confirmations, primary_key: false) do
      add(:pin, :string, primary_key: true)
      add(:election_id, :binary)
      add(:referenda_id, :binary)
      add(:voter_id, :binary)
      add(:tx_hash, :string, null: false)

      timestamps()
    end
  end
end
