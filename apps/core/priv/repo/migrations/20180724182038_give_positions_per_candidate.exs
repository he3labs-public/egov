defmodule Core.Repo.Migrations.GivePositionsPerCandidate do
  use Ecto.Migration

  def change do
    alter table(:voting_elections) do
      add(:candidates, {:array, :map})
      remove(:chairmen)
      remove(:vice_chairmen)
      remove(:secretaries)
      remove(:vice_secretaries)
    end
  end
end
