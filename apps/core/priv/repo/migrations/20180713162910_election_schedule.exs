defmodule Core.Repo.Migrations.ElectionSchedule do
  use Ecto.Migration

  def change do
    alter table("voting_elections") do
      add(:authorized_at, :naive_datetime)
      add(:opened_at, :naive_datetime)
    end
  end
end
