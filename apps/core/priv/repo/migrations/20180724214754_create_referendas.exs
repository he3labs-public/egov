defmodule Core.Repo.Migrations.CreateReferendas do
  use Ecto.Migration

  def change do
    create table(:referendas, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :measures, {:array, :map}

      timestamps()
    end

  end
end
