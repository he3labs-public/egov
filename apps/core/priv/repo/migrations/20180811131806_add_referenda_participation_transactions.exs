defmodule Core.Repo.Migrations.AddReferendaParticipationTransactions do
  use Ecto.Migration

  def change do
    alter table(:voting_referenda_participations) do
      add(
        :request_transaction_id,
        references(
          :nem_transactions,
          column: :tx_hash,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )

      add(
        :submit_transaction_id,
        references(
          :nem_transactions,
          column: :tx_hash,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end
  end
end
