defmodule Core.Repo.Migrations.AddReferendaSeedTransaction do
  use Ecto.Migration

  def change do
    alter table(:referenda_seeds) do
      add(
        :seed_transaction_id,
        references(
          :nem_transactions,
          column: :tx_hash,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end
  end
end
