defmodule Core.Repo.Migrations.CreateNemAccounts do
  use Ecto.Migration

  def change do
    create table(:nem_accounts, primary_key: false) do
      add(:address, :string, primary_key: true)
      add(:public_key, :string)
      add(:private_key, :string)

      timestamps()
    end

    create(unique_index(:nem_accounts, [:public_key]))
    create(unique_index(:nem_accounts, [:private_key]))
  end
end
