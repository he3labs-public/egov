defmodule Core.Repo.Migrations.NemTransactions do
  use Ecto.Migration

  def change do
    create table("nem_transactions", primary_key: false) do
      add(:tx_hash, :string, primary_key: true)
      add(:announced_at, :naive_datetime)

      add(:confirmed_at, :naive_datetime)

      add(:error, :string)
      add(:error_at, :naive_datetime)
    end

    alter table(:election_seeds) do
      add(
        :seed_transaction_id,
        references(
          :nem_transactions,
          column: :tx_hash,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end

    alter table(:bank_account_seeds) do
      add(
        :seed_transaction_id,
        references(
          :nem_transactions,
          column: :tx_hash,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end
  end
end
