defmodule Core.Repo.Migrations.CreateIdentities do
  use Ecto.Migration

  def change do
    create table(:identities, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:birthdate, :date)
      add(:blood_degree, :string)
      add(:email, :string)
      add(:first_name, :string)
      add(:last_name, :string)
      add(:mailing_address, :map)
      add(:phone, :string)

      timestamps()
    end
  end
end
