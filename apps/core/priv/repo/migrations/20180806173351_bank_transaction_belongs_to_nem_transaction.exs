defmodule Core.Repo.Migrations.BankTransactionBelongsToNemTransaction do
  use Ecto.Migration

  def change do
    alter table(:bank_transactions) do
      add(
        :nem_transaction_id,
        references(
          :nem_transactions,
          column: :tx_hash,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        )
      )
    end
  end
end
