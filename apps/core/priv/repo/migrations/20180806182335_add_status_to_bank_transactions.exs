defmodule Core.Repo.Migrations.AddStatusToBankTransactions do
  use Ecto.Migration

  def change do
    alter table(:bank_transactions) do
      add(:status, :string)
    end

    flush()
    Core.Repo.update_all("bank_transactions", set: [status: "pending"])

    alter table(:bank_transactions) do
      modify(:status, :string, null: false)
    end
  end
end
