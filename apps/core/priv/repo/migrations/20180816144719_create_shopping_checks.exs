defmodule Core.Repo.Migrations.CreateShoppingChecks do
  use Ecto.Migration

  def change do
    create table(:shopping_checks) do
      add :customer_id, :binary_id
      add :customer_name, :string
      add :status, :string, null: false
      add :total, :integer, null: false, default: 0
      add :items, {:array, :map}, null: false, default: []

      timestamps()
    end
  end
end
