defmodule Core.Repo.Migrations.CreateVotingRegistrations do
  use Ecto.Migration

  def change do
    create table(:voting_registrations, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:first_name, :string, null: false)
      add(:last_name, :string, null: false)
      add(:id_number, :string, null: false)

      timestamps()
    end
  end
end
