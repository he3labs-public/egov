defmodule Core.Repo.Migrations.AddVotedFieldToParticipation do
  use Ecto.Migration

  def change do
    alter table("voting_election_participations") do
      add(:voted, :boolean, null: false)
    end

    alter table("voting_referenda_participations") do
      add(:voted, :boolean, null: false)
    end
  end
end
