defmodule Core.Repo.Migrations.CreateBankTransactions do
  use Ecto.Migration

  def change do
    create table(:bank_transactions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :description, :string
      add :amount, :integer
      add :from_id, references(:bank_accounts, column: :id_number, on_delete: :nothing, type: :binary_id)
      add :to_id, references(:bank_accounts, column: :id_number, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:bank_transactions, [:from_id])
    create index(:bank_transactions, [:to_id])
  end
end
