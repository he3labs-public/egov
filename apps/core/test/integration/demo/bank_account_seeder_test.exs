defmodule Core.Demo.BankAccountSeederTest do
  use Core.DataCase
  @moduletag :external

  alias Core.Demo.BankAccount

  setup_all [:use_exnem_observer, :seed_account]

  describe "when message \"observerReady\" is received" do
    test "it should announce the seed transaction" do
      Core.subscribe("demo:bank_account_seed_announced")
      seed = create_seed()
      refute seed.seed_transaction.announced_at

      {:ok, seeder} = BankAccount.Seeder.start(seed)
      send(seeder, "observerReady")

      assert_receive(
        {"demo:bank_account_seed_announced",
         %BankAccount.Seed{seed_transaction: announced_tx = %Core.NemTransaction{}}},
        1000
      )

      assert announced_tx.announced_at
      assert announced_tx.tx_hash
    end
  end

  describe "when message {\"confirmedAdded\", dto} is received" do
    test "it should broadcast on \"demo:bank_account_seeded\"" do
      Core.subscribe("demo:bank_account_seeded")
      seed = create_seed()
      {:ok, seeder} = BankAccount.Seeder.start(seed)

      send(seeder, {"confirmedAdded", %{}})
      assert_receive({"demo:bank_account_seeded", %BankAccount.Seed{}}, 1000)
    end
  end

  describe "when message {\"error\", error} is received" do
    test "it should broadcast on \"demo:bank_account_seeding_failed\"" do
      Core.subscribe("demo:bank_account_seeding_failed")
      seed = create_seed()

      error = %{
        meta: %{hash: "ABC1234"},
        status: "Something went wrong",
        deadline: Exnem.deadline()
      }

      {:ok, seeder} = BankAccount.Seeder.start(seed)

      send(seeder, {"error", error})

      assert_receive(
        {"demo:bank_account_seeding_failed",
         %BankAccount.Seed{
           seed_transaction: %Core.NemTransaction{error: "Something went wrong"}
         }},
        1000
      )
    end
  end

  defp create_seed do
    nem_account = insert(:nem_account)

    bank_account = insert(:bank_account, nem_account: nem_account)

    insert(
      :bank_account_seed,
      banking_account_id: bank_account.id,
      nem_address: nem_account.address
    )
  end

  defp seed_account(_) do
    # Ecto.Adapters.SQL.Sandbox.mode(Core.Repo, {:shared, self()})

    Demo.Generate.main_token()

    :ok
  end
end
