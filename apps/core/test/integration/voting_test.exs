defmodule Core.Integration.VotingTest do
  use Core.DataCase
  @moduletag :external

  alias Core.{Demo, Voting, Factory, Repo}

  setup_all do
    Application.put_env(:core, :exnem_observer, Exnem.Observer)
    Application.put_env(:core, :voting_workers_enabled, true)

    :ok
  end

  describe "referenda vote test" do
    test "open referenda, request ballot, submit ballot, tally that vote" do
      referenda =
        insert(:voting_referenda,
          finalized_at: Core.naive_now(-1, :sec),
          end_date: Core.naive_now(100, :sec)
        )

      Core.subscribe("voting:referenda_seeded")
      Core.subscribe("voting:referenda_seeding_failed")

      {:ok, seed} = Voting.open_referenda(referenda.id)

      referenda =
        Core.Voting.get_referenda!(seed.referenda_id)
        |> Repo.preload(:nem_account)

      receive do
        {"voting:referenda_seeded", _} -> :noop
        {"voting:referenda_seeding_failed", _} -> flunk("seeding failed")
      after
        20000 -> flunk("timeout seeding")
      end

      voter = insert(:identity)

      Core.subscribe("ballot_ready")
      Core.subscribe("voting:ballot_request_failed")

      assert {:ok, a} = Demo.request_referenda_ballot(referenda.id, voter.id)

      receive do
        {"ballot_ready", _} -> :noop
        {"voting:ballot_request_failed", _} -> flunk("request failed")
      after
        20000 -> flunk("timeout request")
      end

      submission_payload = Factory.referenda_ballot_submission(referenda) |> Map.from_struct()

      Core.subscribe("ballot_confirmed")
      Core.subscribe("voting:ballot_submit_failed")

      assert :ok =
               Demo.submit_referenda_ballot(
                 referenda.id,
                 voter.id,
                 submission_payload
               )

      receive do
        {"ballot_confirmed", _} -> :noop
        {"voting:ballot_submit_failed", _} -> flunk("request failed")
      after
        20000 -> flunk("timeout submit")
      end

      final_tally = Voting.tally_referenda(referenda)

      empty_entry = %{abstain: 0, no: 0, yes: 0}

      compare_tally =
        referenda.measures
        |> Enum.reduce(%{}, fn m, acc -> Map.put(acc, m.id, empty_entry) end)

      compare_tally =
        submission_payload.referenda
        |> Enum.reduce(
          compare_tally,
          fn x, acc ->
            Map.update!(acc, x.id, &integer_to_vote(&1, x.vote))
          end
        )

      assert {compare_tally, 0, 1} == final_tally
    end

    defp integer_to_vote(map, value) when is_integer(value) do
      case value do
        -1 -> %{map | no: map.no + 1}
        0 -> %{map | abstain: map.no + 1}
        1 -> %{map | yes: map.no + 1}
      end
    end
  end

  describe "election vote test" do
    test "open election, request ballot, submit ballot, tally that vote" do
      election =
        insert(:voting_election,
          finalized_at: Core.naive_now(-1, :sec),
          end_date: Core.naive_now(100, :sec)
        )

      Core.subscribe("voting:election_seeded")
      Core.subscribe("voting:election_seeding_failed")

      {:ok, seed} = Voting.open_election(election.id)

      election =
        Core.Voting.get_election!(seed.election_id)
        |> Repo.preload(:nem_account)

      receive do
        {"voting:election_seeded", _} -> :noop
        {"voting:election_seeding_failed", _} -> flunk("seeding failed")
      after
        20000 -> flunk("timeout seeding")
      end

      voter = insert(:identity)

      Core.subscribe("ballot_ready")
      Core.subscribe("voting:ballot_request_failed")

      assert {:ok, a} = Demo.request_election_ballot(election.id, voter.id)

      receive do
        {"ballot_ready", _} -> :noop
        {"voting:ballot_request_failed", _} -> flunk("request failed")
      after
        20000 -> flunk("timeout request")
      end

      submission_payload =
        Factory.election_ballot_submission(election)
        |> Map.from_struct()

      Core.subscribe("ballot_confirmed")
      Core.subscribe("voting:ballot_submit_failed")

      assert :ok =
               Demo.submit_election_ballot(
                 election.id,
                 voter.id,
                 submission_payload
               )

      receive do
        {"ballot_confirmed", _} -> :noop
        {"voting:ballot_submit_failed", _} -> flunk("request failed")
      after
        20000 -> flunk("timeout submit")
      end

      final_tally = Voting.tally_election(election)
      empty_tally = Voting.Election.Tally.initialize_election_tally_slate(election.candidates)

      compare_tally =
        submission_payload.candidates
        |> Enum.reduce(empty_tally, fn c, acc ->
          update_in(acc, [c.position, c.id, :count], &(&1 + 1))
        end)

      assert {compare_tally, 0, 1} == final_tally
    end
  end
end
