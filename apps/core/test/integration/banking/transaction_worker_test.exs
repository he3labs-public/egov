defmodule Core.Banking.Transaction.WorkerTest do
  use Core.DataCase
  @moduletag :external

  require Logger

  alias Core.Banking
  alias Core.Banking.Transaction

  setup_all [:use_exnem_observer, :generate_main_token]

  describe "start/1 when the transaction does not exist" do
    test "it should fail with an error" do
      assert {:error, {%Ecto.NoResultsError{}, _}} =
               Banking.Transaction.Worker.start(%Transaction{id: Ecto.UUID.generate()})
    end
  end

  describe "start/1 when the sender does NOT have enough funds" do
    test "should fail the transaction" do
      empty_account = insert(:bank_account)
      transaction = %Transaction{id: id} = insert(:bank_transaction, from: empty_account)

      Core.subscribe("banking:transaction_failed")

      {:ok, _pid} = Banking.Transaction.Worker.start(transaction)

      assert_receive({"banking:transaction_failed", %Transaction{id: ^id}=failed_transaction}, 2000)
      assert failed_transaction.status == "failed"
      assert failed_transaction.nem_transaction.announced_at
      assert failed_transaction.nem_transaction.error_at
    end
  end

  describe "start/1 when the sender has enough funds" do
    setup [:create_transaction, :fund_transaction_sender]

    test "should complete the transaction", %{transaction: %Transaction{id: id}=transaction} do
      Core.subscribe("banking:transaction_completed")

      {:ok, _pid} = Banking.Transaction.Worker.start(transaction)

      timeout = Exnem.Config.block_duration() * 2 * 1000
      assert_receive({"banking:transaction_completed", %Transaction{id: ^id}=completed_tx}, timeout)
      assert completed_tx.status == "completed"
      assert completed_tx.nem_transaction.announced_at
      assert completed_tx.nem_transaction.confirmed_at
    end
  end

  defp create_transaction(_context) do
    %{transaction: insert(:bank_transaction)}
  end

  defp fund_transaction_sender(%{transaction: transaction}) do
    with {:ok, _confirmation} <- provision_bank_account(transaction.from, transaction.amount) do
      {:ok, %{transaction: transaction}}
    end
  end

  defp provision_bank_account(%Banking.Account{} = account, amount) when is_integer(amount) do
    mosaic_id = Core.Demo.usd_mosaic_id()
    {:ok, mosaic} = Exnem.Transaction.mosaic(mosaic_id, amount)
    {:ok, tx} = Exnem.Transaction.transfer(account.nem_account_id, [mosaic])

    payload =
      tx
      |> Exnem.Transaction.pack()
      |> Exnem.Crypto.KeyPair.sign(Core.Demo.demo_lord_key_pair())

    Exnem.Announce.sync_announce(payload)
  end
end
