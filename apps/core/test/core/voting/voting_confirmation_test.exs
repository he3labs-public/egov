defmodule Core.VotingTest do
  use Core.DataCase

  alias Core.Voting

  describe "voting_confirmations" do
    alias Core.Voting.Confirmation

    setup do
      params = params_for(:voting_confirmation)
      %{params: params}
    end

    @invalid_attrs %{election_id: nil, pin: nil, referenda_id: nil, tx_hash: nil, voter_id: nil}

    def confirmation_fixture(params) do
      {:ok, confirmation} = Voting.create_confirmation(params)

      confirmation
    end

    test "list_confirmations/0 returns all confirmations", %{params: params} do
      confirmation = confirmation_fixture(params)
      assert Voting.list_confirmations() == [confirmation]
    end

    test "get_confirmation!/1 returns the confirmation with given id", %{params: params} do
      conf = confirmation_fixture(params)
      assert Voting.get_confirmation!(conf.pin) == conf
    end

    test "create_confirmation/1 with valid data creates a confirmation", %{params: params} do
      assert {:ok, %Confirmation{} = conf} = Voting.create_confirmation(params)
      assert conf.pin == params.pin
      assert conf.election_id == params.election_id
      # assert conf.referenda_id == params.referenda_id
      assert conf.tx_hash == params.tx_hash
      assert conf.voter_id == params.voter_id
    end

    test "create_confirmation/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Voting.create_confirmation(@invalid_attrs)
    end

    test "update_confirmation/2 with valid data updates the confirmation", %{params: params} do
      old_confirmation = confirmation_fixture(params)
      new_params = params_for(:voting_confirmation)

      assert {:ok, conf} = Voting.update_confirmation(old_confirmation, new_params)
      assert %Confirmation{} = conf
      assert conf.election_id == new_params.election_id
      # assert conf.referenda_id == new_params.referenda_id
      assert conf.tx_hash == new_params.tx_hash
      assert conf.voter_id == new_params.voter_id
    end

    test "update_confirmation/2 with invalid data returns error changeset", %{params: params} do
      conf = confirmation_fixture(params)

      assert {:error, %Ecto.Changeset{}} = Voting.update_confirmation(conf, @invalid_attrs)

      assert conf == Voting.get_confirmation!(conf.pin)
    end

    test "delete_confirmation/1 deletes the confirmation", %{params: params} do
      conf = confirmation_fixture(params)
      assert {:ok, %Confirmation{}} = Voting.delete_confirmation(conf)
      assert_raise Ecto.NoResultsError, fn -> Voting.get_confirmation!(conf.pin) end
    end

    test "change_confirmation/1 returns a confirmation changeset", %{params: params} do
      conf = confirmation_fixture(params)
      assert %Ecto.Changeset{} = Voting.change_confirmation(conf)
    end
  end
end
