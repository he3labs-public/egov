defmodule Core.VotingReferendaTest do
  use Core.DataCase

  alias Core.Voting

  describe "voting_referendas" do
    alias Core.Voting.Referenda

    setup do
      params = params_for(:voting_referenda)
      %{params: params}
    end

    @invalid_attrs %{end_date: nil, start_date: nil, candidates: nil}

    def referenda_fixture(params) do
      {:ok, referenda} = Voting.create_referenda(params)

      referenda
    end

    test "list_referendas/0 returns all referendas", %{params: params} do
      referenda = referenda_fixture(params)
      assert Voting.list_referendas() == [referenda]
    end

    test "get_referenda!/1 returns the referenda with given id", %{params: params} do
      referenda = referenda_fixture(params)
      assert Voting.get_referenda!(referenda.id) == referenda
    end

    test "create_referenda/1 with valid data creates a referenda", %{params: params} do
      assert {:ok, %Referenda{} = referenda} = Voting.create_referenda(params)

      params.measures
      |> Enum.zip(referenda.measures)
      |> Enum.each(fn {params, record} ->
        assert params.name == record.name
        assert params.title == record.title
        assert params.overview == record.overview
      end)
    end

    test "create_referenda/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Voting.create_referenda(@invalid_attrs)
    end

    test "update_referenda/2 with valid data updates the referenda", %{params: params} do
      referenda = referenda_fixture(params)
      new_measure = params_for(:voting_measure)
      new_measures = [new_measure]

      assert {:ok, referenda} = Voting.update_referenda(referenda, %{measures: new_measures})
      assert %Referenda{} = referenda

      new_measures
      |> Enum.zip(referenda.measures)
      |> Enum.each(fn {params, record} ->
        assert params.name == record.name
        assert params.title == record.title
        assert params.overview == record.overview
      end)
    end

    test "update_referenda/2 with invalid data returns error changeset", %{params: params} do
      referenda = referenda_fixture(params)
      assert {:error, %Ecto.Changeset{}} = Voting.update_referenda(referenda, @invalid_attrs)
      assert referenda == Voting.get_referenda!(referenda.id)
    end

    test "delete_referenda/1 deletes the referenda", %{params: params} do
      referenda = referenda_fixture(params)
      assert {:ok, %Referenda{}} = Voting.delete_referenda(referenda)
      assert_raise Ecto.NoResultsError, fn -> Voting.get_referenda!(referenda.id) end
    end

    test "change_referenda/1 returns a referenda changeset", %{params: params} do
      referenda = referenda_fixture(params)
      assert %Ecto.Changeset{} = Voting.change_referenda(referenda)
    end

    # --------------------------------------------------------------------------------

    test "seed_referenda_account/1 with referenda record returns a seed" do
      referenda = insert(:voting_referenda)
      assert {:ok, %Referenda.Seed{}} = Voting.seed_referenda_account(referenda)
    end

    test "open_referenda/1 with a non-finalized referenda id returns an error" do
      referenda = insert(:voting_referenda)
      assert {:error, %Ecto.Changeset{}} = Voting.open_referenda(referenda.id)
    end

    test "open_referenda/1 with a valid referenda id returns a seed" do
      referenda = insert(:voting_referenda, finalized_at: Core.naive_now(-1))
      assert {:ok, %Referenda.Seed{}} = Voting.open_referenda(referenda.id)
    end

    test "request_referenda_ballot/3 with valid data returns a participation record" do
      referenda = insert(:voting_referenda, opened_at: Core.naive_now(-1))
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:ok, %Referenda.Participation{} = participation} =
               Voting.request_referenda_ballot(referenda.id, voter.id, ballot_nem_account.address)

      assert participation.referenda_id == referenda.id
      assert participation.voter_id == voter.id
      assert participation.ballot_address == ballot_nem_account.address
    end

    test "request_referenda_ballot/3 with invalid data returns an error" do
      referenda = insert(:voting_referenda, opened_at: Core.naive_now(-1))
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:error, %Ecto.Changeset{}} =
               Voting.request_referenda_ballot(referenda.id, voter.id, nil)

      assert {:error, %Ecto.Changeset{}} =
               Voting.request_referenda_ballot(nil, voter.id, ballot_nem_account.address)

      assert {:error, %Ecto.Changeset{}} =
               Voting.request_referenda_ballot(referenda.id, nil, ballot_nem_account.address)
    end

    test "request_referenda_ballot/3 with unopened referenda returns an error" do
      referenda = insert(:voting_referenda)
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:error, "Referenda not running"} =
               Voting.request_referenda_ballot(referenda.id, voter.id, ballot_nem_account.address)
    end

    test "request_referenda_ballot/3 after end date returns an error" do
      referenda =
        insert(:voting_referenda,
          opened_at: Core.naive_now(-1, :sec),
          end_date: Core.naive_now(-1, :sec)
        )

      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:error, "Referenda not running"} =
               Voting.request_referenda_ballot(referenda.id, voter.id, ballot_nem_account.address)
    end

    test "submit_referenda_ballot/4 with unopened referenda returns an error" do
      referenda = insert(:voting_referenda)
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)
      submission_payload = Faker.String.base64(100)

      assert {:error, "Referenda not running"} =
               Voting.submit_referenda_ballot(
                 referenda.id,
                 voter.id,
                 ballot_nem_account.address,
                 submission_payload
               )
    end

    test "submit_referenda_ballot/4 with valid data returns an ok" do
      referenda = insert(:voting_referenda, opened_at: Core.naive_now(-1))
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      Voting.request_referenda_ballot(referenda.id, voter.id, ballot_nem_account.address)

      submission_payload = Faker.String.base64(100)

      assert :ok ==
               Voting.submit_referenda_ballot(
                 referenda.id,
                 voter.id,
                 ballot_nem_account.address,
                 submission_payload
               )
    end

    test "submit_referenda_ballot/4 with past referenda returns an error" do
      referenda =
        insert(:voting_referenda,
          opened_at: Core.naive_now(-1, :sec),
          end_date: Core.naive_now(-1, :sec)
        )

      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      submission_payload = Faker.String.base64(100)

      assert {:error, "Referenda not running"} =
               Voting.submit_referenda_ballot(
                 referenda.id,
                 voter.id,
                 ballot_nem_account.address,
                 submission_payload
               )
    end

    test "tally_referenda for a newly created referenda returns a tally with all 0s" do
      referenda = insert(:voting_referenda)

      final_count = Voting.tally_referenda(referenda)
      compare = Voting.Referenda.Tally.initialize_referenda_tally_slate(referenda.measures)

      assert {compare, 0, 0} == final_count
    end
  end
end
