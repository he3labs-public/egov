defmodule Core.VotingElectionTest do
  use Core.DataCase

  alias Core.Voting

  describe "voting_elections" do
    alias Core.Voting.Election

    setup do
      params = params_for(:voting_election)
      %{params: params}
    end

    @invalid_attrs %{end_date: nil, start_date: nil, candidates: nil}

    def election_fixture(params) do
      {:ok, election} = Voting.create_election(params)

      election
    end

    test "list_voting_elections/0 returns all voting_elections", %{params: params} do
      election = election_fixture(params)
      assert Voting.list_voting_elections() == [election]
    end

    test "get_election!/1 returns the election with given id", %{params: params} do
      election = election_fixture(params)
      assert Voting.get_election!(election.id) == election
    end

    test "create_election/1 with valid data creates a election", %{params: params} do
      assert {:ok, %Election{} = election} = Voting.create_election(params)

      candidates_to_compare =
        election.candidates
        |> Enum.map(&Map.from_struct/1)
        |> Enum.map(&Map.delete(&1, :id))

      assert candidates_to_compare == params.candidates
      assert election.end_date == params.end_date
      assert election.start_date == params.start_date
    end

    test "create_election/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Voting.create_election(@invalid_attrs)
    end

    test "update_election/2 with valid data updates the election", %{params: params} do
      old_election = election_fixture(params)
      new_candidate = params_for(:candidate)
      new_candidates = [new_candidate]

      assert {:ok, updated_election} =
               Voting.update_election(old_election, %{candidates: new_candidates})

      assert %Election{} = updated_election

      updated_candidates_to_compare =
        updated_election.candidates
        |> Enum.map(&Map.from_struct/1)
        |> Enum.map(&Map.delete(&1, :id))

      assert updated_candidates_to_compare == new_candidates
      assert updated_election.end_date == old_election.end_date
      assert updated_election.start_date == old_election.start_date
    end

    test "update_election/2 with invalid data returns error changeset", %{params: params} do
      election = election_fixture(params)
      assert {:error, %Ecto.Changeset{}} = Voting.update_election(election, @invalid_attrs)
      assert election == Voting.get_election!(election.id)
    end

    test "delete_election/1 deletes the election", %{params: params} do
      election = election_fixture(params)
      assert {:ok, %Election{}} = Voting.delete_election(election)

      assert_raise Ecto.NoResultsError, fn -> Voting.get_election!(election.id) end
    end

    test "change_election/1 returns a election changeset", %{params: params} do
      election = election_fixture(params)
      assert %Ecto.Changeset{} = Voting.change_election(election)
    end

    # ------------------------------------------------------------------------

    test "seed_election_account/1 with election record returns a seed" do
      election = insert(:voting_election)
      assert {:ok, %Election.Seed{}} = Voting.seed_election_account(election)
    end

    test "open_election/1 with a non-finalized election id returns an error" do
      election = insert(:voting_election)
      assert {:error, %Ecto.Changeset{}} = Voting.open_election(election.id)
    end

    test "open_election/1 with a valid election id returns a seed" do
      election = insert(:voting_election, finalized_at: Core.naive_now(-1))
      assert {:ok, %Election.Seed{}} = Voting.open_election(election.id)
    end

    test "request_election_ballot/3 with valid data returns a participation record" do
      election = insert(:voting_election, opened_at: Core.naive_now(-1))
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:ok, %Election.Participation{} = participation} =
               Voting.request_election_ballot(election.id, voter.id, ballot_nem_account.address)

      assert participation.election_id == election.id
      assert participation.voter_id == voter.id
      assert participation.ballot_address == ballot_nem_account.address
    end

    test "request_election_ballot/3 with invalid data returns an error" do
      election = insert(:voting_election, opened_at: Core.naive_now(-1))
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:error, %Ecto.Changeset{}} =
               Voting.request_election_ballot(election.id, voter.id, nil)

      assert {:error, %Ecto.Changeset{}} =
               Voting.request_election_ballot(nil, voter.id, ballot_nem_account.address)

      assert {:error, %Ecto.Changeset{}} =
               Voting.request_election_ballot(election.id, nil, ballot_nem_account.address)
    end

    test "request_election_ballot/3 with unopened election returns an error" do
      election = insert(:voting_election)
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:error, "Election not running"} =
               Voting.request_election_ballot(election.id, voter.id, ballot_nem_account.address)
    end

    test "request_election_ballot/3 after end date returns an error" do
      election =
        insert(:voting_election,
          opened_at: Core.naive_now(-1, :sec),
          end_date: Core.naive_now(-1, :sec)
        )

      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      assert {:error, "Election not running"} =
               Voting.request_election_ballot(election.id, voter.id, ballot_nem_account.address)
    end

    test "submit_election_ballot/4 with unopened election returns an error" do
      election = insert(:voting_election)
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)
      submission_payload = Faker.String.base64(100)

      assert {:error, "Election not running"} =
               Voting.submit_election_ballot(
                 election.id,
                 voter.id,
                 ballot_nem_account.address,
                 submission_payload
               )
    end

    test "submit_election_ballot/4 with valid data returns an ok" do
      election = insert(:voting_election, opened_at: Core.naive_now(-1))
      voter = insert(:identity)
      ballot_nem_account = insert(:nem_account)

      Voting.request_election_ballot(election.id, voter.id, ballot_nem_account.address)
      submission_payload = Faker.String.base64(100)

      assert :ok ==
               Voting.submit_election_ballot(
                 election.id,
                 voter.id,
                 ballot_nem_account.address,
                 submission_payload
               )
    end

    test "tally_election for a newly created election returns a tally with all 0s" do
      election = insert(:voting_election)
      final_count = Voting.tally_election(election)
      empty_tally = Voting.Election.Tally.initialize_election_tally_slate(election.candidates)

      assert {empty_tally, 0, 0} == final_count
    end
  end
end
