defmodule Core.VotingRegistrationTest do
  use Core.DataCase

  alias Core.Voting

  describe "voting_registrations" do
    alias Core.Voting.Registration

    setup do
      identity = insert(:identity)
      params = params_for(:voting_registration, id_number: identity.id)

      %{identity: identity, params: params}
    end

    @invalid_attrs %{first_name: nil, id_number: nil, last_name: nil}

    def registration_fixture_from_id(%{
          first_name: first_name,
          last_name: last_name,
          id: id_number
        }) do
      params = %{first_name: first_name, last_name: last_name, id_number: id_number}
      {:ok, registration} = Voting.create_registration(params)

      registration
    end

    test "list_voting_registrations/0 returns all voting_registrations", %{identity: identity} do
      registration = registration_fixture_from_id(identity)
      assert Voting.list_voting_registrations() == [registration]
    end

    test "get_registration!/1 returns the registration with given id", %{identity: identity} do
      registration = registration_fixture_from_id(identity)
      assert Voting.get_registration!(registration.id) == registration
    end

    test "create_registration/1 with valid data creates a registration", %{params: params} do
      assert {:ok, %Registration{} = registration} = Voting.create_registration(params)
      assert registration.first_name == params.first_name
      assert registration.last_name == params.last_name
      assert registration.id_number == params.id_number
    end

    test "create_registration/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Voting.create_registration(@invalid_attrs)
    end

    test "update_registration/2 with valid data updates the registration", %{identity: identity} do
      registration = registration_fixture_from_id(identity)
      new_identity = build(:identity)

      assert {:ok, registration} =
               Voting.update_registration(registration, %{identity: new_identity})

      assert %Registration{} = registration
    end

    test "update_registration/2 with invalid data returns error changeset", %{identity: identity} do
      registration = registration_fixture_from_id(identity)

      assert {:error, %Ecto.Changeset{}} =
               Voting.update_registration(registration, @invalid_attrs)

      assert registration == Voting.get_registration!(registration.id)
    end

    test "delete_registration/1 deletes the registration", %{identity: identity} do
      registration = registration_fixture_from_id(identity)
      assert {:ok, %Registration{}} = Voting.delete_registration(registration)
      assert_raise Ecto.NoResultsError, fn -> Voting.get_registration!(registration.id) end
    end

    test "change_registration/1 returns a registration changeset", %{identity: identity} do
      registration = registration_fixture_from_id(identity)
      assert %Ecto.Changeset{} = Voting.change_registration(registration)
    end

    # ------------------------------------------------------------------------

    test "create_registration/1 with valid data broadcasts on \"voting:voter_registered\"", %{
      params: params
    } do
      Core.subscribe("voting:voter_registered")

      {:ok, account} = Voting.create_registration(params)

      assert_received({"voting:voter_registered", ^account})
    end

    test "create_registration/1 with invalid data broadcasts on \"voting:voter_registration_failed\"" do
      Core.subscribe("voting:voter_registration_failed")

      {:error, changeset} = Voting.create_registration(@invalid_attrs)

      assert_received({"voting:voter_registration_failed", ^changeset})
    end
  end
end
