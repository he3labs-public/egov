defmodule Core.ShoppingTest do
  use Core.DataCase

  alias Core.Shopping

  describe "products" do
    alias Core.Shopping.Product

    @valid_attrs %{id: "8c2f6b89-373e-4f76-915f-6c7bb0c328d1", name: "some name", price: 42}
    @update_attrs %{
      id: "8c2f6b89-373e-4f76-915f-6c7bb0c328d1",
      name: "some updated name",
      price: 43
    }
    @invalid_attrs %{id: nil, name: nil, price: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Shopping.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Shopping.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Shopping.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Shopping.create_product(@valid_attrs)
      assert product.name == "some name"
      assert product.price == 42
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shopping.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, product} = Shopping.update_product(product, @update_attrs)
      assert %Product{} = product
      assert product.name == "some updated name"
      assert product.price == 43
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Shopping.update_product(product, @invalid_attrs)
      assert product == Shopping.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Shopping.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Shopping.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Shopping.change_product(product)
    end
  end

  describe "checks" do
    alias Core.Shopping.Check

    @valid_attrs %{
      customer_id: "7488a646-e31f-11e4-aace-600308960662",
      customer_name: "Agnes Varenne",
      status: "open"
    }
    @update_attrs %{customer_id: "7488a646-e31f-11e4-aace-600308960668", status: "closed"}
    @invalid_attrs %{customer_id: nil, status: nil}

    def check_fixture(attrs \\ %{}) do
      {:ok, check} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Shopping.create_check()

      check
    end

    test "create_check/1 with valid data creates a check" do
      assert {:ok, %Check{} = check} = Shopping.create_check(@valid_attrs)
      assert check.customer_id == "7488a646-e31f-11e4-aace-600308960662"
      assert check.customer_name == "Agnes Varenne"
      assert check.status == "open"
    end

    test "create_check/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Shopping.create_check(@invalid_attrs)
    end

    test "current_check/0 with existing checks should return the last check" do
      assert {:ok, %Check{}} = Shopping.create_check(@valid_attrs)
      assert {:ok, %Check{} = check2} = Shopping.create_check(@valid_attrs)

      current_check = Shopping.current_check()
      assert %Check{} = current_check
      
      assert current_check == check2
    end

    test "current_check/0 with no existing checks should create a check" do
      assert length(Shopping.list_checks()) == 0

      current_check_a = Shopping.current_check()
      assert %Check{} = current_check_a
      assert length(Shopping.list_checks()) == 1

      current_check_b = Shopping.current_check()
      assert %Check{} = current_check_b
      assert length(Shopping.list_checks()) == 1

      assert current_check_a == current_check_b
    end

    test "update_check/2 with valid data updates the check" do
      check = check_fixture()
      assert {:ok, check} = Shopping.update_check(check, @update_attrs)
      assert %Check{} = check
      assert check.customer_id == "7488a646-e31f-11e4-aace-600308960668"
      assert check.status == "closed"
    end

    test "update_check/2 with invalid data returns error changeset" do
      check = check_fixture()
      assert {:error, %Ecto.Changeset{}} = Shopping.update_check(check, @invalid_attrs)
    end

    test "change_check/1 returns a check changeset" do
      check = check_fixture()
      assert %Ecto.Changeset{} = Shopping.change_check(check)
    end
  end
end
