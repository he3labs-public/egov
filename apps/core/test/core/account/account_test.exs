defmodule Core.AccountTest do
  use Core.DataCase

  alias Core.NemAccount
  alias Core.Account
  alias Core.Account.{ApiToken, Identity, MailingAddress}

  describe "identities" do
    @valid_attrs params_for(:identity)
    @update_attrs params_for(:identity)
    @invalid_attrs %{birthdate: nil, email: nil, first_name: nil, last_name: nil}

    def identity_fixture(attrs \\ %{}) do
      {:ok, identity} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_identity()

      identity
    end

    test "list_identities/0 returns all identities" do
      identity = identity_fixture() |> Core.Repo.preload(:api_token)
      assert Account.list_identities() == [identity]
    end

    test "get_identity!/1 returns the identity with given id" do
      identity = identity_fixture()
      assert Account.get_identity!(identity.id) == identity
    end

    test "create_identity/1 with valid data creates a identity" do
      assert {:ok, %Identity{} = identity} = Account.create_identity(@valid_attrs)
      assert identity.birthdate == @valid_attrs.birthdate
      assert identity.email == @valid_attrs.email
      assert identity.first_name == @valid_attrs.first_name
      assert identity.last_name == @valid_attrs.last_name

      assert %MailingAddress{} = identity.mailing_address
      assert Map.from_struct(identity.mailing_address) == @valid_attrs.mailing_address
    end

    test "create_identity/1 with valid data creates an associated NemAccount" do
      {:ok, identity} = Account.create_identity(@valid_attrs)
      identity = identity |> Repo.preload(:nem_account)

      assert %NemAccount{} = identity.nem_account
    end

    test "create_identity/1 with valid data creates an associated ApiToken" do
      {:ok, identity} = Account.create_identity(@valid_attrs)
      identity = identity |> Repo.preload(:api_token)

      assert %ApiToken{} = identity.api_token
    end

    test "create_identity/1 with valid data broadcasts on \"account:identity_created\"" do
      Core.subscribe("account:identity_created")

      {:ok, identity} = Account.create_identity(@valid_attrs)

      assert_received({"account:identity_created", ^identity})
    end

    test "create_identity/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_identity(@invalid_attrs)
    end

    test "create_identity/1 with invalid data broadcasts on \"account:identity_creation_failed\"" do
      Core.subscribe("account:identity_creation_failed")

      {:error, changeset} = Account.create_identity(@invalid_attrs)

      assert_received({"account:identity_creation_failed", ^changeset})
    end

    test "update_identity/2 with valid data updates the identity" do
      identity = identity_fixture()
      assert {:ok, identity} = Account.update_identity(identity, @update_attrs)
      assert %Identity{} = identity

      assert identity.birthdate == @update_attrs.birthdate
      assert identity.email == @update_attrs.email
      assert identity.first_name == @update_attrs.first_name
      assert identity.last_name == @update_attrs.last_name
      assert Map.from_struct(identity.mailing_address) == @update_attrs.mailing_address
    end

    test "update_identity/2 with invalid data returns error changeset" do
      identity = identity_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_identity(identity, @invalid_attrs)
      assert identity == Account.get_identity!(identity.id)
    end

    test "delete_identity/1 deletes the identity" do
      identity = identity_fixture()
      assert {:ok, %Identity{}} = Account.delete_identity(identity)
      assert_raise Ecto.NoResultsError, fn -> Account.get_identity!(identity.id) end
    end

    test "change_identity/1 returns a identity changeset" do
      identity = identity_fixture()
      assert %Ecto.Changeset{} = Account.change_identity(identity)
    end
  end

  describe "api_tokens" do
    alias Core.Account.ApiToken

    @valid_attrs %{token: "some token"}
    @update_attrs %{token: "some updated token"}
    @invalid_attrs %{token: nil}

    def api_token_fixture(attrs \\ %{}) do
      {:ok, api_token} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_api_token()

      api_token
    end

    test "list_api_tokens/0 returns all api_tokens" do
      api_token = api_token_fixture()
      assert Account.list_api_tokens() == [api_token]
    end

    test "get_api_token!/1 returns the api_token with given id" do
      api_token = api_token_fixture()
      assert Account.get_api_token!(api_token.id) == api_token
    end

    test "create_api_token/1 with valid data creates a api_token" do
      assert {:ok, %ApiToken{} = api_token} = Account.create_api_token(@valid_attrs)
      assert api_token.token == "some token"
    end

    test "create_api_token/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_api_token(@invalid_attrs)
    end

    test "update_api_token/2 with valid data updates the api_token" do
      api_token = api_token_fixture()
      assert {:ok, api_token} = Account.update_api_token(api_token, @update_attrs)
      assert %ApiToken{} = api_token
      assert api_token.token == "some updated token"
    end

    test "update_api_token/2 with invalid data returns error changeset" do
      api_token = api_token_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_api_token(api_token, @invalid_attrs)
      assert api_token == Account.get_api_token!(api_token.id)
    end

    test "delete_api_token/1 deletes the api_token" do
      api_token = api_token_fixture()
      assert {:ok, %ApiToken{}} = Account.delete_api_token(api_token)
      assert_raise Ecto.NoResultsError, fn -> Account.get_api_token!(api_token.id) end
    end

    test "change_api_token/1 returns a api_token changeset" do
      api_token = api_token_fixture()
      assert %Ecto.Changeset{} = Account.change_api_token(api_token)
    end
  end
end
