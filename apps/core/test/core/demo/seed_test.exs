defmodule Core.Demo.SeedTest do
  use Core.DataCase

  alias Core.Demo.Seed
  alias Core.{Account, Shopping, Voting}

  describe "identities/1" do
    setup do
      %{number_identities: :rand.uniform(5)}
    end

    test "should create the given number of identities", %{number_identities: number_identities} do
      assert Account.count_identities() == 0

      [%Account.Identity{} | _more] = Seed.identities(number_identities)

      assert Account.count_identities() == number_identities
    end
  end

  describe "voting/1" do
    test "election(hours: duration) should create 1 election" do
      assert Voting.count_elections() == 0

      Seed.election(hours: 8)

      assert Voting.count_elections() == 1

      [election | _] = Voting.list_voting_elections()

      eight_hours_from_now =
        Core.naive_now(8, :hour) |> DateTime.from_naive!("Etc/UTC") |> DateTime.to_unix()

      end_date = DateTime.from_naive!(election.end_date, "Etc/UTC") |> DateTime.to_unix()

      assert_in_delta end_date, eight_hours_from_now, 5
    end

    test "referenda(hours: duration) should create 1 referenda" do
      assert Voting.count_referenda() == 0

      Seed.referenda(hours: 8)

      assert Voting.count_referenda() == 1

      [referenda | _] = Voting.list_referendas()

      eight_hours_from_now =
        Core.naive_now(8, :hour) |> DateTime.from_naive!("Etc/UTC") |> DateTime.to_unix()

      end_date = DateTime.from_naive!(referenda.end_date, "Etc/UTC") |> DateTime.to_unix()

      assert_in_delta end_date, eight_hours_from_now, 5
    end
  end

  describe "shopping/0" do
    test "should create 1 store account" do
      assert Shopping.count_store_accounts() == 0

      Seed.shopping()

      assert Shopping.count_store_accounts() == 1
    end

    test "should create 40 products" do
      assert Shopping.count_products() == 0

      Seed.shopping()

      assert Shopping.count_products() == 40
    end
  end
end
