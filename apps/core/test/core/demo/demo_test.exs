defmodule Core.DemoTest do
  use Core.DataCase

  alias Core.Account.Identity
  alias Core.Demo
  alias Core.Demo.BankAccount

  describe "create_identity" do
    setup do
      %{params: params_for(:identity)}
    end

    test "create_identity/1 with valid params returns an identity", %{params: params} do
      {:ok, %Identity{}} = Demo.create_identity(params)
    end

    test "create_identity/1 with valid params broadcasts on \"account\"", %{params: params} do
      Core.subscribe("account:identity_created")

      {:ok, identity} = Demo.create_identity(params)

      assert_received({"account:identity_created", ^identity})
    end
  end

  describe "seed_bank_account" do
    setup do
      account = insert(:nem_account)
      bank_account = insert(:bank_account, nem_account: account)
      %{bank_account: bank_account}
    end

    test "seed_bank_account/1 with valid params returns a banking account seed", %{
      bank_account: bank_account
    } do
      assert {:ok, %BankAccount.Seed{}} = Demo.seed_bank_account(bank_account)
    end
  end

  describe "namespace/0" do
    setup [:preserve_namespace]

    test "it should read from config key :demo_namespace" do
      Application.put_env(:core, :demo_namespace, "foo123")

      assert "foo123" == Core.Demo.namespace()
    end
  end

  describe "mosaic_name/0" do
    setup [:preserve_mosaic_name]

    test "should read from config key :demo_mosaic_name" do
      Application.put_env(:core, :demo_mosaic_name, "abc123")

      assert "abc123" == Core.Demo.mosaic_name()
    end
  end

  describe "mosaic_supply/0" do
    setup [:preserve_mosaic_supply]

    test "should read from config key :demo_mosaic_supply" do
      Application.put_env(:core, :demo_mosaic_supply, 423_000)

      assert 423_000 == Core.Demo.mosaic_supply()
    end
  end

  def preserve_namespace(_context) do
    preserve_config(:demo_namespace)
    :ok
  end

  def preserve_mosaic_name(_context) do
    preserve_config(:demo_mosaic_name)
    :ok
  end

  def preserve_mosaic_supply(_context) do
    preserve_config(:demo_mosaic_supply)
    :ok
  end

  defp preserve_config(key) do
    value_before_test = Application.fetch_env!(:core, key)

    on_exit(make_ref(), fn ->
      Application.put_env(:core, key, value_before_test)
    end)
  end
end
