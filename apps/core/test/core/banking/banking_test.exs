defmodule Core.BankingTest do
  use Core.DataCase

  alias Core.Banking

  describe "bank_accounts" do
    alias Core.Banking.Account

    setup do
      identity = insert(:identity)
      params = params_for(:bank_account, id_number: identity.id)

      %{identity: identity, params: params}
    end

    @invalid_attrs %{first_name: nil, last_name: nil, id_number: nil}

    def account_fixture(%{first_name: first_name, last_name: last_name, id: id_number}) do
      params = %{first_name: first_name, last_name: last_name, id_number: id_number}
      {:ok, account} = Banking.create_account(params)

      account
    end

    test "list_accounts/0 returns all bank_accounts", %{identity: identity} do
      account = account_fixture(identity)
      assert Banking.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id", %{identity: identity} do
      account = account_fixture(identity)
      assert Banking.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account", %{params: params} do
      assert {:ok, %Account{}} = Banking.create_account(params)
    end

    test "create_account/1 with valid data creates an associated NemAccount", %{params: params} do
      {:ok, account} = Banking.create_account(params)

      account = account |> Repo.preload(:nem_account)
      assert %Core.NemAccount{} = account.nem_account
    end

    test "create_account/1 with valid data broadcasts on \"banking:account_created\"", %{
      params: params
    } do
      Core.subscribe("banking:account_created")

      {:ok, account} = Banking.create_account(params)

      assert_received({"banking:account_created", ^account})
    end

    test "create_account/1 with broadcast: false option does NOT broadcast", %{
      params: params
    } do
      Core.subscribe("banking:account_created")

      {:ok, account} = Banking.create_account(params, broadcast: false)

      refute_received({"banking:account_created", ^account})
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Banking.create_account(%{})
    end

    test "create_account/1 with invalid data broadcasts on \"banking:account_creation_failed\"" do
      Core.subscribe("banking:account_creation_failed")

      {:error, changeset} = Banking.create_account(@invalid_attrs)

      assert_received({"banking:account_creation_failed", ^changeset})
    end

    test "update_account/2 with valid data updates the account", %{identity: identity} do
      account = account_fixture(identity)
      new_identity = insert(:identity)
      assert {:ok, account} = Banking.update_account(account, %{identity: new_identity})
      assert %Account{} = account
    end

    test "update_account/2 with invalid data returns error changeset", %{identity: identity} do
      account = account_fixture(identity)
      assert {:error, %Ecto.Changeset{}} = Banking.update_account(account, @invalid_attrs)
      assert account == Banking.get_account!(account.id)
    end

    test "delete_account/1 deletes the account", %{identity: identity} do
      account = account_fixture(identity)
      assert {:ok, %Account{}} = Banking.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Banking.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset", %{identity: identity} do
      account = account_fixture(identity)
      assert %Ecto.Changeset{} = Banking.change_account(account)
    end
  end

  describe "bank transactions" do
    alias Core.Banking.Transaction

    setup do
      from = insert(:bank_account)
      to = insert(:bank_account)

      %{valid_attrs: params_for(:bank_transaction, from: from, to: to), from: from, to: to}
    end

    @update_attrs %{amount: 43, description: "some updated description"}
    @invalid_attrs %{amount: nil, description: nil}

    test "list_transactions/0 returns all transactions" do
      transaction = insert(:bank_transaction)
      assert Banking.list_transactions() == [transaction]
    end

    test "get_transaction!/1 returns the transaction with given id" do
      transaction = insert(:bank_transaction)

      assert Banking.get_transaction!(transaction.id) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction", %{valid_attrs: valid_attrs} do
      assert {:ok, %Transaction{} = transaction} = Banking.create_transaction(valid_attrs)
      assert transaction.amount == valid_attrs.amount
      assert transaction.description == valid_attrs.description
      assert transaction.status == "pending"
    end

    test "create_transaction/1 with valid data should broadcast on Core topic banking:transaction_created",
         %{valid_attrs: valid_attrs} do
      Core.subscribe("banking:transaction_created")

      assert {:ok, %Transaction{} = transaction} = Banking.create_transaction(valid_attrs)

      assert_receive({"banking:transaction_created", ^transaction})
    end

    test "create_transaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Banking.create_transaction(@invalid_attrs)
    end

    test "update_transaction/2 with valid data updates the transaction" do
      transaction = insert(:bank_transaction)
      assert {:ok, transaction} = Banking.update_transaction(transaction, @update_attrs)
      assert %Transaction{} = transaction
      assert transaction.amount == 43
      assert transaction.description == "some updated description"
    end

    test "update_transaction/2 with invalid data returns error changeset" do
      transaction = insert(:bank_transaction)
      assert {:error, %Ecto.Changeset{}} = Banking.update_transaction(transaction, @invalid_attrs)
      assert transaction == Banking.get_transaction!(transaction.id)
    end

    test "delete_transaction/1 deletes the transaction" do
      transaction = insert(:bank_transaction)
      assert {:ok, %Transaction{}} = Banking.delete_transaction(transaction)
      assert_raise Ecto.NoResultsError, fn -> Banking.get_transaction!(transaction.id) end
    end

    test "change_transaction/1 returns a transaction changeset" do
      transaction = insert(:bank_transaction)
      assert %Ecto.Changeset{} = Banking.change_transaction(transaction)
    end

    test "transfer/3 with valid data creates a transaction", %{from: from, to: to} do
      assert {:ok, %Transaction{} = transaction} =
               Banking.transfer(from.id_number, to.id_number, "20")

      assert transaction.to_id == to.id_number
      assert transaction.from_id == from.id_number
      assert transaction.amount == 2000
    end

    test "transfer/3 with valid data broadcasts on Core topic banking:transaction_created", %{
      from: from,
      to: to
    } do
      Core.subscribe("banking:transaction_created")
      {:ok, transaction} = Banking.transfer(from.id_number, to.id_number, "25")

      assert_receive({"banking:transaction_created", ^transaction})
    end
  end
end
