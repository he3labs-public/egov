ExUnit.start()
ExUnit.configure(exclude: :external)

Faker.start()
{:ok, _} = Application.ensure_all_started(:ex_machina)

# Ecto.Adapters.SQL.Sandbox.mode(Core.Repo, :manual)
