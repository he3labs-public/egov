# EctoHelper

Prettify Ecto errors
Code from GitHub Gist  
https://gist.github.com/sergio1990/537fcf958752ae223baa4a782ee09109

Code Author  
https://github.com/sergio1990

Thanks to Sergio Gernyak for sharing this very simple but helpful bit of code.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ecto_helper` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ecto_helper, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/ecto_helper](https://hexdocs.pm/ecto_helper).
