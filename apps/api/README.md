# Setup (not needed if setup followed from `../..`)

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can make API requests at [`localhost:4100`](http://localhost:4100).

The WebSocket endpoint is available at [`localhost:4100/socket`](ws://localhost:4100/socket). If you are using a Phoenix Framework compatible WebSocket library you may need to drop the transport from the path: [`localhost:4100/socket`](ws://localhost:4100/socket).


## API Endpoints

### /authorize

A quick-and-dirty endpoint for authorizing users with a pin code.

Pin codes are 4 characters, auto-generated upon creating an Identity, and never include the numbers 0 or 1.

Go to the Identities page in the admin panel to lookup pin codes.

Example Request:

```
POST /authorize

pin=TW12
```

Successful Response (200):

```json
{
  "data": {
    "id":"c34c98a6-31d6-4666-badd-1c192d9a05f3",
    "first_name":"Clifford",
    "last_name":"Mitchell",
    "birthdate":"1961-05-27",
    "blood_degree":"1/4",
    "email":"clifford_mitchell@example.org",
    "phone":"(406) 555-1212",
    "mailing_address":{"state":"MT","postcode":"59105","city":"Billings","address2":"Suite #1","address1":"654 Main St."},
    "api_token":"f010f7d15a98491ea75ad120e44b7a50"  
  }
}
```

Failure Response (200):

```json
{
  "error": "Authorization Failed"
}
```

## WebSocket Connections

WebSocket connections provide real-time functionality between client and server. Before making a WebSocket connection use the /authorize endpoint to obtain a user token. See the Authorize End point above for more details.

#### Authentication

Connections to the server require user authentication. To authenticate, a valid user token must be included in the connection parameters.

JavaScript Example:
```javascript
let socket = new Socket("/socket", {params: {userToken: "123"}})
socket.connect()
```

C# Example:
```cs
var host = "ws://localhost:4100/socket";
var urlparams = new Dictionary<string, string>()
                              {
                                {"userToken", "123"}
                              };
var socket = new PhoenixSocket.Socket(host, urlparams: url_params);

socket.Connect();
```


#### Channels

Channels provide bi-direction communication between the server and clients. After a connection is made clients must join a channel to send or receive events.

When joining a channel you must know which topic you want to listen to. Topic names are in the form `topic:subtopic`.

Available topics are listed in the sections below.


JavaScript Example:
```javascript
let channel = socket.channel("voting:lobby", {})
channel.on("new_msg", msg => console.log("Got message: ", msg) )

channel.join()
  .receive("ok", ({messages}) => console.log("catching up: ", messages) )
  .receive("error", ({reason}) => console.log("failed join: ", reason) )
  .receive("timeout", () => console.log("Networking issue. Still waiting...") )
```

C# Example:
```cs
var channel = socket.Channel("rooms:123");
channel.On("new_msg", msg =>
  {
    Console.WriteLine("Got message: " + JsonConvert.SerializeObject(msg));
  });

channel.Join()
  .Receive("ok", messages => {
    Console.WriteLine("catching up: " + JsonConvert.SerializeObject(messages));
  })
  .Receive("error", reason => {
    Console.WriteLine("failed join: " + JsonConvert.SerializeObject(reason));
  })
  .Receive("timeout", _ => {
    Console.WriteLine("Networking issue. Still waiting...");
  });
```

#### Voting

Join "voting:lobby" to receive general election related notifications.

On successful join, the server replies with a list of current elections. Election info includes embedded "user_info", with details about the current user's voting participation.

Example reply:

```json
{
  "representatives":[{
      "id":"e36ac2c8-a785-4348-8a25-164d50c33f01",
      "positions": ["Chairman","Vice-Chairman","Secretary","Vice-Secretary"],
      "candidates":[
        {"name":"Sally","id":"14155008-5260-4d41-a11f-77afa40c7b50","position":"Chairman"},
        {"name":"Cedric","id":"f99f2fc0-4d90-482a-a013-748a92edbe04","position":"Chairman"},
        {"name":"Franklin","id":"f30deb43-f4dc-4a13-9273-b9e58fe39834","position":"Chairman"},
        {"name":"Bo","id":"27af6252-95bf-423f-8432-ac40947304e6","position":"Chairman"},
        {"name":"Flying Shield","id":"3b91688e-4e6b-49f7-a054-2339f2de7e07","position":"Vice-Chairman"},
        {"name":"Not Afraid","id":"5151ccb0-5136-4bf4-892d-7adcf3eba1d9","position":"Vice-Chairman"},
        {"name":"Strong Bear","id":"aeb691b7-06c8-4946-8390-bd0a3b3900d4","position":"Vice-Chairman"},
        {"name":"Walking Tree","id":"2b8bbacb-b29d-4a02-95a2-f9ac4031512c","position":"Vice-Chairman"},
        {"name":"Looking Crow","id":"66f91c41-17b0-43dc-b2dc-07dfb1e9ca9c","position":"Vice-Secretary"},
        {"name":"Bent Foot","id":"ff10979d-0e86-4a29-acff-065d02f582cc","position":"Vice-Secretary"},
        {"name":"Open Heart","id":"f90621ff-69e4-439d-addf-3a4d977fcbdb","position":"Secretary"},
        {"name":"Laughing Bird","id":"431bff64-4a4f-4a94-b757-2fd8ba456b4f","position":"Secretary"},
        {"name":"Lone Bird","id":"5bea8a01-d3fd-47be-9ba1-88557cd54358","position":"Secretary"}
      ],
      "user_info":{"voted": true,"eligible": true},
      "start_date":"2018-07-23T00:07:00.000000",
      "end_date":"2018-07-23T00:07:00.000000",
      "open": false,
      "inserted_at":"2018-07-23T23:24:01.184925",
      "updated_at":"2018-07-23T23:24:01.184940"
    }],

  "referendas": [{
      "id":"120f10d9-5b6c-46c1-8223-ffec43e0698d",
      "user_info":{"voted":false,"eligible":true},
      "measures":[
          {
            "title":"Interactive Diamonds & Gemstones",
            "overview":"Recusandae deserunt sint eum? Provident debitis saepe temporibus esse tempore maxime commodi rerum. Cumque est id dignissimos sint officiis fugiat possimus. Esse qui minus recusandae qui non illo!\n\nAut et quia veritatis. Est omnis voluptatem dolore. Voluptate id eius animi distinctio veniam iusto sint. Aut amet et corrupti sit aliquid dolor. Eius enim doloribus aut sit.",
            "name":"Proposition 39",
            "id":"047e1de2-8a3c-4548-8318-bb9eb553f3d7"
          },
          {
            "title":"4th generation Commodity Chemicals",
            "overview":"Sint dolorum laboriosam dolorem minus qui qui consequatur illo aut? Reprehenderit aut aliquam sed reiciendis saepe incidunt fuga. Illum eum nobis sit voluptates illum accusantium quam ad nam. Expedita qui enim molestiae?",
            "name":"Proposition 81",
            "id":"d5cbeb8a-ad12-4e73-b6dd-eb2847eaa67a"
          },
          {
            "title":"Actuating Business Training & Employment Agencies",
            "overview":"Numquam repellat delectus excepturi eveniet nihil rem ipsa. Explicabo ab veniam perferendis dolores voluptates neque quo facere? Ad cumque repudiandae aliquid! Voluptate odio rerum nam quo occaecati voluptates nesciunt? At ad sapiente ratione quo et.\n\nEveniet animi repellendus nihil. Non molestias et impedit vel. Architecto eos velit rerum corrupti qui officiis est eaque officiis? Velit consequatur modi magnam? Necessitatibus dignissimos et necessitatibus.",
            "name":"Proposition 51",
            "id":"07d571c3-d2a0-43b4-90d1-df6200d5745f"
          },
          {
            "title":"Attitude-oriented Industrial & Office REITs","overview":"Animi in quibusdam a exercitationem officia eum. Error necessitatibus nisi perspiciatis accusantium quas corporis incidunt aperiam? Sint nobis suscipit enim vero non exercitationem mollitia.\n\nMagnam veniam asperiores molestiae cupiditate numquam ut voluptatem quae laboriosam! Molestiae nihil deserunt magnam beatae ratione omnis sapiente aliquam repellat! Et laudantium sunt occaecati molestiae est amet?",
            "name":"Proposition 114",
            "id":"92032fbd-980d-45fe-802c-549ec8d5490a"
          },
          {
            "title":"Logistical Nonferrous Metals","overview":"Reiciendis accusamus nostrum quae eos minima aut corrupti repudiandae enim. Accusantium assumenda molestiae fugiat dolor! Consequatur laborum iusto in eaque mollitia. Unde quasi corporis dicta possimus provident voluptas. Fuga quod quisquam aut sit.\n\nConsectetur eum eos ex quia doloribus possimus ut. Odio et dolores quas veritatis alias et molestiae! Iste velit voluptatibus quia qui qui omnis quaerat unde nam. Exercitationem enim ducimus totam. Et sint earum aut magnam autem itaque nemo cum!",
            "name":"Proposition 73",
            "id":"c9c1e120-4409-44da-a961-083945c74a86"
          }
      ]
  }]
}
```


###### Request a Ballot

A ballot must be requested in order for a voter to participate in an open election or referenda. If the voter is eligible and has not already participated, they will be sent a participation chit. A participation chit is cast as part of the voter's ballot submission, and is required in order for the vote to be counted.

A blockchain transaction must confirm before the chit is ready to be used. This could take as long as 30 seconds depending on when the request is made.

The voter must wait until the chit is received before casting their vote. In order to minimize waiting, the ballot request can be sent as soon as the voter opens the election or referenda.

Example ballot requests:

```javascript
var timeout = 10000

// Referendum
channel.push("request_ballot", {election_id: id}, timeout)
       .receive("ok", (msg) => console.log("election ballot request sent", msg) )
       .receive("error", (reasons) => console.log("election ballot request failed", reasons) )
       .receive("timeout", () => console.log("Networking issue...") )

// Election
channel.push("request_ballot", {referenda_id: id}, timeout)
       .receive("ok", (msg) => console.log("referenda ballot request sent", msg) )
       .receive("error", (reasons) => console.log("referenda ballot request failed", reasons) )
       .receive("timeout", () => console.log("Networking issue...") )
```


###### Ballot Ready

When a ballot has been requested, an `"election_ballot_ready"` or `"referenda_ballot_ready"` message is broadcast to notify clients when the chit transaction has been confirmed.

Example `"election_ballot_ready"` Payload:

```json
{"election_id": "e36ac2c8-a785-4348-8a25-164d50c33f01"}
```

Example `"referenda_ballot_ready"` Payload:

```json
{"referenda_id": "120f10d9-5b6c-46c1-8223-ffec43e0698d"}
```


Example Javascript to handle the message:

```javascript
channel.on("election_ballot_ready", msg => {
  console.log("ballot ready for election: ", msg.election_id)
})
```

###### Submitting a Ballot

When a ballot has been filled out and is ready to submit, send a `"submit_ballot"` request with the following payload:

Example Payload when submitting for an Election:

```json
{
  "election_id": "a8786dc7-3506-464d-a0c8-2db9c4288587",
  "candidates": [
    {
      "id": "4d54a181-20ba-4348-87c5-9860595a463e",
      "position": "Chairman",
      "name": "Abby Larkin"
    },
    {
      "id": "8c2f6b89-373e-4f76-915f-6c7bb0c328d1",
      "position": "Vice-Chairman",
      "name": "Darrick Steuber"
    },
    {
      "id": "225bb17a-adbb-4056-a802-bfd70cb90ecd",
      "position": "Secretary",
      "name": "Carmelo Luettgen"
    },
    {
      "id": "af8188d6-7f96-49a0-9814-d1becd6f0e87",
      "position": "Vice-Secretary",
      "name": "Rogers Larkin"
    }
  ]
}
```

Example Payload when submitting for a Referendum:

```json
{
  "referenda_id": "6c92c2c3-d3f5-440a-bc2f-2c7d5110d591",
  "referenda": [
    {
      "id": "15a23777-a781-4bbb-8aa3-130c2247bf1b",
      "vote": 0
    },
    {
      "id": "2c23409a-adc4-4607-9488-6bfeced624c5",
      "vote": 1
    },
    {
      "id": "cd0217ea-ee2c-4742-a32e-0825de69b13d",
      "vote": -1
    },
    {
      "id": "eb8ab440-bcd3-40fe-8205-df704afd3c74",
      "vote": -1
    }
  ]
}
```

The server will reply `"ok"` and acknowledge the submission, or `"error"` if there was a problem.

When the ballot is fully recorded, an `"election_ballot_confirmed"` or `"referenda_ballot_confirmed"` message is sent.

Example `"election_ballot_confirmed"` Payload:

```json
{"election_id": "e36ac2c8-a785-4348-8a25-164d50c33f01", "confirmation_code": "IU27"}
```

Example `"referenda_ballot_confirmed"` Payload:

```json
{"referenda_id": "120f10d9-5b6c-46c1-8223-ffec43e0698d", "confirmation_code": "ST99"}
```

#### Banking

Join "banking:lobby" to send and receive banking related messages.

On successful join, the server replies with information about the user's bank account--balance, and a list of recent transactions.

Example Join:

```javascript
let bankingChannel = socket.channel("banking:lobby", {})

bankingChannel.join()
  .receive("ok", reply => { console.log("Joined successfully", reply) })
  .receive("error", reply => { console.log("Unable to join", reply) })
  .receive("timeout", () => console.log("Networking issue...") )
```

Example Reply:

```json
{
  "balance": "486.50",
  "transactions": [
    {
      "id": "4cb798c3-530b-4a6f-92eb-77bba1c48a62",
      "description": "Giovanni's Incredible Apizza",
      "amount": "-28.50",
      "timestamp": "2018-07-23T23:24:01.184925",
      "status": "completed"
    },
    {
      "id": "02ff7cb4-4c13-48c8-8d55-924125d87593",
      "description": "Deposit",
      "amount": "515.0",
      "timestamp": "2018-07-21T23:24:01.184925",
      "status": "completed"
    }
  ]
}
```


###### Lookup a contact

A banking contact can be looked up by scanning their id QR code. This confirms they have a bank account before transferring money.

Example lookup request:

```javascript
let lookup = {id: "adec0387-3c79-40c4-93ca-8407c62a0c79"}

bankingChannel.push("lookup", lookup)
  .receive("ok", (msg) => console.log("Contact Found", msg) )
  .receive("error", (reasons) => console.log("Not Found", reasons) )
  .receive("timeout", () => console.log("Networking issue...") )

```

The server will respond `"ok"` if the  is found, and `"error"` if nothing was found, or there was a problem.

Example payload on success:

```json
{
  "id": "adec0387-3c79-40c4-93ca-8407c62a0c79",
  "first_name": "Paris",
  "last_name": "Lakin"
}
```

Example payload on error :

```json
{"error": {"reason": "Not Found"}}
```


###### Transfers

Pay a bill, checkout while shopping, or to send some money to a friend.

Example request to transfer money:

```javascript
// "to" should be a person's id number
let transfer = {amount: "20.00", to: "537b2a7a-5028-4f3c-95d9-11fb204f3101"}

bankingChannel.push("transfer", transfer)
  .receive("ok", (tx_info) => console.log("Transfer Sent", tx_info) )
  .receive("error", (reasons) => console.log("Transfer Failed", reasons) )
  .receive("timeout", () => console.log("Networking issue...") )
```

The server will respond `"ok"`, and reply with transaction info if the transfer request was successful, or `"error"` if there was a problem.

Example payload on success:

```json
{
  "id": "941c5d77-44ac-4479-8f7f-2d977d9fa731",
  "description": "Transfer to Kim Pacocha",
  "amount": "20.00",
  "timestamp": "2018-07-23T23:24:01.184925",
  "status": "pending"
}
```

Example payload on error:

```json
{"error": "The transfer could not be completed." }
```

###### Incoming Transactions

Recipients receive a notification when an incoming transfer is made.

Listening for Incoming Transfers:

```javascript
bankingChannel.on("incoming_transfer", (tx_info) => {
  console.log("Incoming Transfer: ", tx_info)
})
```

Example payload:

```json
{
  "id": "941c5d77-44ac-4479-8f7f-2d977d9fa731",
  "description": "Transfer from Paris Lakin",
  "amount": "20.00",
  "timestamp": "2018-07-23T23:24:01.184925",
  "status": "pending"
}
```

###### Transaction Status Changes

When a transaction status changes the sender and recipient of the transaction will receive a notification. The full details of the transaction are provided in the message.

Statuses:

* failed
* completed

Listening for Status Changes:

```javascript
bankingChannel.on("transaction_status", (tx_info) => {
  console.log("Transaction Status Change:", tx_info)
})
```

Example payload:

```json
{
  "id": "63d9a667-702f-46c9-b341-45f5bad85fcf",
  "amount": "5.29",
  "description": "Transfer to Elizabeth Vandervort",
  "timestamp": "2018-08-07T08:57:44.939581",
  "status": "completed"
}
```

###### Account State Changes

A full transaction history and balance are sent as an `account_state` event when any transaction is created, failed, or completed.

Listening for State Changes:

```javascript
bankingChannel.on("account_state", (state) => {
  console.log("Account State Change:", state)
})
```

Example payload:

```json
{
  "balance": "486.50",
  "transactions": [
    {
      "id": "941c5d77-44ac-4479-8f7f-2d977d9fa731",
      "description": "Transfer from Paris Lakin",
      "amount": "20.00",
      "timestamp": "2018-07-23T23:24:01.184925",
      "status": "pending"
    },
    {
      "id": "4cb798c3-530b-4a6f-92eb-77bba1c48a62",
      "description": "Giovanni's Incredible Apizza",
      "amount": "-28.50",
      "timestamp": "2018-07-23T23:24:01.184925",
      "status": "completed"
    },
    {
      "id": "02ff7cb4-4c13-48c8-8d55-924125d87593",
      "description": "Deposit",
      "amount": "515.0",
      "timestamp": "2018-07-21T23:24:01.184925",
      "status": "completed"
    }
  ]
}
```

#### Shopping

Join "shopping:lobby" to send and receive shopping related messages.

On a successful join, the server replies with information about the current check--it will always start empty.

Example Join:

```javascript
let shoppingChannel = socket.channel("shopping:lobby", {})

shoppingchannel.join()
  .receive("ok", empty_check => { console.log("Joined successfully", reply) })
  .receive("error", reply => { console.log("Unable to join", reply) })
  .receive("timeout", () => console.log("Networking issue...") )
```

Example reply:

```json
{
  "customer_id": null,
  "customer_name": null,
  "status": "open",
  "total": 0,
  "items": []
}
```

Shopping involves two distinct roles:

1. Clerk scanning products
2. Customer paying at a kiosk

Both Clerk and Customer should listen for `"check_updated"` messages to receive updates about products being added, and the customer being identified.

Example `"check_updated"` message:

```json
{
  "customer_id": null,
  "customer_name": null,
  "status": "open",
  "total": 275,
  "items": [
    {
      "id": "defbb36a-e71b-43eb-8e04-b338ab109b6a",
      "name": "Maharaj",
      "price": 275,
      "timestamp": "2018-08-09T22:04:25.545142"
    }
  ]
}
```

###### Clerk: Scanning a product

When a clerk scans a product, send an `"product_scanned"` message.

On success the server will respond `"ok"` and no reply. All clients will receive a `"check_updated"` message with the updated check info.

If there was a problem the server will respond `"error"` and reply with the reason.

Example request:

```javascript
let product_scanned = {product_id: "00000000-0000-0000-0000-000000000001"}

shoppingChannel.push("product_scanned", product_scanned)
  .receive("ok", (_nothing) => console.log("Product scan successful") )
  .receive("error", (reason) => console.log("Product scan failed", reason) )
  .receive("timeout", () => console.log("Networking issue...") )
```

###### Clerk: Resetting the check

When a clerk wants to reset / clear the current check and start over, send a `"reset_check"` message. On success the server will respond `"ok"` and reply with a fresh empty check. All other clients will receive a `"check_updated"` message with a new/empty check.

Example request:

```javascript
shoppingChannel.push("reset_check", {})
  .receive("ok", (empty_check) => console.log("Check reset", empty_check) )
  .receive("error", (reasons) => console.log("Problem resetting check", reasons) )
  .receive("timeout", () => console.log("Networking issue...") )
```
###### Clerk: Closing the check

When a clerk is done scanning and is ready for the customer to pay, send a `"close_check"` message. On success the server will respond `"ok"` with no reply. The customer kiosk will receive a `"ready_for_payment"` message.

Example request:

```javascript
shoppingChannel.push("close_check", {})
  .receive("ok", (empty_check) => console.log("Check reset", empty_check) )
  .receive("error", (reasons) => console.log("Problem resetting check", reasons) )
  .receive("timeout", () => console.log("Networking issue...") )
```
###### Customer: Scanning their id

When a customer scans their id to identify themselves, send a `"customer_scanned"` message. On success the server will respond with `"ok"` and no reply.

The clerk/scanner will be sent a `"check_updated"` message with the customer id and name added to the current check.

```javascript
let customer_id = {customer_id: "adec0387-3c79-40c4-93ca-8407c62a0c79"}

shoppingChannel.push("customer_scanned", customer_id)
  .receive("ok", (_nothing) => console.log("Customer scan successful", current_check) )
  .receive("error", (reason) => console.log("Customer scan failed", reason) )
  .receive("timeout", () => console.log("Networking issue...") )
```

###### Customer: Ready for payment

When a customer's check is closed, they will receive a `"ready_for_payment"` message with a total amount.

Example payload:

```json
{
  "total": "22.75"
}
```

###### Customer: Accept and Pay

When a customer has confirmed the total and is ready to pay, send an `"accept_and_pay"` message.

On success the server will respond `"ok"` and create a transaction to bill the person. When the transaction is confirmed, the Kiosk and Clerk/Scanner will receive a `"payment_complete"` message with a new / empty check.


###### Customer: Decline Payment

When a customer declines the request for payment, send a `"decline_payment"` message. On success the server will respond `"ok"` and reset / empty the check. The Clerk/Scanner will be sent  a `"payment_declined"` with a new / empty check.

