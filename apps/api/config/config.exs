# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :api,
  namespace: Api

# Configures the endpoint
config :api, Api.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "p4Y85ZKSv345y6WzVtOQE7BOXy2LzFGxsGMiOb1FjPbBYuJdaX6p30mjVWM+ibRb",
  render_errors: [view: Api.ErrorView, accepts: ~w(json)],
  pubsub: [name: Core.PubSub]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :api, :generators, context_app: :core

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
