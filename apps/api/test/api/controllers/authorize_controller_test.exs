defmodule Api.AuthorizeControllerTest do
  use Api.ConnCase

  alias Core.Account

  def fixture(:identity) do
    {:ok, identity} = Account.create_identity(params_for(:identity))
    identity
  end

  @invalid_attrs %{pin: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create" do
    setup do
      %{identity: fixture(:identity)}
    end

    test "renders identity when code is valid", %{conn: conn, identity: identity} do
      valid_attrs = %{pin: identity.pin}
      conn = post(conn, authorize_path(conn, :create), valid_attrs)

      rendered_identity =
        Api.IdentityView.render("identity.json", %{identity: identity})
        |> Poison.encode!()
        |> Poison.decode!()

      assert json_response(conn, 200)["data"] == rendered_identity
    end

    test "renders errors when code is invalid", %{conn: conn} do
      conn = post(conn, authorize_path(conn, :create), @invalid_attrs)

      assert json_response(conn, 200)["error"] == "Authorization Failed"
    end
  end
end
