defmodule Api.IdentityControllerTest do
  use Api.ConnCase

  alias Core.Account

  def fixture(:identity) do
    {:ok, identity} = Account.create_identity(params_for(:identity))
    identity |> Core.Repo.preload(:api_token)
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup do
      %{identities: [fixture(:identity), fixture(:identity)]}
    end

    test "lists all identities", %{conn: conn, identities: identities} do
      conn = get(conn, identity_path(conn, :index))

      expected =
        Enum.map(identities, fn identity ->
          %{
            "id" => identity.id,
            "birthdate" => identity.birthdate,
            "blood_degree" => identity.blood_degree,
            "email" => identity.email,
            "first_name" => identity.first_name,
            "last_name" => identity.last_name,
            "phone" => identity.phone,
            "mailing_address" => identity.mailing_address,
            "pin" => identity.pin,
            "api_token" => identity.api_token.token
          }
          |> Poison.encode!()
          |> Poison.decode!()
        end)

      assert json_response(conn, 200)["data"] == expected
    end
  end
end
