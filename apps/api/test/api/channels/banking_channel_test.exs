defmodule Api.BankingChannelTest do
  use Api.ChannelCase

  alias Api.BankingChannel
  alias Core.Repo
  alias Core.Banking

  setup do
    api_token = insert(:api_token)
    user = api_token.identity
    account = insert(:bank_account, id_number: user.id)

    transactions =
      0..1
      |> Enum.map(fn index ->
        insert(:bank_transaction,
          to: account,
          status: "completed",
          inserted_at: Core.naive_now(-index)
        )
      end)

    balance =
      transactions
      |> Banking.calculate_balance(account.id_number)
      |> Core.Banking.Amount.integer_to_string()

    {:ok, reply, socket} =
      socket("id", %{user_token: api_token.token})
      |> subscribe_and_join(BankingChannel, "banking:lobby")

    %{
      account: account,
      transactions: transactions,
      balance: balance,
      socket: socket,
      join_reply: reply
    }
  end

  describe "join" do
    test "should reply with a balance", %{join_reply: join_reply, balance: balance} do
      assert %{balance: ^balance} = join_reply
    end

    test "should reply with a list of transaction", %{
      join_reply: join_reply,
      account: account,
      transactions: transactions
    } do
      transactions =
        transactions
        |> Enum.map(
          &Api.TransactionView.render("transaction.json", %{
            transaction: &1,
            display_to: account.id_number
          })
        )

      assert %{transactions: ^transactions} = join_reply
    end

    test "should subscribe to banking:transaction_created messages for the current user", %{
      account: account
    } do
      transaction =
        insert(:bank_transaction, amount: 100, to: account)
        |> Repo.preload([:from, :to])

      Core.broadcast("banking:transaction_created", transaction)

      message =
        Api.TransactionView.render("transaction.json", %{
          transaction: transaction,
          display_to: account.id_number
        })

      assert_push("account_state", _)
      assert_push("incoming_transfer", ^message)
    end

    test "should not subscribe to banking:transaction_created messages for other users" do
      transaction = insert(:bank_transaction)

      Core.broadcast("banking:transaction_created", transaction)

      refute_push("incoming_transfer", %{})
    end

    test "should subscribe to banking:transaction_failed messages for the current user", %{
      account: account
    } do
      transaction =
        insert(:bank_transaction, amount: 100, to: account)
        |> Repo.preload([:from, :to])

      Core.broadcast("banking:transaction_failed", transaction)

      message =
        Api.TransactionView.render("transaction.json", %{
          transaction: transaction,
          display_to: account.id_number
        })

      assert_push("transaction_status", ^message)
      assert_push("account_state", _)
    end

    test "should subscribe to banking:transaction_completed messages for the current user", %{
      account: account
    } do
      transaction =
        insert(:bank_transaction, amount: 100, to: account)
        |> Repo.preload([:from, :to])

      Core.broadcast("banking:transaction_completed", transaction)

      message =
        Api.TransactionView.render("transaction.json", %{
          transaction: transaction,
          display_to: account.id_number
        })

      assert_push("transaction_status", ^message)
      assert_push("account_state", _)
    end
  end

  test "ping replies with status ok", %{socket: socket} do
    ref = push(socket, "ping", %{"hello" => "there"})
    assert_reply(ref, :ok, %{"hello" => "there"})
  end

  describe "lookup with valid attrs" do
    test "should reply with status ok", %{socket: socket, account: account} do
      ref = push(socket, "lookup", %{"id" => account.id_number})

      expected_reply = %{
        id: account.id_number,
        first_name: account.first_name,
        last_name: account.last_name
      }

      assert_reply(ref, :ok, ^expected_reply)
    end
  end

  describe "lookup with unknown id number" do
    test "should reply with status error", %{socket: socket} do
      unknown_id_number = Ecto.UUID.generate()
      ref = push(socket, "lookup", %{"id" => unknown_id_number})

      expected_reply = %{reason: "Not Found"}

      assert_reply(ref, :error, ^expected_reply)
    end
  end

  describe "transfer with valid attrs" do
    setup do
      recipient = insert(:bank_account)

      %{
        recipient: recipient,
        description: "Transfer to #{recipient.first_name} #{recipient.last_name}",
        valid_attrs: %{amount: "1.00", to: recipient.id_number}
      }
    end

    test "should reply with status ok", %{
      socket: socket,
      description: description,
      valid_attrs: %{amount: amount, to: to}
    } do
      ref = push(socket, "transfer", %{amount: amount, to: to})
      relative_amount = "-#{amount}"

      assert_reply(ref, :ok, %{
        id: _,
        description: ^description,
        amount: ^relative_amount,
        timestamp: _
      })
    end
  end
end
