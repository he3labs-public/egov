defmodule Api.ShoppingChannelTest do
  use Api.ChannelCase

  alias Api.ShoppingChannel
  alias Core.Shopping
  alias Core.Banking
  alias Core.Account.Identity

  setup do
    {:ok, _, socket} =
      socket("user_id", %{some: :assign})
      |> subscribe_and_join(ShoppingChannel, "shopping:lobby")

    {:ok, socket: socket}
  end

  test "ping replies with status ok", %{socket: socket} do
    ref = push(socket, "ping", %{"hello" => "there"})
    assert_reply(ref, :ok, %{"hello" => "there"})
  end

  describe "product_scanned with valid attrs" do
    setup do
      product = insert(:product)
      {:ok, item} = Shopping.CheckItem.from_product(product)

      %{product: product, item: item, item_id: item.id, item_name: item.name}
    end

    test "should reply with status ok", %{
      socket: socket,
      product: product
    } do
      ref = push(socket, "product_scanned", %{product_id: product.id})

      assert_reply(ref, :ok, %{})
    end

    test "should broadcast on Core topic shopping:add_product", %{
      socket: socket,
      product: product
    } do
      Core.subscribe("shopping:add_product")

      push(socket, "product_scanned", %{product_id: product.id})

      assert_receive({"shopping:add_product", ^product})
    end

    test "should subscribe to Core topic shopping:add_product and update the check", %{
      product: product,
      item_id: item_id,
      item_name: item_name
    } do
      Core.broadcast("shopping:add_product", product)

      total = product.price

      assert_push("check_updated", %Shopping.Check{
        customer_id: nil,
        items: [%{id: ^item_id, name: ^item_name, timestamp: _t}],
        total: ^total
      })
    end
  end

  describe "product_scanned with invalid attrs" do
    test "should reply with status error", %{socket: socket} do
      ref = push(socket, "product_scanned", %{product_id: "d0d8ab9c-67cf-47e7-94ec-d348129b3efb"})

      assert_reply(ref, :error, %{reason: "Product not found"})
    end
  end

  describe "reset_check" do
    setup do
      product = insert(:product)
      {:ok, item} = Shopping.CheckItem.from_product(product)

      %{product: product, item: item, item_id: item.id, item_name: item.name}
    end

    test "should reply ok", %{
      socket: socket,
      product: product
    } do
      ref = push(socket, "product_scanned", %{product_id: product.id})
      assert_reply(ref, :ok, %{})

      ref = push(socket, "reset_check", %{})
      assert_reply(ref, :ok, %{customer_id: nil, items: [], total: 0})
    end

    test "should subscribe to Core topic shopping:check_reset and reset the check", %{
      socket: socket,
      product: product
    } do
      ref = push(socket, "product_scanned", %{product_id: product.id})
      assert_reply(ref, :ok, %{})

      Core.broadcast("shopping:check_reset", %{})

      assert_push("check_updated", %{customer_id: nil, items: [], total: 0})
    end

    test "should broadcast on Core topic shopping:check_reset", %{socket: socket} do
      test = self()

      Task.async(fn ->
        Core.subscribe("shopping:check_reset")

        send(test, "ready")

        receive do
          msg -> send(test, msg)
        after
          500 -> raise "Failed to receive shopping:check_reset"
        end
      end)

      receive do
        "ready" ->
          ref = push(socket, "reset_check", %{})
          assert_reply(ref, :ok, %{customer_id: nil, items: [], total: 0})

          assert_receive({"shopping:check_reset", %{}}, 1000)
      after
        2000 -> raise "Task failed to start up"
      end
    end
  end

  describe "close_check" do
    setup %{socket: socket} do
      product = insert(:product)
      push(socket, "product_scanned", %{product_id: product.id})

      %{total: product.price}
    end

    test "should reply ok", %{socket: socket} do
      ref = push(socket, "close_check", %{})

      assert_reply(ref, :ok, %{})
    end

    test "should subscribe to Core topic shopping:check_closed and push ready_for_payment to the kiosk",
         %{socket: socket, total: total} do
      ref = push(socket, "close_check", %{})
      assert_reply(ref, :ok, %{})

      Core.broadcast("shopping:check_closed", %{})

      assert_push("ready_for_payment", %{total: ^total})
    end

    test "should broadcast on Core topic shopping:check_closed", %{socket: socket} do
      test = self()

      Task.async(fn ->
        Core.subscribe("shopping:check_closed")

        send(test, "ready")

        receive do
          msg -> send(test, msg)
        after
          500 -> raise "Failed to receive shopping:check_closed"
        end
      end)

      receive do
        "ready" ->
          ref = push(socket, "close_check", %{})
          assert_reply(ref, :ok, %{})

          assert_receive({"shopping:check_closed", %{}}, 1000)
      after
        2000 -> raise "Task failed to start up"
      end
    end
  end

  describe "decline_payment" do
    setup do
      customer = insert(:identity)
      customer_id = customer.id
      customer_name = Identity.full_name(customer)
      insert(:bank_account, id_number: customer.id)
      product = insert(:product)

      Core.broadcast("shopping:add_product", product)
      Core.broadcast("shopping:add_customer", customer)
      Core.broadcast("shopping:check_closed", %{})

      %{
        customer: customer,
        customer_id: customer_id,
        customer_name: customer_name,
        total: product.price
      }
    end

    test "should reply ok", %{socket: socket} do
      ref = push(socket, "decline_payment", %{})

      assert_reply(ref, :ok, %{})
    end

    test "should broadcast on Core topic shopping:payment_declined", %{socket: socket} do
      Core.subscribe("shopping:payment_declined")

      push(socket, "decline_payment", %{})

      assert_receive({"shopping:payment_declined", %{}})
    end

    test "should subscribe to Core topic shopping:payment_declined and push payment_declined and reset the check" do
      Core.subscribe("shopping:check_reset")
      Core.broadcast("shopping:payment_declined", %{})

      assert_push("payment_declined", %{})
      assert_receive({"shopping:check_reset", %{}})
    end
  end

  describe "customer_scanned" do
    setup do
      customer = insert(:identity)
      insert(:bank_account, id_number: customer.id)

      %{customer: customer, customer_id: customer.id, customer_name: Identity.full_name(customer)}
    end

    test "should reply ok", %{
      socket: socket,
      customer: customer
    } do
      customer_id = customer.id
      ref = push(socket, "customer_scanned", %{customer_id: customer_id})

      assert_reply(ref, :ok, %{})
    end

    test "should broadcast on Core topic shopping:add_customer", %{
      socket: socket,
      customer: customer
    } do
      Core.subscribe("shopping:add_customer")

      push(socket, "customer_scanned", %{customer_id: customer.id})

      assert_receive({"shopping:add_customer", ^customer})
    end

    test "should subscribe to Core topic shopping:add_customer and update the check", %{
      customer: customer,
      customer_id: customer_id,
      customer_name: customer_name
    } do
      Core.broadcast("shopping:add_customer", customer)

      assert_push("check_updated", %Shopping.Check{
        customer_id: ^customer_id,
        customer_name: ^customer_name
      })
    end
  end

  describe "accept_and_pay" do
    setup do
      customer = insert(:identity)
      customer_id = customer.id
      customer_name = Identity.full_name(customer)
      insert(:bank_account, id_number: customer.id)
      product = insert(:product)

      Core.broadcast("shopping:add_product", product)
      Core.broadcast("shopping:add_customer", customer)

      %{
        customer: customer,
        customer_id: customer_id,
        customer_name: customer_name,
        total: product.price
      }
    end

    test "should reply ok", %{socket: socket} do
      ref = push(socket, "accept_and_pay", %{})

      assert_reply(ref, :ok, %{})
    end

    test "should subscribe to Core topic banking:transaction_complete and reset the check", %{
      socket: socket
    } do
      Core.subscribe("shopping:check_reset")

      ref = push(socket, "close_check", %{})
      assert_reply(ref, :ok, %{})

      ref = push(socket, "accept_and_pay", %{})
      assert_reply(ref, :ok, %{})

      last_transaction = Core.Banking.list_transactions() |> List.last()
      Core.broadcast("banking:transaction_completed", last_transaction)

      assert_push("payment_complete", %{})
      assert_receive({"shopping:check_reset", %{}})
    end

    test "should subscribe to Core topic banking:transaction_failed", %{socket: socket} do
      Core.subscribe("shopping:check_reset")

      ref = push(socket, "close_check", %{})
      assert_reply(ref, :ok, %{})

      ref = push(socket, "accept_and_pay", %{})
      assert_reply(ref, :ok, %{})

      last_transaction = Core.Banking.list_transactions() |> List.last()
      Core.broadcast("banking:transaction_failed", last_transaction)

      assert_push("payment_failed", %{})
      assert_receive({"shopping:check_reset", %{}})
    end

    test "should start a banking transfer for the check total", %{
      socket: socket,
      customer_id: customer_id
    } do
      transaction_before = Banking.get_transactions_for(customer_id)
      assert Enum.empty?(transaction_before)

      ref = push(socket, "accept_and_pay", %{})
      assert_reply(ref, :ok, %{})

      transactions_after = Banking.get_transactions_for(customer_id)
      assert length(transactions_after) == 1

      transaction = List.first(transactions_after)
      assert transaction.from_id == customer_id
      assert transaction.to_id == Shopping.store_id_number()
      assert transaction.description == "Shopping"
    end
  end
end
