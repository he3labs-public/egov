defmodule Api.VotingChannelTest do
  use Api.ChannelCase

  alias Api.VotingChannel
  alias Core.Voting

  setup do
    elections = [insert(:voting_election, opened_at: Core.naive_now(-1))]
    referendas = [insert(:voting_referenda, opened_at: Core.naive_now(-1))]
    api_token = insert(:api_token)
    voter = api_token.identity

    {:ok, join_reply, socket} =
      socket("id", %{user_token: api_token.token})
      |> subscribe_and_join(VotingChannel, "voting:lobby")

    {:ok,
     socket: socket,
     join_reply: join_reply,
     voter: voter,
     elections: elections,
     referendas: referendas}
  end

  describe "join/3" do
    test "should reply with a list of elections", %{
      voter: voter,
      join_reply: reply,
      elections: elections,
      referendas: referendas
    } do
      representatives =
        Enum.map(
          elections,
          &Api.ElectionView.render("election.json", %{election: &1, voter_id: voter.id})
        )

      referendas =
        Enum.map(
          referendas,
          &Api.ReferendaView.render("referenda.json", %{referenda: &1, voter_id: voter.id})
        )

      assert %{representatives: representatives, referendas: referendas} == reply
    end

    test "should subscribe to \"ballot_ready\" for the current voter", %{
      socket: _socket,
      voter: voter,
      elections: [election | _],
      referendas: [referenda | _]
    } do
      Core.broadcast("ballot_ready", %{election_id: election.id, voter_id: voter.id})
      expected_payload = %{"election_id" => election.id}
      assert_push("election_ballot_ready", ^expected_payload)

      Core.broadcast("ballot_ready", %{referenda_id: referenda.id, voter_id: voter.id})
      expected_payload = %{"referenda_id" => referenda.id}
      assert_push("referenda_ballot_ready", ^expected_payload)
    end

    test "should not subscribe to \"ballot_ready\" for other voters", %{
      elections: [election | _],
      referendas: [referenda | _]
    } do
      Core.broadcast("ballot_ready", %{election_id: election.id, voter_id: "abc123"})
      unexpected_payload = %{"election_id" => election.id}
      refute_push("election_ballot_ready", ^unexpected_payload)

      Core.broadcast("ballot_ready", %{referenda_id: referenda.id, voter_id: "abc123"})
      unexpected_payload = %{"referenda_id" => referenda.id}
      refute_push("referenda_ballot_ready", ^unexpected_payload)
    end

    test "should subscribe to \"ballot_confirmed\" for the current voter", %{
      voter: voter,
      elections: [election | _],
      referendas: [referenda | _]
    } do
      code = "1234"

      Core.broadcast("ballot_confirmed", %{
        election_id: election.id,
        voter_id: voter.id,
        pin: code
      })

      election_id = election.id

      assert_push("election_ballot_confirmed", %{
        "election_id" => ^election_id,
        "confirmation_code" => ^code
      })

      Core.broadcast("ballot_confirmed", %{
        referenda_id: referenda.id,
        voter_id: voter.id,
        pin: code
      })

      referenda_id = referenda.id

      assert_push("referenda_ballot_confirmed", %{
        "referenda_id" => ^referenda_id,
        "confirmation_code" => ^code
      })
    end

    test "should not subscribe to \"ballot_confirmed\" for other voters", %{
      elections: [election | _],
      referendas: [referenda | _]
    } do
      code = "1234"

      Core.broadcast("ballot_confirmed", %{
        election_id: election.id,
        voter_id: "abc123",
        pin: code
      })

      expected_payload = %{"election_id" => election.id}
      refute_push("election_ballot_ready", ^expected_payload)

      Core.broadcast("ballot_confirmed", %{
        referenda_id: referenda.id,
        voter_id: "abc123",
        pin: code
      })

      expected_payload = %{"referenda_id" => referenda.id}
      refute_push("referenda_ballot_ready", ^expected_payload)
    end
  end

  test "ping replies with status ok", %{socket: socket} do
    ref = push(socket, "ping", %{"hello" => "there"})
    assert_reply(ref, :ok, %{"hello" => "there"})
  end

  test "request_ballot with a voter who has not participated replies with status ok", %{
    socket: socket,
    elections: [election | _],
    referendas: [referenda | _]
  } do
    ref = push(socket, "request_ballot", %{"election_id" => election.id})
    assert_reply(ref, :ok, %{})

    ref = push(socket, "request_ballot", %{"referenda_id" => referenda.id})
    assert_reply(ref, :ok, %{})
  end

  test "request_ballot with a voter who has alread participated replies with status error", %{
    socket: socket,
    voter: voter,
    elections: [election | _],
    referendas: [referenda | _]
  } do
    nem_account = insert(:nem_account)
    attrs = %{election_id: election.id, voter_id: voter.id, ballot_address: nem_account.address}
    Voting.create_election_participation(attrs)

    ref = push(socket, "request_ballot", %{"election_id" => election.id})
    assert_reply(ref, :error, %{reason: "Ballot has already been sent"})

    attrs = %{
      referenda_id: referenda.id,
      voter_id: voter.id,
      ballot_address: nem_account.address
    }

    Voting.create_referenda_participation(attrs)
    ref = push(socket, "request_ballot", %{"referenda_id" => referenda.id})
    assert_reply(ref, :error, %{reason: "Ballot has already been sent"})
  end

  test "request_ballot with an election that is not open replies with status error", %{
    socket: socket,
    elections: [election | _],
    referendas: [referenda | _]
  } do
    {:ok, _} = Voting.update_election(election, %{opened_at: nil})
    ref = push(socket, "request_ballot", %{"election_id" => election.id})
    assert_reply(ref, :error, %{reason: "Election not running"})

    {:ok, _} = Voting.update_referenda(referenda, %{opened_at: nil})
    ref = push(socket, "request_ballot", %{"referenda_id" => referenda.id})
    assert_reply(ref, :error, %{reason: "Referenda not running"})
  end

  @tag :skip
  test "submit_ballot with invalid election JSON replies with an error", %{
    socket: socket,
    elections: [election | _]
  } do
    ref = push(socket, "submit_ballot", %{"election_id" => election.id, "candidates" => ""})
    assert_reply(ref, :error, %{reason: "Submitted ballot JSON could not be decoded."})
  end

  test "submit_ballot with valid JSON, but election ballot that hasn't been requested replies with an error",
       %{
         socket: socket,
         elections: [election | _]
       } do
    ref = push(socket, "submit_ballot", %{"election_id" => election.id, "candidates" => "{}"})
    assert_reply(ref, :error, %{reason: "Ballot does not exist, request one first."})
  end

  @tag :skip
  test "submit_ballot with invalid referenda JSON replies with an error", %{
    socket: socket,
    referendas: [referenda | _]
  } do
    ref = push(socket, "submit_ballot", %{"referenda_id" => referenda.id, "referenda" => ""})
    assert_reply(ref, :error, %{reason: "Submitted ballot JSON could not be decoded."})
  end

  @tag :skip
  test "submit_ballot with valid JSON, but referenda ballot that hasn't been requested replies with an error",
       %{
         socket: socket,
         referendas: [referenda | _]
       } do
    ref = push(socket, "submit_ballot", %{"referenda_id" => referenda.id, "referenda" => "{}"})
    assert_reply(ref, :error, %{reason: "Ballot does not exist, request one first."})
  end
end
