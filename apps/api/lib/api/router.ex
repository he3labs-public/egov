defmodule Api.Router do
  use Api, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Api do
    pipe_through :api

    resources "/authorize", AuthorizeController, only: [:create]
    resources "/identities", IdentityController, only: [:index]
  end
end
