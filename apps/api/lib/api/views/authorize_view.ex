defmodule Api.AuthorizeView do
  use Api, :view
  alias Api.IdentityView

  def render("failed.json", %{}) do
    %{error: "Authorization Failed"}
  end

  def render("success.json", %{identity: identity}) do
    %{data: IdentityView.render("identity.json", %{identity: identity})}
  end
end
