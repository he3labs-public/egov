defmodule Api.IdentityView do
  use Api, :view
  alias Api.IdentityView

  def render("index.json", %{identities: identities}) do
    %{data: render_many(identities, IdentityView, "identity.json")}
  end

  def render("show.json", %{identity: identity}) do
    %{data: render_one(identity, IdentityView, "identity.json")}
  end

  def render("identity.json", %{identity: identity}) do
    identity = identity |> Core.Repo.preload(:api_token)

    %{id: identity.id,
      birthdate: identity.birthdate,
      blood_degree: identity.blood_degree,
      email: identity.email,
      first_name: identity.first_name,
      last_name: identity.last_name,
      phone: identity.phone,
      mailing_address: identity.mailing_address,
      pin: identity.pin,
      api_token: identity.api_token.token}
  end
end
