defmodule Api.ReferendaView do
  use Api, :view
  alias Api.ReferendaView

  def render("index.json", %{referendas: referendas}) do
    %{data: render_many(referendas, ReferendaView, "referenda.json")}
  end

  def render("show.json", %{referenda: referenda}) do
    %{data: render_one(referenda, ReferendaView, "referenda.json")}
  end

  def render("referenda.json", %{referenda: referenda, voter_id: voter_id}) do
    render("referenda.json", %{referenda: referenda})
    |> Map.put(:user_info, user_info(referenda, voter_id))
  end

  def render("referenda.json", %{referenda: referenda}) do
    %{
      id: referenda.id,
      end_date: referenda.end_date,
      start_date: referenda.start_date,
      open: is_open(referenda),
      measures: referenda.measures
    }
  end

  defp user_info(referenda, voter_id) do
    %{
      voted: has_voted(referenda.id, voter_id),
      eligible: true
    }
  end

  defp is_open(referenda) do
    referenda.opened_at && referenda.end_date < DateTime.utc_now()
  end

  defp has_voted(referenda_id, voter_id) do
    case Core.Voting.get_referenda_participation(referenda_id, voter_id) do
      nil -> false
      _ -> true
    end
  end
end
