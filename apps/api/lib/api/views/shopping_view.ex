defmodule Api.ShoppingView do
  use Api, :view

  alias Api.ShoppingView

  alias Core.Banking.Amount
  alias Core.Shopping

  def render("check.json", %{check: check}) do
    check
    |> Map.from_struct()
    |> Map.put(:items, render_many(check.items, ShoppingView, "product.json"))
    |> Map.put(:total, Amount.string_to_integer(check.total))
  end

  def render("product.json", %{product: product}) do
    product
    |> Map.from_struct()
  end
end
