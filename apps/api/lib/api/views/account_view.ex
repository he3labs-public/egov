defmodule Api.AccountView do
  use Api, :view

  def render("account.json", %{account: account}) do
    %{
      id: account.id_number,
      first_name: account.first_name,
      last_name: account.last_name
    }
  end
end
