defmodule Api.TransactionView do
  use Api, :view

  alias Api.TransactionView

  alias Core.Banking.{Amount, Transaction}

  def render("index.json", %{transaction: transaction}) do
    %{data: render_many(transaction, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction, display_to: user_id}) do
    amount = Transaction.relative_amount(transaction, user_id)

    %{
      id: transaction.id,
      description: Transaction.describe_to(transaction, user_id),
      amount: Amount.integer_to_string(amount),
      timestamp: transaction.inserted_at,
      status: transaction.status
    }
  end
end
