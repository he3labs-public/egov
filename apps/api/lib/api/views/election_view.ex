defmodule Api.ElectionView do
  use Api, :view
  alias Api.ElectionView

  def render("index.json", %{elections: elections}) do
    %{data: render_many(elections, ElectionView, "election.json")}
  end

  def render("show.json", %{election: election}) do
    %{data: render_one(election, ElectionView, "election.json")}
  end

  def render("election.json", %{election: election, voter_id: voter_id}) do
    render("election.json", %{election: election})
    |> Map.put(:user_info, user_info(election, voter_id))
  end

  def render("election.json", %{election: election}) do
    %{
      id: election.id,
      end_date: election.end_date,
      start_date: election.start_date,
      open: is_open(election),
      positions: Core.Voting.Candidate.positions(),
      candidates: election.candidates
    }
  end

  defp user_info(election, voter_id) do
    %{
      voted: has_voted(election.id, voter_id),
      eligible: true
    }
  end

  defp is_open(election) do
    election.opened_at && election.end_date < DateTime.utc_now()
  end

  defp has_voted(election_id, voter_id) do
    case Core.Voting.get_election_participation(election_id, voter_id) do
      nil -> false
      _ -> true
    end
  end
end
