defmodule Api.AuthorizeController do
  use Api, :controller

  alias Core.Account
  alias Core.Account.Identity

  action_fallback(Api.FallbackController)

  def create(conn, %{"pin" => pin}) do
    with %Identity{} = identity <- Account.authorize(pin) do
      conn
      |> put_status(:ok)
      |> render("success.json", identity: identity)
    else
      nil ->
        render(conn, "failed.json", %{})
    end
  end
end
