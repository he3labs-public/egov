defmodule Api.IdentityController do
  use Api, :controller

  alias Core.Account
  alias Core.Account.Identity

  action_fallback Api.FallbackController

  def index(conn, _params) do
    identities = Account.list_identities()
    render(conn, "index.json", identities: identities)
  end

  def create(conn, %{"identity" => identity_params}) do
    with {:ok, %Identity{} = identity} <- Account.create_identity(identity_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", identity_path(conn, :show, identity))
      |> render("show.json", identity: identity)
    end
  end

  def show(conn, %{"id" => id}) do
    identity = Account.get_identity!(id)
    render(conn, "show.json", identity: identity)
  end

  def update(conn, %{"id" => id, "identity" => identity_params}) do
    identity = Account.get_identity!(id)

    with {:ok, %Identity{} = identity} <- Account.update_identity(identity, identity_params) do
      render(conn, "show.json", identity: identity)
    end
  end

  def delete(conn, %{"id" => id}) do
    identity = Account.get_identity!(id)
    with {:ok, %Identity{}} <- Account.delete_identity(identity) do
      send_resp(conn, :no_content, "")
    end
  end
end
