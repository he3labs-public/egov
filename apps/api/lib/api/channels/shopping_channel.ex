defmodule Api.ShoppingChannel do
  require Logger
  use Api, :channel

  alias Core.Shopping
  alias Core.Account.Identity

  def join("shopping:lobby", payload, socket) do
    Logger.debug("[ShoppingChannel] Join Request")

    if authorized?(payload) do
      Logger.debug("[ShoppingChannel] Join Authorized")

      setup_core_pubsub_subscriptions()

      socket =
        socket
        |> assign(:check, Shopping.current_check())
        |> assign(:transactions, [])

      {:ok, socket}
    else
      Logger.debug("[ShoppingChannel] Join Failed")

      {:error, %{reason: "unauthorized"}}
    end
  end

  ##
  ## Clerk: Messages Inbound from Scanner
  ##

  def handle_in("product_scanned", %{"product_id" => product_id}, socket) do
    Logger.debug("[ShoppingChannel] Clerk scanned product #{product_id}")

    case Shopping.get_product(product_id) do
      nil ->
        Logger.debug("[ShoppingChannel] Product scan failed: product not found")
        {:reply, {:error, %{reason: "Product not found"}}, socket}

      product ->
        Logger.debug("[ShoppingChannel] Product scan successful: product was found")

        Core.broadcast("shopping:add_product", product)

        {:reply, {:ok, %{}}, socket}
    end
  end

  def handle_in("reset_check", %{}, socket) do
    Logger.debug("[ShoppingChannel] Clerk reset_check")

    socket = reset_check(socket)
    {:reply, {:ok, socket.assigns.check}, socket}
  end

  def handle_in("close_check", %{}, socket) do
    Logger.debug("[ShoppingChannel] Clerk close_check")

    case Shopping.close_check(socket.assigns.check) do
      {:ok, closed_check} ->
        Logger.debug("[ShoppingChannel] Check close suuccessful")

        socket = assign(socket, :check, closed_check)
        Core.broadcast_from("shopping:check_closed", %{})

        {:reply, {:ok, %{}}, socket}

      {:error, reason} ->
        Logger.debug("[ShoppingChannel] Check close failed: #{reason}")

        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  ##
  ## Customer: Messages Inbound from Kiosk
  ##

  def handle_in("customer_scanned", %{"customer_id" => customer_id}, socket) do
    Logger.debug("[ShoppingChannel] Inbound customer_scanned: #{customer_id}")

    case Shopping.lookup_customer(customer_id) do
      {:ok, customer} ->
        Logger.debug("[ShoppingChannel] Customer scan successful: customer was identified")

        # Notify Everyone
        Core.broadcast("shopping:add_customer", customer)

        {:reply, {:ok, %{}}, socket}

      {:error, reason} ->
        Logger.debug("[ShoppingChannel] Customer scan failed: #{reason}")

        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def handle_in("accept_and_pay", _payload, socket) do
    Logger.debug("[ShoppingChannel] Inbound accept_and_pay")

    check = socket.assigns.check

    case Shopping.charge_customer(check.customer_id, check.total) do
      {:ok, transaction} ->
        {:reply, {:ok, %{}}, add_transaction(socket, transaction.id)}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def handle_in("decline_payment", _payload, socket) do
    Logger.debug("[ShoppingChannel] Inbound decline_payment")

    Core.broadcast("shopping:payment_declined", %{})

    {:reply, {:ok, %{}}, socket}
  end

  ##
  ## General / Other Messages
  ##

  def handle_in("ping", payload, socket) do
    Logger.debug("[ShoppingChannel] Ping request")

    {:reply, {:ok, payload}, socket}
  end

  ##
  ## Core PubSub Messages
  ##

  def handle_info({"banking:transaction_completed", transaction}, socket) do
    if Enum.member?(socket.assigns.transactions, transaction.id) do
      push(socket, "payment_complete", %{})

      socket =
        socket
        |> remove_transaction(transaction.id)
        |> reset_check()

      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info({"banking:transaction_failed", transaction}, socket) do
    if Enum.member?(socket.assigns.transactions, transaction.id) do
      push(socket, "payment_failed", %{})

      socket =
        socket
        |> remove_transaction(transaction.id)
        |> reset_check()

      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info({"shopping:add_customer", customer}, socket) do
    customer_id = customer.id
    customer_name = Identity.full_name(customer)

    Logger.debug(
      "[ShoppingChannel] Customer Identified: id: #{customer_id}, name: #{customer_name}"
    )

    case Shopping.add_customer_to_check(socket.assigns.check, customer_id, customer_name) do
      {:ok, updated_check} ->
        Logger.debug("[ShoppingChannel] Customer added to check: #{inspect(updated_check)}")

        socket = assign(socket, :check, updated_check)
        push(socket, "check_updated", updated_check)

        {:noreply, socket}

      {:error, reason} ->
        Logger.debug("[ShoppingChannel] Customer NOT added to check: #{reason}")

        {:noreply, socket}
    end
  end

  def handle_info({"shopping:add_product", product}, socket) do
    case Shopping.add_product_to_check(socket.assigns.check, product) do
      {:ok, updated_check} ->
        Logger.debug("[ShoppingChannel] Product added to check: #{product.id}")

        socket = assign(socket, :check, updated_check)
        push(socket, "check_updated", updated_check)

        {:noreply, socket}

      {:error, reason} ->
        Logger.debug("[ShoppingChannel] Product NOT added to check: #{reason}")
        {:noreply, socket}
    end
  end

  def handle_info({"shopping:check_reset", %{}}, socket) do
    {:ok, empty_check} = Shopping.reset_check(socket.assigns.check)
    socket = assign(socket, :check, empty_check)

    push(socket, "check_updated", empty_check)

    {:noreply, socket}
  end

  def handle_info({"shopping:check_closed", %{}}, socket) do
    case Shopping.close_check(socket.assigns.check) do
      {:ok, closed_check} ->
        socket = assign(socket, :check, closed_check)
        push(socket, "ready_for_payment", %{total: closed_check.total})

        {:noreply, socket}

      {:error, reason} ->
        Logger.debug("[ShoppingChannel] Check close failed: #{reason}")

        {:noreply, socket}
    end
  end

  def handle_info({"shopping:payment_declined", %{}}, socket) do
    Logger.debug("[ShoppingChannel] Customer declined payment")

    push(socket, "payment_declined", %{})

    {:noreply, reset_check(socket)}
  end

  ##
  ## Helpers
  ##

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

  defp setup_core_pubsub_subscriptions() do
    Core.subscribe("shopping:add_customer")
    Core.subscribe("shopping:add_product")
    Core.subscribe("shopping:check_reset")
    Core.subscribe("shopping:check_closed")
    Core.subscribe("shopping:payment_declined")
    Core.subscribe("banking:transaction_completed")
    Core.subscribe("banking:transaction_failed")
  end

  defp reset_check(socket) do
    {:ok, empty_check} = Shopping.reset_check(socket.assigns.check)

    socket = assign(socket, :check, empty_check)

    Core.broadcast_from("shopping:check_reset", %{})

    socket
  end

  defp add_transaction(socket, transaction_id) do
    updated_transactions = [transaction_id | socket.assigns.transactions]
    assign(socket, :transactions, updated_transactions)
  end

  defp remove_transaction(socket, transaction_id) do
    updated_transactions = List.delete(socket.assigns.transactions, transaction_id)
    assign(socket, :transactions, updated_transactions)
  end
end
