defmodule Api.VotingChannel do
  require Logger

  use Api, :channel

  alias Core.Demo

  def join("voting:lobby", payload, socket) do
    Logger.debug("[VotingChannel] Join ...")
    Logger.debug("[VotingChannel]     Payload: #{inspect(payload)}")
    Logger.debug("[VotingChannel]     Socket: #{inspect(socket)}")

    api_token = socket.assigns[:user_token]

    if api_token do
      case authorize(api_token) do
        {:ok, user_id} ->
          Logger.debug("[VotingChannel] Join Authorized: User Id = #{user_id}")

          socket = assign(socket, :user_id, user_id)
          reply = build_state(user_id)
          setup_core_pubsub_subscriptions()

          {:ok, reply, socket}

        :error ->
          Logger.debug("[VotingChannel] Authentication Failed")
          {:error, %{reason: "unauthorized"}}
      end
    else
      Logger.debug("[VotingChannel] Join Failed: Missing API token")
      {:error, %{reason: "unauthorized"}}
    end
  end

  ##
  ## Inbound Client Messages
  ##

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("request_ballot", %{"election_id" => election_id}, socket) do
    voter_id = socket.assigns[:user_id]

    case Demo.request_election_ballot(election_id, voter_id) do
      {:ok, _participation} ->
        {:reply, {:ok, %{}}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def handle_in("request_ballot", %{"referenda_id" => referenda_id}, socket) do
    voter_id = socket.assigns[:user_id]

    case Demo.request_referenda_ballot(referenda_id, voter_id) do
      {:ok, _participation} ->
        {:reply, {:ok, %{}}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def handle_in(
        "submit_ballot",
        %{"election_id" => election_id, "candidates" => _candidates} = ballot,
        socket
      ) do
    voter_id = socket.assigns[:user_id]

    case Demo.submit_election_ballot(election_id, voter_id, ballot) do
      :ok ->
        {:reply, {:ok, %{}}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def handle_in(
        "submit_ballot",
        %{"referenda_id" => referenda_id, "referenda" => _referenda} = ballot,
        socket
      ) do
    voter_id = socket.assigns[:user_id]

    case Demo.submit_referenda_ballot(referenda_id, voter_id, ballot) do
      :ok ->
        {:reply, {:ok, %{}}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  ##
  ## Core PubSub Messages
  ##

  def handle_info({"ballot_ready", %{voter_id: voter_id, election_id: election_id}}, socket) do
    if voter_id == socket.assigns[:user_id] do
      push(socket, "election_ballot_ready", %{"election_id" => election_id})
      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info({"ballot_ready", %{voter_id: voter_id, referenda_id: referenda_id}}, socket) do
    if voter_id == socket.assigns[:user_id] do
      push(socket, "referenda_ballot_ready", %{"referenda_id" => referenda_id})
      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info(
        {"ballot_confirmed", %{election_id: election_id, voter_id: voter_id, pin: code}},
        socket
      ) do
    if voter_id == socket.assigns[:user_id] do
      push(socket, "election_ballot_confirmed", %{
        "election_id" => election_id,
        "confirmation_code" => code
      })

      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info(
        {"ballot_confirmed", %{referenda_id: referenda_id, voter_id: voter_id, pin: code}},
        socket
      ) do
    if voter_id == socket.assigns[:user_id] do
      push(socket, "referenda_ballot_confirmed", %{
        "referenda_id" => referenda_id,
        "confirmation_code" => code
      })

      {:noreply, socket}
    else
      {:noreply, socket}
    end
  end

  def handle_info({"voting:election_seeded", _}, socket) do
    voter_id = socket.assigns[:user_id]
    reply = build_state(voter_id)
    push(socket, "voting_refresh", reply)
    {:noreply, socket}
  end

  def handle_info({"voting:referenda_seeded", _}, socket) do
    voter_id = socket.assigns[:user_id]
    reply = build_state(voter_id)

    push(socket, "voting_refresh", reply)
    {:noreply, socket}
  end

  ##
  ## Helpers
  ##

  defp authorize(api_token) do
    Logger.debug("[VotingChannel] Authenticating ...")

    if Core.Account.api_token_valid?(api_token) do
      record = Core.Account.get_api_token_by!(token: api_token)
      {:ok, record.identity_id}
    else
      :error
    end
  end

  defp setup_core_pubsub_subscriptions() do
    Core.subscribe("ballot_ready")
    Core.subscribe("ballot_confirmed")
    Core.subscribe("voting:election_seeded")
    Core.subscribe("voting:referenda_seeded")
  end

  defp build_state(voter_id) do
    representatives =
      Core.Voting.list_voting_elections()
      |> Enum.map(&Api.ElectionView.render("election.json", %{election: &1, voter_id: voter_id}))

    referendas =
      Core.Voting.list_referendas()
      |> Enum.map(
        &Api.ReferendaView.render("referenda.json", %{referenda: &1, voter_id: voter_id})
      )

    %{representatives: representatives, referendas: referendas}
  end
end
