defmodule Api.BankingChannel do
  require Logger
  use Api, :channel

  import Api.TransactionView, only: [render: 2]

  alias Core.Banking
  alias Core.Banking.Transaction

  def join("banking:lobby", payload, socket) do
    Logger.debug("[BankingChannel] Join ...")
    Logger.debug("[BankingChannel]     Payload: #{inspect(payload)}")
    Logger.debug("[BankingChannel]     Socket: #{inspect(socket)}")

    setup_core_pubsub_subscriptions()
    api_token = socket.assigns[:user_token]

    if api_token do
      case authorize(api_token) do
        {:ok, user_id} ->
          Logger.debug("[BankingChannel] Join Authorized: User Id = #{user_id}")

          socket = assign(socket, :user_id, user_id)
          reply = build_state(user_id)
          setup_core_pubsub_subscriptions()

          {:ok, reply, socket}

        :error ->
          Logger.debug("[BankingChannel] Authentication Failed")
          {:error, %{reason: "unauthorized"}}
      end
    else
      Logger.debug("[VotingChannel] Join Failed: Missing API token")
      {:error, %{reason: "unauthorized"}}
    end
  end

  ##
  ## Inbound Client Messages
  ##

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("lookup", %{"id" => id_number}, socket) do
    case Banking.lookup(id_number) do
      nil ->
        {:reply, {:error, %{reason: "Not Found"}}, socket}

      account ->
        message = Api.AccountView.render("account.json", %{account: account})
        {:reply, {:ok, message}, socket}
    end
  end

  def handle_in("transfer", %{"amount" => amount, "to" => recipient_id}, socket) do
    from = socket.assigns[:user_id]
    to = recipient_id

    case Banking.transfer(from, to, amount) do
      {:ok, transaction} ->
        params = %{transaction: transaction, display_to: from}
        message = render("transaction.json", params)

        {:reply, {:ok, message}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  ##
  ## Core PubSub Messages
  ##

  def handle_info({"banking:transaction_created", transaction}, socket) do
    user_id = socket.assigns[:user_id]

    if Transaction.member?(transaction, user_id) do
      push(socket, "account_state", build_state(user_id))
    end

    if Transaction.recipient?(transaction, user_id) do
      message = render_transaction(transaction, user_id)

      push(socket, "incoming_transfer", message)
    end

    {:noreply, socket}
  end

  def handle_info({"banking:transaction_failed", transaction}, socket) do
    user_id = socket.assigns[:user_id]

    if Transaction.member?(transaction, user_id) do
      message = render_transaction(transaction, user_id)

      push(socket, "transaction_status", message)
      push(socket, "account_state", build_state(user_id))
    end

    {:noreply, socket}
  end

  def handle_info({"banking:transaction_completed", transaction}, socket) do
    user_id = socket.assigns[:user_id]

    if Transaction.member?(transaction, user_id) do
      message = render_transaction(transaction, user_id)

      push(socket, "transaction_status", message)
      push(socket, "account_state", build_state(user_id))
    end

    {:noreply, socket}
  end

  ##
  ## Helpers
  ##

  defp authorize(api_token) do
    Logger.debug("[BankingChannel] Authenticating ...")

    if Core.Account.api_token_valid?(api_token) do
      record = Core.Account.get_api_token_by!(token: api_token)
      {:ok, record.identity_id}
    else
      :error
    end
  end

  defp setup_core_pubsub_subscriptions() do
    Core.subscribe("banking:transaction_created")
    Core.subscribe("banking:transaction_failed")
    Core.subscribe("banking:transaction_completed")
  end

  defp build_state(user_id) do
    transactions = Banking.get_transactions_for(user_id)

    balance =
      transactions
      |> Banking.calculate_balance(user_id)
      |> Core.Banking.Amount.integer_to_string()

    %{balance: balance, transactions: render_transactions(transactions, user_id)}
  end

  defp render_transactions(transactions, user_id) do
    transactions
    |> Enum.map(&render_transaction(&1, user_id))
  end

  defp render_transaction(transaction, user_id) do
    render("transaction.json", %{transaction: transaction, display_to: user_id})
  end
end
