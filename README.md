# eGov Demo

This is an open-source release of a e-government demo built upon the NEM Catapult blockchain.
In this repo are components for: Identity, Banking, Shopping and Voting.

In `apps/api`, the README contains some documentation about how to interact with the system.

This is not meant to be an out of the box eGov system, but rather an example of what can be done with the NEM blockchain.

## Installing Dependencies

* Install Node: `brew install npm`
* Install Elixir: `brew install elixir`
* Install PostgreSQL: `brew install postgresql`

## Setting up ONC Demo
  1. cd [project dir]
  * edit `config/config.exs` and configure catapult node URL, demo lord keypair, etc.
  * mix deps.get
  * `mix ecto.setup` - OR - `pg_restore -Ovxcd onc_demo_dev onc_dev.dump -U postgres`
  * cd [project dir]
  * iex -S mix phx.server

## URLs

The JSON API is available at [`localhost:4100`](http://localhost:4100).

WebSocket connections should be made at [`localhost:4100/socket`](ws://localhost:4100/socket).

## Credits

### EctoHelper
Prettify Ecto errors

Code from GitHub Gist  
https://gist.github.com/sergio1990/537fcf958752ae223baa4a782ee09109

Code Author  
https://github.com/sergio1990

Thanks to Sergio Gernyak for sharing this very simple but helpful bit of code.
