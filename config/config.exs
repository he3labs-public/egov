# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# By default, the umbrella project as well as each child
# application will require this configuration file, ensuring
# they all use the same configuration. While one could
# configure all applications here, we prefer to delegate
# back to each application for organization purposes.
import_config "../apps/*/config/config.exs"

## Notes:
##   - All config entries are required!
##   - Demo Lord keypair should own all of the mosaic.

# config :exnem,
#   node_url: "localhost:3000",
#   network_type: :mijin_test,
#   # How often are blocks harvested (in seconds)? Default: 15
#   block_duration: 15

# config :core,
#   demo_lord_key_pair: %{
#     public_key: "ABC123",
#     private_key: "098ZYX",
#     address: "AABBC"
#   },
#   demo_namespace: "my",
#   demo_mosaic_name: "coin",
#   demo_mosaic_supply: 8_900_000_000_000,
#   demo_lord_mosaic_ids: %{
#     # Mosaic ID as an integer
#     usd: 8_050_144_702_280_590_652
#   }
